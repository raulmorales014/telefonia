/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { TelefoniaTestModule } from '../../../test.module';
import { SubestadoDetailComponent } from 'app/entities/subestado/subestado-detail.component';
import { Subestado } from 'app/shared/model/subestado.model';

describe('Component Tests', () => {
    describe('Subestado Management Detail Component', () => {
        let comp: SubestadoDetailComponent;
        let fixture: ComponentFixture<SubestadoDetailComponent>;
        const route = ({ data: of({ subestado: new Subestado(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [TelefoniaTestModule],
                declarations: [SubestadoDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(SubestadoDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(SubestadoDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.subestado).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
