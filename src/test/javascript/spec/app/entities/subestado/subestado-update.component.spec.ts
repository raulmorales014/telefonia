/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { TelefoniaTestModule } from '../../../test.module';
import { SubestadoUpdateComponent } from 'app/entities/subestado/subestado-update.component';
import { SubestadoService } from 'app/entities/subestado/subestado.service';
import { Subestado } from 'app/shared/model/subestado.model';

describe('Component Tests', () => {
    describe('Subestado Management Update Component', () => {
        let comp: SubestadoUpdateComponent;
        let fixture: ComponentFixture<SubestadoUpdateComponent>;
        let service: SubestadoService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [TelefoniaTestModule],
                declarations: [SubestadoUpdateComponent]
            })
                .overrideTemplate(SubestadoUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(SubestadoUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SubestadoService);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity', fakeAsync(() => {
                // GIVEN
                const entity = new Subestado(123);
                spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                comp.subestado = entity;
                // WHEN
                comp.save();
                tick(); // simulate async

                // THEN
                expect(service.update).toHaveBeenCalledWith(entity);
                expect(comp.isSaving).toEqual(false);
            }));

            it('Should call create service on save for new entity', fakeAsync(() => {
                // GIVEN
                const entity = new Subestado();
                spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                comp.subestado = entity;
                // WHEN
                comp.save();
                tick(); // simulate async

                // THEN
                expect(service.create).toHaveBeenCalledWith(entity);
                expect(comp.isSaving).toEqual(false);
            }));
        });
    });
});
