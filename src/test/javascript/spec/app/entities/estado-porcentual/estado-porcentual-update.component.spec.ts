/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { TelefoniaTestModule } from '../../../test.module';
import { EstadoPorcentualUpdateComponent } from 'app/entities/estado-porcentual/estado-porcentual-update.component';
import { EstadoPorcentualService } from 'app/entities/estado-porcentual/estado-porcentual.service';
import { EstadoPorcentual } from 'app/shared/model/estado-porcentual.model';

describe('Component Tests', () => {
    describe('EstadoPorcentual Management Update Component', () => {
        let comp: EstadoPorcentualUpdateComponent;
        let fixture: ComponentFixture<EstadoPorcentualUpdateComponent>;
        let service: EstadoPorcentualService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [TelefoniaTestModule],
                declarations: [EstadoPorcentualUpdateComponent]
            })
                .overrideTemplate(EstadoPorcentualUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(EstadoPorcentualUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(EstadoPorcentualService);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity', fakeAsync(() => {
                // GIVEN
                const entity = new EstadoPorcentual(123);
                spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                comp.estadoPorcentual = entity;
                // WHEN
                comp.save();
                tick(); // simulate async

                // THEN
                expect(service.update).toHaveBeenCalledWith(entity);
                expect(comp.isSaving).toEqual(false);
            }));

            it('Should call create service on save for new entity', fakeAsync(() => {
                // GIVEN
                const entity = new EstadoPorcentual();
                spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                comp.estadoPorcentual = entity;
                // WHEN
                comp.save();
                tick(); // simulate async

                // THEN
                expect(service.create).toHaveBeenCalledWith(entity);
                expect(comp.isSaving).toEqual(false);
            }));
        });
    });
});
