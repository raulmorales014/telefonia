/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { TelefoniaTestModule } from '../../../test.module';
import { EstadoPorcentualDeleteDialogComponent } from 'app/entities/estado-porcentual/estado-porcentual-delete-dialog.component';
import { EstadoPorcentualService } from 'app/entities/estado-porcentual/estado-porcentual.service';

describe('Component Tests', () => {
    describe('EstadoPorcentual Management Delete Component', () => {
        let comp: EstadoPorcentualDeleteDialogComponent;
        let fixture: ComponentFixture<EstadoPorcentualDeleteDialogComponent>;
        let service: EstadoPorcentualService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [TelefoniaTestModule],
                declarations: [EstadoPorcentualDeleteDialogComponent]
            })
                .overrideTemplate(EstadoPorcentualDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(EstadoPorcentualDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(EstadoPorcentualService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete', inject(
                [],
                fakeAsync(() => {
                    // GIVEN
                    spyOn(service, 'delete').and.returnValue(of({}));

                    // WHEN
                    comp.confirmDelete(123);
                    tick();

                    // THEN
                    expect(service.delete).toHaveBeenCalledWith(123);
                    expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                })
            ));
        });
    });
});
