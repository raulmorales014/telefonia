/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { TelefoniaTestModule } from '../../../test.module';
import { EstadoPorcentualDetailComponent } from 'app/entities/estado-porcentual/estado-porcentual-detail.component';
import { EstadoPorcentual } from 'app/shared/model/estado-porcentual.model';

describe('Component Tests', () => {
    describe('EstadoPorcentual Management Detail Component', () => {
        let comp: EstadoPorcentualDetailComponent;
        let fixture: ComponentFixture<EstadoPorcentualDetailComponent>;
        const route = ({ data: of({ estadoPorcentual: new EstadoPorcentual(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [TelefoniaTestModule],
                declarations: [EstadoPorcentualDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(EstadoPorcentualDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(EstadoPorcentualDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.estadoPorcentual).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
