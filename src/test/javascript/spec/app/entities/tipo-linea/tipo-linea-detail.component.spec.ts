/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { TelefoniaTestModule } from '../../../test.module';
import { TipoLineaDetailComponent } from 'app/entities/tipo-linea/tipo-linea-detail.component';
import { TipoLinea } from 'app/shared/model/tipo-linea.model';

describe('Component Tests', () => {
    describe('TipoLinea Management Detail Component', () => {
        let comp: TipoLineaDetailComponent;
        let fixture: ComponentFixture<TipoLineaDetailComponent>;
        const route = ({ data: of({ tipoLinea: new TipoLinea(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [TelefoniaTestModule],
                declarations: [TipoLineaDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(TipoLineaDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(TipoLineaDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.tipoLinea).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
