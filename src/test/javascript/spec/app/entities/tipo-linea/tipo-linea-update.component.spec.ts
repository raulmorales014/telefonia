/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { TelefoniaTestModule } from '../../../test.module';
import { TipoLineaUpdateComponent } from 'app/entities/tipo-linea/tipo-linea-update.component';
import { TipoLineaService } from 'app/entities/tipo-linea/tipo-linea.service';
import { TipoLinea } from 'app/shared/model/tipo-linea.model';

describe('Component Tests', () => {
    describe('TipoLinea Management Update Component', () => {
        let comp: TipoLineaUpdateComponent;
        let fixture: ComponentFixture<TipoLineaUpdateComponent>;
        let service: TipoLineaService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [TelefoniaTestModule],
                declarations: [TipoLineaUpdateComponent]
            })
                .overrideTemplate(TipoLineaUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(TipoLineaUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(TipoLineaService);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity', fakeAsync(() => {
                // GIVEN
                const entity = new TipoLinea(123);
                spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                comp.tipoLinea = entity;
                // WHEN
                comp.save();
                tick(); // simulate async

                // THEN
                expect(service.update).toHaveBeenCalledWith(entity);
                expect(comp.isSaving).toEqual(false);
            }));

            it('Should call create service on save for new entity', fakeAsync(() => {
                // GIVEN
                const entity = new TipoLinea();
                spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                comp.tipoLinea = entity;
                // WHEN
                comp.save();
                tick(); // simulate async

                // THEN
                expect(service.create).toHaveBeenCalledWith(entity);
                expect(comp.isSaving).toEqual(false);
            }));
        });
    });
});
