/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { TelefoniaTestModule } from '../../../test.module';
import { TipoLineaDeleteDialogComponent } from 'app/entities/tipo-linea/tipo-linea-delete-dialog.component';
import { TipoLineaService } from 'app/entities/tipo-linea/tipo-linea.service';

describe('Component Tests', () => {
    describe('TipoLinea Management Delete Component', () => {
        let comp: TipoLineaDeleteDialogComponent;
        let fixture: ComponentFixture<TipoLineaDeleteDialogComponent>;
        let service: TipoLineaService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [TelefoniaTestModule],
                declarations: [TipoLineaDeleteDialogComponent]
            })
                .overrideTemplate(TipoLineaDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(TipoLineaDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(TipoLineaService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete', inject(
                [],
                fakeAsync(() => {
                    // GIVEN
                    spyOn(service, 'delete').and.returnValue(of({}));

                    // WHEN
                    comp.confirmDelete(123);
                    tick();

                    // THEN
                    expect(service.delete).toHaveBeenCalledWith(123);
                    expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                })
            ));
        });
    });
});
