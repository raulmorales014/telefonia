/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { TelefoniaTestModule } from '../../../test.module';
import { ComercialDetailComponent } from 'app/entities/comercial/comercial-detail.component';
import { Comercial } from 'app/shared/model/comercial.model';

describe('Component Tests', () => {
    describe('Comercial Management Detail Component', () => {
        let comp: ComercialDetailComponent;
        let fixture: ComponentFixture<ComercialDetailComponent>;
        const route = ({ data: of({ comercial: new Comercial(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [TelefoniaTestModule],
                declarations: [ComercialDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(ComercialDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(ComercialDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.comercial).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
