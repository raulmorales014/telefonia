/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { TelefoniaTestModule } from '../../../test.module';
import { ComercialUpdateComponent } from 'app/entities/comercial/comercial-update.component';
import { ComercialService } from 'app/entities/comercial/comercial.service';
import { Comercial } from 'app/shared/model/comercial.model';

describe('Component Tests', () => {
    describe('Comercial Management Update Component', () => {
        let comp: ComercialUpdateComponent;
        let fixture: ComponentFixture<ComercialUpdateComponent>;
        let service: ComercialService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [TelefoniaTestModule],
                declarations: [ComercialUpdateComponent]
            })
                .overrideTemplate(ComercialUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(ComercialUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ComercialService);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity', fakeAsync(() => {
                // GIVEN
                const entity = new Comercial(123);
                spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                comp.comercial = entity;
                // WHEN
                comp.save();
                tick(); // simulate async

                // THEN
                expect(service.update).toHaveBeenCalledWith(entity);
                expect(comp.isSaving).toEqual(false);
            }));

            it('Should call create service on save for new entity', fakeAsync(() => {
                // GIVEN
                const entity = new Comercial();
                spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                comp.comercial = entity;
                // WHEN
                comp.save();
                tick(); // simulate async

                // THEN
                expect(service.create).toHaveBeenCalledWith(entity);
                expect(comp.isSaving).toEqual(false);
            }));
        });
    });
});
