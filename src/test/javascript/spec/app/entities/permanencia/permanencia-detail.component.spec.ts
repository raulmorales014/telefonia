/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { TelefoniaTestModule } from '../../../test.module';
import { PermanenciaDetailComponent } from 'app/entities/permanencia/permanencia-detail.component';
import { Permanencia } from 'app/shared/model/permanencia.model';

describe('Component Tests', () => {
    describe('Permanencia Management Detail Component', () => {
        let comp: PermanenciaDetailComponent;
        let fixture: ComponentFixture<PermanenciaDetailComponent>;
        const route = ({ data: of({ permanencia: new Permanencia(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [TelefoniaTestModule],
                declarations: [PermanenciaDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(PermanenciaDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(PermanenciaDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.permanencia).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
