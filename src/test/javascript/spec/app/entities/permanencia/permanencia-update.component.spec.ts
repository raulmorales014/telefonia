/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { TelefoniaTestModule } from '../../../test.module';
import { PermanenciaUpdateComponent } from 'app/entities/permanencia/permanencia-update.component';
import { PermanenciaService } from 'app/entities/permanencia/permanencia.service';
import { Permanencia } from 'app/shared/model/permanencia.model';

describe('Component Tests', () => {
    describe('Permanencia Management Update Component', () => {
        let comp: PermanenciaUpdateComponent;
        let fixture: ComponentFixture<PermanenciaUpdateComponent>;
        let service: PermanenciaService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [TelefoniaTestModule],
                declarations: [PermanenciaUpdateComponent]
            })
                .overrideTemplate(PermanenciaUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(PermanenciaUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PermanenciaService);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity', fakeAsync(() => {
                // GIVEN
                const entity = new Permanencia(123);
                spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                comp.permanencia = entity;
                // WHEN
                comp.save();
                tick(); // simulate async

                // THEN
                expect(service.update).toHaveBeenCalledWith(entity);
                expect(comp.isSaving).toEqual(false);
            }));

            it('Should call create service on save for new entity', fakeAsync(() => {
                // GIVEN
                const entity = new Permanencia();
                spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                comp.permanencia = entity;
                // WHEN
                comp.save();
                tick(); // simulate async

                // THEN
                expect(service.create).toHaveBeenCalledWith(entity);
                expect(comp.isSaving).toEqual(false);
            }));
        });
    });
});
