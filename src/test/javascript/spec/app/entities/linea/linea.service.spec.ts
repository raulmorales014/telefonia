/* tslint:disable max-line-length */
import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { take, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { LineaService } from 'app/entities/linea/linea.service';
import { ILinea, Linea } from 'app/shared/model/linea.model';

describe('Service Tests', () => {
    describe('Linea Service', () => {
        let injector: TestBed;
        let service: LineaService;
        let httpMock: HttpTestingController;
        let elemDefault: ILinea;
        let currentDate: moment.Moment;
        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [HttpClientTestingModule]
            });
            injector = getTestBed();
            service = injector.get(LineaService);
            httpMock = injector.get(HttpTestingController);
            currentDate = moment();

            elemDefault = new Linea(
                0,
                0,
                false,
                0,
                false,
                'AAAAAAA',
                'AAAAAAA',
                'AAAAAAA',
                currentDate,
                currentDate,
                'AAAAAAA',
                'AAAAAAA',
                0,
                0,
                0,
                'AAAAAAA',
                'AAAAAAA'
            );
        });

        describe('Service methods', async () => {
            it('should find an element', async () => {
                const returnedFromService = Object.assign(
                    {
                        provision: currentDate.format(DATE_FORMAT),
                        activacion: currentDate.format(DATE_FORMAT)
                    },
                    elemDefault
                );
                service
                    .find(123)
                    .pipe(take(1))
                    .subscribe(resp => expect(resp).toMatchObject({ body: elemDefault }));

                const req = httpMock.expectOne({ method: 'GET' });
                req.flush(JSON.stringify(returnedFromService));
            });

            it('should create a Linea', async () => {
                const returnedFromService = Object.assign(
                    {
                        id: 0,
                        provision: currentDate.format(DATE_FORMAT),
                        activacion: currentDate.format(DATE_FORMAT)
                    },
                    elemDefault
                );
                const expected = Object.assign(
                    {
                        provision: currentDate,
                        activacion: currentDate
                    },
                    returnedFromService
                );
                service
                    .create(new Linea(null))
                    .pipe(take(1))
                    .subscribe(resp => expect(resp).toMatchObject({ body: expected }));
                const req = httpMock.expectOne({ method: 'POST' });
                req.flush(JSON.stringify(returnedFromService));
            });

            it('should update a Linea', async () => {
                const returnedFromService = Object.assign(
                    {
                        linea: 1,
                        esNueva: true,
                        descuento: 1,
                        porta: true,
                        portaNombre: 'BBBBBB',
                        portaCif: 'BBBBBB',
                        portaOperador: 'BBBBBB',
                        provision: currentDate.format(DATE_FORMAT),
                        activacion: currentDate.format(DATE_FORMAT),
                        paqueteNumero: 'BBBBBB',
                        terminal: 'BBBBBB',
                        pagoInicial: 1,
                        cuotas: 1,
                        pagoUnico: 1,
                        observacion: 'BBBBBB',
                        observacionAdicional: 'BBBBBB'
                    },
                    elemDefault
                );

                const expected = Object.assign(
                    {
                        provision: currentDate,
                        activacion: currentDate
                    },
                    returnedFromService
                );
                service
                    .update(expected)
                    .pipe(take(1))
                    .subscribe(resp => expect(resp).toMatchObject({ body: expected }));
                const req = httpMock.expectOne({ method: 'PUT' });
                req.flush(JSON.stringify(returnedFromService));
            });

            it('should return a list of Linea', async () => {
                const returnedFromService = Object.assign(
                    {
                        linea: 1,
                        esNueva: true,
                        descuento: 1,
                        porta: true,
                        portaNombre: 'BBBBBB',
                        portaCif: 'BBBBBB',
                        portaOperador: 'BBBBBB',
                        provision: currentDate.format(DATE_FORMAT),
                        activacion: currentDate.format(DATE_FORMAT),
                        paqueteNumero: 'BBBBBB',
                        terminal: 'BBBBBB',
                        pagoInicial: 1,
                        cuotas: 1,
                        pagoUnico: 1,
                        observacion: 'BBBBBB',
                        observacionAdicional: 'BBBBBB'
                    },
                    elemDefault
                );
                const expected = Object.assign(
                    {
                        provision: currentDate,
                        activacion: currentDate
                    },
                    returnedFromService
                );
                service
                    .query(expected)
                    .pipe(
                        take(1),
                        map(resp => resp.body)
                    )
                    .subscribe(body => expect(body).toContainEqual(expected));
                const req = httpMock.expectOne({ method: 'GET' });
                req.flush(JSON.stringify([returnedFromService]));
                httpMock.verify();
            });

            it('should delete a Linea', async () => {
                const rxPromise = service.delete(123).subscribe(resp => expect(resp.ok));

                const req = httpMock.expectOne({ method: 'DELETE' });
                req.flush({ status: 200 });
            });
        });

        afterEach(() => {
            httpMock.verify();
        });
    });
});
