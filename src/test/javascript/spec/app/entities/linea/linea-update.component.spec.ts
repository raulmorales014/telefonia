/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { TelefoniaTestModule } from '../../../test.module';
import { LineaUpdateComponent } from 'app/entities/linea/linea-update.component';
import { LineaService } from 'app/entities/linea/linea.service';
import { Linea } from 'app/shared/model/linea.model';

describe('Component Tests', () => {
    describe('Linea Management Update Component', () => {
        let comp: LineaUpdateComponent;
        let fixture: ComponentFixture<LineaUpdateComponent>;
        let service: LineaService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [TelefoniaTestModule],
                declarations: [LineaUpdateComponent]
            })
                .overrideTemplate(LineaUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(LineaUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(LineaService);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity', fakeAsync(() => {
                // GIVEN
                const entity = new Linea(123);
                spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                comp.linea = entity;
                // WHEN
                comp.save();
                tick(); // simulate async

                // THEN
                expect(service.update).toHaveBeenCalledWith(entity);
                expect(comp.isSaving).toEqual(false);
            }));

            it('Should call create service on save for new entity', fakeAsync(() => {
                // GIVEN
                const entity = new Linea();
                spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                comp.linea = entity;
                // WHEN
                comp.save();
                tick(); // simulate async

                // THEN
                expect(service.create).toHaveBeenCalledWith(entity);
                expect(comp.isSaving).toEqual(false);
            }));
        });
    });
});
