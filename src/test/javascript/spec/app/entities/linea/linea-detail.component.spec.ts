/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { TelefoniaTestModule } from '../../../test.module';
import { LineaDetailComponent } from 'app/entities/linea/linea-detail.component';
import { Linea } from 'app/shared/model/linea.model';

describe('Component Tests', () => {
    describe('Linea Management Detail Component', () => {
        let comp: LineaDetailComponent;
        let fixture: ComponentFixture<LineaDetailComponent>;
        const route = ({ data: of({ linea: new Linea(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [TelefoniaTestModule],
                declarations: [LineaDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(LineaDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(LineaDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.linea).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
