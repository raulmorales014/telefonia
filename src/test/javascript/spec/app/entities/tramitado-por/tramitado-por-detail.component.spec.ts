/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { TelefoniaTestModule } from '../../../test.module';
import { TramitadoPorDetailComponent } from 'app/entities/tramitado-por/tramitado-por-detail.component';
import { TramitadoPor } from 'app/shared/model/tramitado-por.model';

describe('Component Tests', () => {
    describe('TramitadoPor Management Detail Component', () => {
        let comp: TramitadoPorDetailComponent;
        let fixture: ComponentFixture<TramitadoPorDetailComponent>;
        const route = ({ data: of({ tramitadoPor: new TramitadoPor(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [TelefoniaTestModule],
                declarations: [TramitadoPorDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(TramitadoPorDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(TramitadoPorDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.tramitadoPor).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
