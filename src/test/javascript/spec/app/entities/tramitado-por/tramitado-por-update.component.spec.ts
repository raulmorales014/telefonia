/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { TelefoniaTestModule } from '../../../test.module';
import { TramitadoPorUpdateComponent } from 'app/entities/tramitado-por/tramitado-por-update.component';
import { TramitadoPorService } from 'app/entities/tramitado-por/tramitado-por.service';
import { TramitadoPor } from 'app/shared/model/tramitado-por.model';

describe('Component Tests', () => {
    describe('TramitadoPor Management Update Component', () => {
        let comp: TramitadoPorUpdateComponent;
        let fixture: ComponentFixture<TramitadoPorUpdateComponent>;
        let service: TramitadoPorService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [TelefoniaTestModule],
                declarations: [TramitadoPorUpdateComponent]
            })
                .overrideTemplate(TramitadoPorUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(TramitadoPorUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(TramitadoPorService);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity', fakeAsync(() => {
                // GIVEN
                const entity = new TramitadoPor(123);
                spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                comp.tramitadoPor = entity;
                // WHEN
                comp.save();
                tick(); // simulate async

                // THEN
                expect(service.update).toHaveBeenCalledWith(entity);
                expect(comp.isSaving).toEqual(false);
            }));

            it('Should call create service on save for new entity', fakeAsync(() => {
                // GIVEN
                const entity = new TramitadoPor();
                spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                comp.tramitadoPor = entity;
                // WHEN
                comp.save();
                tick(); // simulate async

                // THEN
                expect(service.create).toHaveBeenCalledWith(entity);
                expect(comp.isSaving).toEqual(false);
            }));
        });
    });
});
