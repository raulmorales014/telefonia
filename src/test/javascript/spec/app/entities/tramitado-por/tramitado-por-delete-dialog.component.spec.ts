/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { TelefoniaTestModule } from '../../../test.module';
import { TramitadoPorDeleteDialogComponent } from 'app/entities/tramitado-por/tramitado-por-delete-dialog.component';
import { TramitadoPorService } from 'app/entities/tramitado-por/tramitado-por.service';

describe('Component Tests', () => {
    describe('TramitadoPor Management Delete Component', () => {
        let comp: TramitadoPorDeleteDialogComponent;
        let fixture: ComponentFixture<TramitadoPorDeleteDialogComponent>;
        let service: TramitadoPorService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [TelefoniaTestModule],
                declarations: [TramitadoPorDeleteDialogComponent]
            })
                .overrideTemplate(TramitadoPorDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(TramitadoPorDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(TramitadoPorService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete', inject(
                [],
                fakeAsync(() => {
                    // GIVEN
                    spyOn(service, 'delete').and.returnValue(of({}));

                    // WHEN
                    comp.confirmDelete(123);
                    tick();

                    // THEN
                    expect(service.delete).toHaveBeenCalledWith(123);
                    expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                })
            ));
        });
    });
});
