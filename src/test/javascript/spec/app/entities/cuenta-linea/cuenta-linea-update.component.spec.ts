/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { TelefoniaTestModule } from '../../../test.module';
import { CuentaLineaUpdateComponent } from 'app/entities/cuenta-linea/cuenta-linea-update.component';
import { CuentaLineaService } from 'app/entities/cuenta-linea/cuenta-linea.service';
import { CuentaLinea } from 'app/shared/model/cuenta-linea.model';

describe('Component Tests', () => {
    describe('CuentaLinea Management Update Component', () => {
        let comp: CuentaLineaUpdateComponent;
        let fixture: ComponentFixture<CuentaLineaUpdateComponent>;
        let service: CuentaLineaService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [TelefoniaTestModule],
                declarations: [CuentaLineaUpdateComponent]
            })
                .overrideTemplate(CuentaLineaUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(CuentaLineaUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CuentaLineaService);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity', fakeAsync(() => {
                // GIVEN
                const entity = new CuentaLinea(123);
                spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                comp.cuentaLinea = entity;
                // WHEN
                comp.save();
                tick(); // simulate async

                // THEN
                expect(service.update).toHaveBeenCalledWith(entity);
                expect(comp.isSaving).toEqual(false);
            }));

            it('Should call create service on save for new entity', fakeAsync(() => {
                // GIVEN
                const entity = new CuentaLinea();
                spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                comp.cuentaLinea = entity;
                // WHEN
                comp.save();
                tick(); // simulate async

                // THEN
                expect(service.create).toHaveBeenCalledWith(entity);
                expect(comp.isSaving).toEqual(false);
            }));
        });
    });
});
