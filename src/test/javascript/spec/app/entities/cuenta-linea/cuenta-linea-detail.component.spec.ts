/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { TelefoniaTestModule } from '../../../test.module';
import { CuentaLineaDetailComponent } from 'app/entities/cuenta-linea/cuenta-linea-detail.component';
import { CuentaLinea } from 'app/shared/model/cuenta-linea.model';

describe('Component Tests', () => {
    describe('CuentaLinea Management Detail Component', () => {
        let comp: CuentaLineaDetailComponent;
        let fixture: ComponentFixture<CuentaLineaDetailComponent>;
        const route = ({ data: of({ cuentaLinea: new CuentaLinea(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [TelefoniaTestModule],
                declarations: [CuentaLineaDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(CuentaLineaDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(CuentaLineaDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.cuentaLinea).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
