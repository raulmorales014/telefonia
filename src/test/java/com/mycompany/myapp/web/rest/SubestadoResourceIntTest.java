package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.TelefoniaApp;

import com.mycompany.myapp.domain.Subestado;
import com.mycompany.myapp.repository.SubestadoRepository;
import com.mycompany.myapp.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;


import static com.mycompany.myapp.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the SubestadoResource REST controller.
 *
 * @see SubestadoResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TelefoniaApp.class)
public class SubestadoResourceIntTest {

    private static final String DEFAULT_NOMBRE = "AAAAAAAAAA";
    private static final String UPDATED_NOMBRE = "BBBBBBBBBB";

    @Autowired
    private SubestadoRepository subestadoRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restSubestadoMockMvc;

    private Subestado subestado;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SubestadoResource subestadoResource = new SubestadoResource(subestadoRepository);
        this.restSubestadoMockMvc = MockMvcBuilders.standaloneSetup(subestadoResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Subestado createEntity(EntityManager em) {
        Subestado subestado = new Subestado()
            .nombre(DEFAULT_NOMBRE);
        return subestado;
    }

    @Before
    public void initTest() {
        subestado = createEntity(em);
    }

    @Test
    @Transactional
    public void createSubestado() throws Exception {
        int databaseSizeBeforeCreate = subestadoRepository.findAll().size();

        // Create the Subestado
        restSubestadoMockMvc.perform(post("/api/subestados")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(subestado)))
            .andExpect(status().isCreated());

        // Validate the Subestado in the database
        List<Subestado> subestadoList = subestadoRepository.findAll();
        assertThat(subestadoList).hasSize(databaseSizeBeforeCreate + 1);
        Subestado testSubestado = subestadoList.get(subestadoList.size() - 1);
        assertThat(testSubestado.getNombre()).isEqualTo(DEFAULT_NOMBRE);
    }

    @Test
    @Transactional
    public void createSubestadoWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = subestadoRepository.findAll().size();

        // Create the Subestado with an existing ID
        subestado.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSubestadoMockMvc.perform(post("/api/subestados")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(subestado)))
            .andExpect(status().isBadRequest());

        // Validate the Subestado in the database
        List<Subestado> subestadoList = subestadoRepository.findAll();
        assertThat(subestadoList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllSubestados() throws Exception {
        // Initialize the database
        subestadoRepository.saveAndFlush(subestado);

        // Get all the subestadoList
        restSubestadoMockMvc.perform(get("/api/subestados?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(subestado.getId().intValue())))
            .andExpect(jsonPath("$.[*].nombre").value(hasItem(DEFAULT_NOMBRE.toString())));
    }
    
    @Test
    @Transactional
    public void getSubestado() throws Exception {
        // Initialize the database
        subestadoRepository.saveAndFlush(subestado);

        // Get the subestado
        restSubestadoMockMvc.perform(get("/api/subestados/{id}", subestado.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(subestado.getId().intValue()))
            .andExpect(jsonPath("$.nombre").value(DEFAULT_NOMBRE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingSubestado() throws Exception {
        // Get the subestado
        restSubestadoMockMvc.perform(get("/api/subestados/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSubestado() throws Exception {
        // Initialize the database
        subestadoRepository.saveAndFlush(subestado);

        int databaseSizeBeforeUpdate = subestadoRepository.findAll().size();

        // Update the subestado
        Subestado updatedSubestado = subestadoRepository.findById(subestado.getId()).get();
        // Disconnect from session so that the updates on updatedSubestado are not directly saved in db
        em.detach(updatedSubestado);
        updatedSubestado
            .nombre(UPDATED_NOMBRE);

        restSubestadoMockMvc.perform(put("/api/subestados")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedSubestado)))
            .andExpect(status().isOk());

        // Validate the Subestado in the database
        List<Subestado> subestadoList = subestadoRepository.findAll();
        assertThat(subestadoList).hasSize(databaseSizeBeforeUpdate);
        Subestado testSubestado = subestadoList.get(subestadoList.size() - 1);
        assertThat(testSubestado.getNombre()).isEqualTo(UPDATED_NOMBRE);
    }

    @Test
    @Transactional
    public void updateNonExistingSubestado() throws Exception {
        int databaseSizeBeforeUpdate = subestadoRepository.findAll().size();

        // Create the Subestado

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSubestadoMockMvc.perform(put("/api/subestados")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(subestado)))
            .andExpect(status().isBadRequest());

        // Validate the Subestado in the database
        List<Subestado> subestadoList = subestadoRepository.findAll();
        assertThat(subestadoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteSubestado() throws Exception {
        // Initialize the database
        subestadoRepository.saveAndFlush(subestado);

        int databaseSizeBeforeDelete = subestadoRepository.findAll().size();

        // Delete the subestado
        restSubestadoMockMvc.perform(delete("/api/subestados/{id}", subestado.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Subestado> subestadoList = subestadoRepository.findAll();
        assertThat(subestadoList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Subestado.class);
        Subestado subestado1 = new Subestado();
        subestado1.setId(1L);
        Subestado subestado2 = new Subestado();
        subestado2.setId(subestado1.getId());
        assertThat(subestado1).isEqualTo(subestado2);
        subestado2.setId(2L);
        assertThat(subestado1).isNotEqualTo(subestado2);
        subestado1.setId(null);
        assertThat(subestado1).isNotEqualTo(subestado2);
    }
}
