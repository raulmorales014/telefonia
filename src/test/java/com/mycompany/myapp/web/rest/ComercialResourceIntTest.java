package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.TelefoniaApp;

import com.mycompany.myapp.domain.Comercial;
import com.mycompany.myapp.repository.ComercialRepository;
import com.mycompany.myapp.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;


import static com.mycompany.myapp.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ComercialResource REST controller.
 *
 * @see ComercialResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TelefoniaApp.class)
public class ComercialResourceIntTest {

    private static final String DEFAULT_NOMBRE = "AAAAAAAAAA";
    private static final String UPDATED_NOMBRE = "BBBBBBBBBB";

    private static final String DEFAULT_APELLIDOS = "AAAAAAAAAA";
    private static final String UPDATED_APELLIDOS = "BBBBBBBBBB";

    private static final String DEFAULT_DNI = "AAAAAAAAAA";
    private static final String UPDATED_DNI = "BBBBBBBBBB";

    private static final String DEFAULT_SFID = "AAAAAAAAAA";
    private static final String UPDATED_SFID = "BBBBBBBBBB";

    @Autowired
    private ComercialRepository comercialRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restComercialMockMvc;

    private Comercial comercial;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ComercialResource comercialResource = new ComercialResource(comercialRepository);
        this.restComercialMockMvc = MockMvcBuilders.standaloneSetup(comercialResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Comercial createEntity(EntityManager em) {
        Comercial comercial = new Comercial()
            .nombre(DEFAULT_NOMBRE)
            .apellidos(DEFAULT_APELLIDOS)
            .dni(DEFAULT_DNI)
            .sfid(DEFAULT_SFID);
        return comercial;
    }

    @Before
    public void initTest() {
        comercial = createEntity(em);
    }

    @Test
    @Transactional
    public void createComercial() throws Exception {
        int databaseSizeBeforeCreate = comercialRepository.findAll().size();

        // Create the Comercial
        restComercialMockMvc.perform(post("/api/comercials")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(comercial)))
            .andExpect(status().isCreated());

        // Validate the Comercial in the database
        List<Comercial> comercialList = comercialRepository.findAll();
        assertThat(comercialList).hasSize(databaseSizeBeforeCreate + 1);
        Comercial testComercial = comercialList.get(comercialList.size() - 1);
        assertThat(testComercial.getNombre()).isEqualTo(DEFAULT_NOMBRE);
        assertThat(testComercial.getApellidos()).isEqualTo(DEFAULT_APELLIDOS);
        assertThat(testComercial.getDni()).isEqualTo(DEFAULT_DNI);
        assertThat(testComercial.getSfid()).isEqualTo(DEFAULT_SFID);
    }

    @Test
    @Transactional
    public void createComercialWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = comercialRepository.findAll().size();

        // Create the Comercial with an existing ID
        comercial.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restComercialMockMvc.perform(post("/api/comercials")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(comercial)))
            .andExpect(status().isBadRequest());

        // Validate the Comercial in the database
        List<Comercial> comercialList = comercialRepository.findAll();
        assertThat(comercialList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllComercials() throws Exception {
        // Initialize the database
        comercialRepository.saveAndFlush(comercial);

        // Get all the comercialList
        restComercialMockMvc.perform(get("/api/comercials?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(comercial.getId().intValue())))
            .andExpect(jsonPath("$.[*].nombre").value(hasItem(DEFAULT_NOMBRE.toString())))
            .andExpect(jsonPath("$.[*].apellidos").value(hasItem(DEFAULT_APELLIDOS.toString())))
            .andExpect(jsonPath("$.[*].dni").value(hasItem(DEFAULT_DNI.toString())))
            .andExpect(jsonPath("$.[*].sfid").value(hasItem(DEFAULT_SFID.toString())));
    }
    
    @Test
    @Transactional
    public void getComercial() throws Exception {
        // Initialize the database
        comercialRepository.saveAndFlush(comercial);

        // Get the comercial
        restComercialMockMvc.perform(get("/api/comercials/{id}", comercial.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(comercial.getId().intValue()))
            .andExpect(jsonPath("$.nombre").value(DEFAULT_NOMBRE.toString()))
            .andExpect(jsonPath("$.apellidos").value(DEFAULT_APELLIDOS.toString()))
            .andExpect(jsonPath("$.dni").value(DEFAULT_DNI.toString()))
            .andExpect(jsonPath("$.sfid").value(DEFAULT_SFID.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingComercial() throws Exception {
        // Get the comercial
        restComercialMockMvc.perform(get("/api/comercials/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateComercial() throws Exception {
        // Initialize the database
        comercialRepository.saveAndFlush(comercial);

        int databaseSizeBeforeUpdate = comercialRepository.findAll().size();

        // Update the comercial
        Comercial updatedComercial = comercialRepository.findById(comercial.getId()).get();
        // Disconnect from session so that the updates on updatedComercial are not directly saved in db
        em.detach(updatedComercial);
        updatedComercial
            .nombre(UPDATED_NOMBRE)
            .apellidos(UPDATED_APELLIDOS)
            .dni(UPDATED_DNI)
            .sfid(UPDATED_SFID);

        restComercialMockMvc.perform(put("/api/comercials")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedComercial)))
            .andExpect(status().isOk());

        // Validate the Comercial in the database
        List<Comercial> comercialList = comercialRepository.findAll();
        assertThat(comercialList).hasSize(databaseSizeBeforeUpdate);
        Comercial testComercial = comercialList.get(comercialList.size() - 1);
        assertThat(testComercial.getNombre()).isEqualTo(UPDATED_NOMBRE);
        assertThat(testComercial.getApellidos()).isEqualTo(UPDATED_APELLIDOS);
        assertThat(testComercial.getDni()).isEqualTo(UPDATED_DNI);
        assertThat(testComercial.getSfid()).isEqualTo(UPDATED_SFID);
    }

    @Test
    @Transactional
    public void updateNonExistingComercial() throws Exception {
        int databaseSizeBeforeUpdate = comercialRepository.findAll().size();

        // Create the Comercial

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restComercialMockMvc.perform(put("/api/comercials")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(comercial)))
            .andExpect(status().isBadRequest());

        // Validate the Comercial in the database
        List<Comercial> comercialList = comercialRepository.findAll();
        assertThat(comercialList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteComercial() throws Exception {
        // Initialize the database
        comercialRepository.saveAndFlush(comercial);

        int databaseSizeBeforeDelete = comercialRepository.findAll().size();

        // Delete the comercial
        restComercialMockMvc.perform(delete("/api/comercials/{id}", comercial.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Comercial> comercialList = comercialRepository.findAll();
        assertThat(comercialList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Comercial.class);
        Comercial comercial1 = new Comercial();
        comercial1.setId(1L);
        Comercial comercial2 = new Comercial();
        comercial2.setId(comercial1.getId());
        assertThat(comercial1).isEqualTo(comercial2);
        comercial2.setId(2L);
        assertThat(comercial1).isNotEqualTo(comercial2);
        comercial1.setId(null);
        assertThat(comercial1).isNotEqualTo(comercial2);
    }
}
