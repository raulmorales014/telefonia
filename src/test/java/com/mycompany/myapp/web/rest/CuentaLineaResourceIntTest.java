package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.TelefoniaApp;

import com.mycompany.myapp.domain.CuentaLinea;
import com.mycompany.myapp.repository.CuentaLineaRepository;
import com.mycompany.myapp.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;


import static com.mycompany.myapp.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the CuentaLineaResource REST controller.
 *
 * @see CuentaLineaResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TelefoniaApp.class)
public class CuentaLineaResourceIntTest {

    private static final String DEFAULT_NOMBRE = "AAAAAAAAAA";
    private static final String UPDATED_NOMBRE = "BBBBBBBBBB";

    @Autowired
    private CuentaLineaRepository cuentaLineaRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restCuentaLineaMockMvc;

    private CuentaLinea cuentaLinea;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final CuentaLineaResource cuentaLineaResource = new CuentaLineaResource(cuentaLineaRepository);
        this.restCuentaLineaMockMvc = MockMvcBuilders.standaloneSetup(cuentaLineaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CuentaLinea createEntity(EntityManager em) {
        CuentaLinea cuentaLinea = new CuentaLinea()
            .nombre(DEFAULT_NOMBRE);
        return cuentaLinea;
    }

    @Before
    public void initTest() {
        cuentaLinea = createEntity(em);
    }

    @Test
    @Transactional
    public void createCuentaLinea() throws Exception {
        int databaseSizeBeforeCreate = cuentaLineaRepository.findAll().size();

        // Create the CuentaLinea
        restCuentaLineaMockMvc.perform(post("/api/cuenta-lineas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cuentaLinea)))
            .andExpect(status().isCreated());

        // Validate the CuentaLinea in the database
        List<CuentaLinea> cuentaLineaList = cuentaLineaRepository.findAll();
        assertThat(cuentaLineaList).hasSize(databaseSizeBeforeCreate + 1);
        CuentaLinea testCuentaLinea = cuentaLineaList.get(cuentaLineaList.size() - 1);
        assertThat(testCuentaLinea.getNombre()).isEqualTo(DEFAULT_NOMBRE);
    }

    @Test
    @Transactional
    public void createCuentaLineaWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = cuentaLineaRepository.findAll().size();

        // Create the CuentaLinea with an existing ID
        cuentaLinea.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCuentaLineaMockMvc.perform(post("/api/cuenta-lineas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cuentaLinea)))
            .andExpect(status().isBadRequest());

        // Validate the CuentaLinea in the database
        List<CuentaLinea> cuentaLineaList = cuentaLineaRepository.findAll();
        assertThat(cuentaLineaList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllCuentaLineas() throws Exception {
        // Initialize the database
        cuentaLineaRepository.saveAndFlush(cuentaLinea);

        // Get all the cuentaLineaList
        restCuentaLineaMockMvc.perform(get("/api/cuenta-lineas?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(cuentaLinea.getId().intValue())))
            .andExpect(jsonPath("$.[*].nombre").value(hasItem(DEFAULT_NOMBRE.toString())));
    }
    
    @Test
    @Transactional
    public void getCuentaLinea() throws Exception {
        // Initialize the database
        cuentaLineaRepository.saveAndFlush(cuentaLinea);

        // Get the cuentaLinea
        restCuentaLineaMockMvc.perform(get("/api/cuenta-lineas/{id}", cuentaLinea.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(cuentaLinea.getId().intValue()))
            .andExpect(jsonPath("$.nombre").value(DEFAULT_NOMBRE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingCuentaLinea() throws Exception {
        // Get the cuentaLinea
        restCuentaLineaMockMvc.perform(get("/api/cuenta-lineas/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCuentaLinea() throws Exception {
        // Initialize the database
        cuentaLineaRepository.saveAndFlush(cuentaLinea);

        int databaseSizeBeforeUpdate = cuentaLineaRepository.findAll().size();

        // Update the cuentaLinea
        CuentaLinea updatedCuentaLinea = cuentaLineaRepository.findById(cuentaLinea.getId()).get();
        // Disconnect from session so that the updates on updatedCuentaLinea are not directly saved in db
        em.detach(updatedCuentaLinea);
        updatedCuentaLinea
            .nombre(UPDATED_NOMBRE);

        restCuentaLineaMockMvc.perform(put("/api/cuenta-lineas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedCuentaLinea)))
            .andExpect(status().isOk());

        // Validate the CuentaLinea in the database
        List<CuentaLinea> cuentaLineaList = cuentaLineaRepository.findAll();
        assertThat(cuentaLineaList).hasSize(databaseSizeBeforeUpdate);
        CuentaLinea testCuentaLinea = cuentaLineaList.get(cuentaLineaList.size() - 1);
        assertThat(testCuentaLinea.getNombre()).isEqualTo(UPDATED_NOMBRE);
    }

    @Test
    @Transactional
    public void updateNonExistingCuentaLinea() throws Exception {
        int databaseSizeBeforeUpdate = cuentaLineaRepository.findAll().size();

        // Create the CuentaLinea

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCuentaLineaMockMvc.perform(put("/api/cuenta-lineas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cuentaLinea)))
            .andExpect(status().isBadRequest());

        // Validate the CuentaLinea in the database
        List<CuentaLinea> cuentaLineaList = cuentaLineaRepository.findAll();
        assertThat(cuentaLineaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteCuentaLinea() throws Exception {
        // Initialize the database
        cuentaLineaRepository.saveAndFlush(cuentaLinea);

        int databaseSizeBeforeDelete = cuentaLineaRepository.findAll().size();

        // Delete the cuentaLinea
        restCuentaLineaMockMvc.perform(delete("/api/cuenta-lineas/{id}", cuentaLinea.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<CuentaLinea> cuentaLineaList = cuentaLineaRepository.findAll();
        assertThat(cuentaLineaList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CuentaLinea.class);
        CuentaLinea cuentaLinea1 = new CuentaLinea();
        cuentaLinea1.setId(1L);
        CuentaLinea cuentaLinea2 = new CuentaLinea();
        cuentaLinea2.setId(cuentaLinea1.getId());
        assertThat(cuentaLinea1).isEqualTo(cuentaLinea2);
        cuentaLinea2.setId(2L);
        assertThat(cuentaLinea1).isNotEqualTo(cuentaLinea2);
        cuentaLinea1.setId(null);
        assertThat(cuentaLinea1).isNotEqualTo(cuentaLinea2);
    }
}
