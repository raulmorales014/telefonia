package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.TelefoniaApp;

import com.mycompany.myapp.domain.EstadoPorcentual;
import com.mycompany.myapp.repository.EstadoPorcentualRepository;
import com.mycompany.myapp.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;


import static com.mycompany.myapp.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the EstadoPorcentualResource REST controller.
 *
 * @see EstadoPorcentualResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TelefoniaApp.class)
public class EstadoPorcentualResourceIntTest {

    private static final String DEFAULT_NOMBRE = "AAAAAAAAAA";
    private static final String UPDATED_NOMBRE = "BBBBBBBBBB";

    @Autowired
    private EstadoPorcentualRepository estadoPorcentualRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restEstadoPorcentualMockMvc;

    private EstadoPorcentual estadoPorcentual;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final EstadoPorcentualResource estadoPorcentualResource = new EstadoPorcentualResource(estadoPorcentualRepository);
        this.restEstadoPorcentualMockMvc = MockMvcBuilders.standaloneSetup(estadoPorcentualResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static EstadoPorcentual createEntity(EntityManager em) {
        EstadoPorcentual estadoPorcentual = new EstadoPorcentual()
            .nombre(DEFAULT_NOMBRE);
        return estadoPorcentual;
    }

    @Before
    public void initTest() {
        estadoPorcentual = createEntity(em);
    }

    @Test
    @Transactional
    public void createEstadoPorcentual() throws Exception {
        int databaseSizeBeforeCreate = estadoPorcentualRepository.findAll().size();

        // Create the EstadoPorcentual
        restEstadoPorcentualMockMvc.perform(post("/api/estado-porcentuals")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(estadoPorcentual)))
            .andExpect(status().isCreated());

        // Validate the EstadoPorcentual in the database
        List<EstadoPorcentual> estadoPorcentualList = estadoPorcentualRepository.findAll();
        assertThat(estadoPorcentualList).hasSize(databaseSizeBeforeCreate + 1);
        EstadoPorcentual testEstadoPorcentual = estadoPorcentualList.get(estadoPorcentualList.size() - 1);
        assertThat(testEstadoPorcentual.getNombre()).isEqualTo(DEFAULT_NOMBRE);
    }

    @Test
    @Transactional
    public void createEstadoPorcentualWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = estadoPorcentualRepository.findAll().size();

        // Create the EstadoPorcentual with an existing ID
        estadoPorcentual.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restEstadoPorcentualMockMvc.perform(post("/api/estado-porcentuals")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(estadoPorcentual)))
            .andExpect(status().isBadRequest());

        // Validate the EstadoPorcentual in the database
        List<EstadoPorcentual> estadoPorcentualList = estadoPorcentualRepository.findAll();
        assertThat(estadoPorcentualList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllEstadoPorcentuals() throws Exception {
        // Initialize the database
        estadoPorcentualRepository.saveAndFlush(estadoPorcentual);

        // Get all the estadoPorcentualList
        restEstadoPorcentualMockMvc.perform(get("/api/estado-porcentuals?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(estadoPorcentual.getId().intValue())))
            .andExpect(jsonPath("$.[*].nombre").value(hasItem(DEFAULT_NOMBRE.toString())));
    }
    
    @Test
    @Transactional
    public void getEstadoPorcentual() throws Exception {
        // Initialize the database
        estadoPorcentualRepository.saveAndFlush(estadoPorcentual);

        // Get the estadoPorcentual
        restEstadoPorcentualMockMvc.perform(get("/api/estado-porcentuals/{id}", estadoPorcentual.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(estadoPorcentual.getId().intValue()))
            .andExpect(jsonPath("$.nombre").value(DEFAULT_NOMBRE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingEstadoPorcentual() throws Exception {
        // Get the estadoPorcentual
        restEstadoPorcentualMockMvc.perform(get("/api/estado-porcentuals/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateEstadoPorcentual() throws Exception {
        // Initialize the database
        estadoPorcentualRepository.saveAndFlush(estadoPorcentual);

        int databaseSizeBeforeUpdate = estadoPorcentualRepository.findAll().size();

        // Update the estadoPorcentual
        EstadoPorcentual updatedEstadoPorcentual = estadoPorcentualRepository.findById(estadoPorcentual.getId()).get();
        // Disconnect from session so that the updates on updatedEstadoPorcentual are not directly saved in db
        em.detach(updatedEstadoPorcentual);
        updatedEstadoPorcentual
            .nombre(UPDATED_NOMBRE);

        restEstadoPorcentualMockMvc.perform(put("/api/estado-porcentuals")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedEstadoPorcentual)))
            .andExpect(status().isOk());

        // Validate the EstadoPorcentual in the database
        List<EstadoPorcentual> estadoPorcentualList = estadoPorcentualRepository.findAll();
        assertThat(estadoPorcentualList).hasSize(databaseSizeBeforeUpdate);
        EstadoPorcentual testEstadoPorcentual = estadoPorcentualList.get(estadoPorcentualList.size() - 1);
        assertThat(testEstadoPorcentual.getNombre()).isEqualTo(UPDATED_NOMBRE);
    }

    @Test
    @Transactional
    public void updateNonExistingEstadoPorcentual() throws Exception {
        int databaseSizeBeforeUpdate = estadoPorcentualRepository.findAll().size();

        // Create the EstadoPorcentual

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restEstadoPorcentualMockMvc.perform(put("/api/estado-porcentuals")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(estadoPorcentual)))
            .andExpect(status().isBadRequest());

        // Validate the EstadoPorcentual in the database
        List<EstadoPorcentual> estadoPorcentualList = estadoPorcentualRepository.findAll();
        assertThat(estadoPorcentualList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteEstadoPorcentual() throws Exception {
        // Initialize the database
        estadoPorcentualRepository.saveAndFlush(estadoPorcentual);

        int databaseSizeBeforeDelete = estadoPorcentualRepository.findAll().size();

        // Delete the estadoPorcentual
        restEstadoPorcentualMockMvc.perform(delete("/api/estado-porcentuals/{id}", estadoPorcentual.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<EstadoPorcentual> estadoPorcentualList = estadoPorcentualRepository.findAll();
        assertThat(estadoPorcentualList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(EstadoPorcentual.class);
        EstadoPorcentual estadoPorcentual1 = new EstadoPorcentual();
        estadoPorcentual1.setId(1L);
        EstadoPorcentual estadoPorcentual2 = new EstadoPorcentual();
        estadoPorcentual2.setId(estadoPorcentual1.getId());
        assertThat(estadoPorcentual1).isEqualTo(estadoPorcentual2);
        estadoPorcentual2.setId(2L);
        assertThat(estadoPorcentual1).isNotEqualTo(estadoPorcentual2);
        estadoPorcentual1.setId(null);
        assertThat(estadoPorcentual1).isNotEqualTo(estadoPorcentual2);
    }
}
