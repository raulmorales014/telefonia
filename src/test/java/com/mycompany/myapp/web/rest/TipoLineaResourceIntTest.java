package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.TelefoniaApp;

import com.mycompany.myapp.domain.TipoLinea;
import com.mycompany.myapp.repository.TipoLineaRepository;
import com.mycompany.myapp.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;


import static com.mycompany.myapp.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the TipoLineaResource REST controller.
 *
 * @see TipoLineaResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TelefoniaApp.class)
public class TipoLineaResourceIntTest {

    private static final String DEFAULT_NOMBRE = "AAAAAAAAAA";
    private static final String UPDATED_NOMBRE = "BBBBBBBBBB";

    @Autowired
    private TipoLineaRepository tipoLineaRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restTipoLineaMockMvc;

    private TipoLinea tipoLinea;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TipoLineaResource tipoLineaResource = new TipoLineaResource(tipoLineaRepository);
        this.restTipoLineaMockMvc = MockMvcBuilders.standaloneSetup(tipoLineaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TipoLinea createEntity(EntityManager em) {
        TipoLinea tipoLinea = new TipoLinea()
            .nombre(DEFAULT_NOMBRE);
        return tipoLinea;
    }

    @Before
    public void initTest() {
        tipoLinea = createEntity(em);
    }

    @Test
    @Transactional
    public void createTipoLinea() throws Exception {
        int databaseSizeBeforeCreate = tipoLineaRepository.findAll().size();

        // Create the TipoLinea
        restTipoLineaMockMvc.perform(post("/api/tipo-lineas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipoLinea)))
            .andExpect(status().isCreated());

        // Validate the TipoLinea in the database
        List<TipoLinea> tipoLineaList = tipoLineaRepository.findAll();
        assertThat(tipoLineaList).hasSize(databaseSizeBeforeCreate + 1);
        TipoLinea testTipoLinea = tipoLineaList.get(tipoLineaList.size() - 1);
        assertThat(testTipoLinea.getNombre()).isEqualTo(DEFAULT_NOMBRE);
    }

    @Test
    @Transactional
    public void createTipoLineaWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = tipoLineaRepository.findAll().size();

        // Create the TipoLinea with an existing ID
        tipoLinea.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTipoLineaMockMvc.perform(post("/api/tipo-lineas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipoLinea)))
            .andExpect(status().isBadRequest());

        // Validate the TipoLinea in the database
        List<TipoLinea> tipoLineaList = tipoLineaRepository.findAll();
        assertThat(tipoLineaList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllTipoLineas() throws Exception {
        // Initialize the database
        tipoLineaRepository.saveAndFlush(tipoLinea);

        // Get all the tipoLineaList
        restTipoLineaMockMvc.perform(get("/api/tipo-lineas?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tipoLinea.getId().intValue())))
            .andExpect(jsonPath("$.[*].nombre").value(hasItem(DEFAULT_NOMBRE.toString())));
    }
    
    @Test
    @Transactional
    public void getTipoLinea() throws Exception {
        // Initialize the database
        tipoLineaRepository.saveAndFlush(tipoLinea);

        // Get the tipoLinea
        restTipoLineaMockMvc.perform(get("/api/tipo-lineas/{id}", tipoLinea.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(tipoLinea.getId().intValue()))
            .andExpect(jsonPath("$.nombre").value(DEFAULT_NOMBRE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingTipoLinea() throws Exception {
        // Get the tipoLinea
        restTipoLineaMockMvc.perform(get("/api/tipo-lineas/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTipoLinea() throws Exception {
        // Initialize the database
        tipoLineaRepository.saveAndFlush(tipoLinea);

        int databaseSizeBeforeUpdate = tipoLineaRepository.findAll().size();

        // Update the tipoLinea
        TipoLinea updatedTipoLinea = tipoLineaRepository.findById(tipoLinea.getId()).get();
        // Disconnect from session so that the updates on updatedTipoLinea are not directly saved in db
        em.detach(updatedTipoLinea);
        updatedTipoLinea
            .nombre(UPDATED_NOMBRE);

        restTipoLineaMockMvc.perform(put("/api/tipo-lineas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTipoLinea)))
            .andExpect(status().isOk());

        // Validate the TipoLinea in the database
        List<TipoLinea> tipoLineaList = tipoLineaRepository.findAll();
        assertThat(tipoLineaList).hasSize(databaseSizeBeforeUpdate);
        TipoLinea testTipoLinea = tipoLineaList.get(tipoLineaList.size() - 1);
        assertThat(testTipoLinea.getNombre()).isEqualTo(UPDATED_NOMBRE);
    }

    @Test
    @Transactional
    public void updateNonExistingTipoLinea() throws Exception {
        int databaseSizeBeforeUpdate = tipoLineaRepository.findAll().size();

        // Create the TipoLinea

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTipoLineaMockMvc.perform(put("/api/tipo-lineas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipoLinea)))
            .andExpect(status().isBadRequest());

        // Validate the TipoLinea in the database
        List<TipoLinea> tipoLineaList = tipoLineaRepository.findAll();
        assertThat(tipoLineaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteTipoLinea() throws Exception {
        // Initialize the database
        tipoLineaRepository.saveAndFlush(tipoLinea);

        int databaseSizeBeforeDelete = tipoLineaRepository.findAll().size();

        // Delete the tipoLinea
        restTipoLineaMockMvc.perform(delete("/api/tipo-lineas/{id}", tipoLinea.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<TipoLinea> tipoLineaList = tipoLineaRepository.findAll();
        assertThat(tipoLineaList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TipoLinea.class);
        TipoLinea tipoLinea1 = new TipoLinea();
        tipoLinea1.setId(1L);
        TipoLinea tipoLinea2 = new TipoLinea();
        tipoLinea2.setId(tipoLinea1.getId());
        assertThat(tipoLinea1).isEqualTo(tipoLinea2);
        tipoLinea2.setId(2L);
        assertThat(tipoLinea1).isNotEqualTo(tipoLinea2);
        tipoLinea1.setId(null);
        assertThat(tipoLinea1).isNotEqualTo(tipoLinea2);
    }
}
