package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.TelefoniaApp;

import com.mycompany.myapp.domain.TramitadoPor;
import com.mycompany.myapp.repository.TramitadoPorRepository;
import com.mycompany.myapp.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;


import static com.mycompany.myapp.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the TramitadoPorResource REST controller.
 *
 * @see TramitadoPorResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TelefoniaApp.class)
public class TramitadoPorResourceIntTest {

    private static final String DEFAULT_NOMBRE = "AAAAAAAAAA";
    private static final String UPDATED_NOMBRE = "BBBBBBBBBB";

    @Autowired
    private TramitadoPorRepository tramitadoPorRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restTramitadoPorMockMvc;

    private TramitadoPor tramitadoPor;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TramitadoPorResource tramitadoPorResource = new TramitadoPorResource(tramitadoPorRepository);
        this.restTramitadoPorMockMvc = MockMvcBuilders.standaloneSetup(tramitadoPorResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TramitadoPor createEntity(EntityManager em) {
        TramitadoPor tramitadoPor = new TramitadoPor()
            .nombre(DEFAULT_NOMBRE);
        return tramitadoPor;
    }

    @Before
    public void initTest() {
        tramitadoPor = createEntity(em);
    }

    @Test
    @Transactional
    public void createTramitadoPor() throws Exception {
        int databaseSizeBeforeCreate = tramitadoPorRepository.findAll().size();

        // Create the TramitadoPor
        restTramitadoPorMockMvc.perform(post("/api/tramitado-pors")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tramitadoPor)))
            .andExpect(status().isCreated());

        // Validate the TramitadoPor in the database
        List<TramitadoPor> tramitadoPorList = tramitadoPorRepository.findAll();
        assertThat(tramitadoPorList).hasSize(databaseSizeBeforeCreate + 1);
        TramitadoPor testTramitadoPor = tramitadoPorList.get(tramitadoPorList.size() - 1);
        assertThat(testTramitadoPor.getNombre()).isEqualTo(DEFAULT_NOMBRE);
    }

    @Test
    @Transactional
    public void createTramitadoPorWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = tramitadoPorRepository.findAll().size();

        // Create the TramitadoPor with an existing ID
        tramitadoPor.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTramitadoPorMockMvc.perform(post("/api/tramitado-pors")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tramitadoPor)))
            .andExpect(status().isBadRequest());

        // Validate the TramitadoPor in the database
        List<TramitadoPor> tramitadoPorList = tramitadoPorRepository.findAll();
        assertThat(tramitadoPorList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllTramitadoPors() throws Exception {
        // Initialize the database
        tramitadoPorRepository.saveAndFlush(tramitadoPor);

        // Get all the tramitadoPorList
        restTramitadoPorMockMvc.perform(get("/api/tramitado-pors?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tramitadoPor.getId().intValue())))
            .andExpect(jsonPath("$.[*].nombre").value(hasItem(DEFAULT_NOMBRE.toString())));
    }
    
    @Test
    @Transactional
    public void getTramitadoPor() throws Exception {
        // Initialize the database
        tramitadoPorRepository.saveAndFlush(tramitadoPor);

        // Get the tramitadoPor
        restTramitadoPorMockMvc.perform(get("/api/tramitado-pors/{id}", tramitadoPor.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(tramitadoPor.getId().intValue()))
            .andExpect(jsonPath("$.nombre").value(DEFAULT_NOMBRE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingTramitadoPor() throws Exception {
        // Get the tramitadoPor
        restTramitadoPorMockMvc.perform(get("/api/tramitado-pors/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTramitadoPor() throws Exception {
        // Initialize the database
        tramitadoPorRepository.saveAndFlush(tramitadoPor);

        int databaseSizeBeforeUpdate = tramitadoPorRepository.findAll().size();

        // Update the tramitadoPor
        TramitadoPor updatedTramitadoPor = tramitadoPorRepository.findById(tramitadoPor.getId()).get();
        // Disconnect from session so that the updates on updatedTramitadoPor are not directly saved in db
        em.detach(updatedTramitadoPor);
        updatedTramitadoPor
            .nombre(UPDATED_NOMBRE);

        restTramitadoPorMockMvc.perform(put("/api/tramitado-pors")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTramitadoPor)))
            .andExpect(status().isOk());

        // Validate the TramitadoPor in the database
        List<TramitadoPor> tramitadoPorList = tramitadoPorRepository.findAll();
        assertThat(tramitadoPorList).hasSize(databaseSizeBeforeUpdate);
        TramitadoPor testTramitadoPor = tramitadoPorList.get(tramitadoPorList.size() - 1);
        assertThat(testTramitadoPor.getNombre()).isEqualTo(UPDATED_NOMBRE);
    }

    @Test
    @Transactional
    public void updateNonExistingTramitadoPor() throws Exception {
        int databaseSizeBeforeUpdate = tramitadoPorRepository.findAll().size();

        // Create the TramitadoPor

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTramitadoPorMockMvc.perform(put("/api/tramitado-pors")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tramitadoPor)))
            .andExpect(status().isBadRequest());

        // Validate the TramitadoPor in the database
        List<TramitadoPor> tramitadoPorList = tramitadoPorRepository.findAll();
        assertThat(tramitadoPorList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteTramitadoPor() throws Exception {
        // Initialize the database
        tramitadoPorRepository.saveAndFlush(tramitadoPor);

        int databaseSizeBeforeDelete = tramitadoPorRepository.findAll().size();

        // Delete the tramitadoPor
        restTramitadoPorMockMvc.perform(delete("/api/tramitado-pors/{id}", tramitadoPor.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<TramitadoPor> tramitadoPorList = tramitadoPorRepository.findAll();
        assertThat(tramitadoPorList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TramitadoPor.class);
        TramitadoPor tramitadoPor1 = new TramitadoPor();
        tramitadoPor1.setId(1L);
        TramitadoPor tramitadoPor2 = new TramitadoPor();
        tramitadoPor2.setId(tramitadoPor1.getId());
        assertThat(tramitadoPor1).isEqualTo(tramitadoPor2);
        tramitadoPor2.setId(2L);
        assertThat(tramitadoPor1).isNotEqualTo(tramitadoPor2);
        tramitadoPor1.setId(null);
        assertThat(tramitadoPor1).isNotEqualTo(tramitadoPor2);
    }
}
