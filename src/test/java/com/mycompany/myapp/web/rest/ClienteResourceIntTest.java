package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.TelefoniaApp;

import com.mycompany.myapp.domain.Cliente;
import com.mycompany.myapp.repository.ClienteRepository;
import com.mycompany.myapp.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;


import static com.mycompany.myapp.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ClienteResource REST controller.
 *
 * @see ClienteResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TelefoniaApp.class)
public class ClienteResourceIntTest {

    private static final String DEFAULT_NOMBRE = "AAAAAAAAAA";
    private static final String UPDATED_NOMBRE = "BBBBBBBBBB";

    private static final String DEFAULT_CIF = "AAAAAAAAAA";
    private static final String UPDATED_CIF = "BBBBBBBBBB";

    private static final String DEFAULT_APODERADO = "AAAAAAAAAA";
    private static final String UPDATED_APODERADO = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_NACIMIENTO = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_NACIMIENTO = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_DNI = "AAAAAAAAAA";
    private static final String UPDATED_DNI = "BBBBBBBBBB";

    private static final String DEFAULT_PERSONA_CONTACTO = "AAAAAAAAAA";
    private static final String UPDATED_PERSONA_CONTACTO = "BBBBBBBBBB";

    private static final String DEFAULT_CORREO = "AAAAAAAAAA";
    private static final String UPDATED_CORREO = "BBBBBBBBBB";

    private static final Long DEFAULT_TELEFONO = 1L;
    private static final Long UPDATED_TELEFONO = 2L;

    private static final String DEFAULT_DIRECCION = "AAAAAAAAAA";
    private static final String UPDATED_DIRECCION = "BBBBBBBBBB";

    private static final String DEFAULT_LOCALIDAD = "AAAAAAAAAA";
    private static final String UPDATED_LOCALIDAD = "BBBBBBBBBB";

    private static final String DEFAULT_PROVINCIA = "AAAAAAAAAA";
    private static final String UPDATED_PROVINCIA = "BBBBBBBBBB";

    private static final Long DEFAULT_CODIGO_POSTAL = 1L;
    private static final Long UPDATED_CODIGO_POSTAL = 2L;

    private static final String DEFAULT_CTA = "AAAAAAAAAA";
    private static final String UPDATED_CTA = "BBBBBBBBBB";

    private static final String DEFAULT_OBSERVACIONES = "AAAAAAAAAA";
    private static final String UPDATED_OBSERVACIONES = "BBBBBBBBBB";

    private static final Long DEFAULT_PENALIZACIONES_VF = 1L;
    private static final Long UPDATED_PENALIZACIONES_VF = 2L;

    private static final Long DEFAULT_PENALIZACIONES_UN_4_TEL = 1L;
    private static final Long UPDATED_PENALIZACIONES_UN_4_TEL = 2L;

    private static final Long DEFAULT_LIBERALIZACIONES = 1L;
    private static final Long UPDATED_LIBERALIZACIONES = 2L;

    private static final Long DEFAULT_COSTE_ALCLIENTE = 1L;
    private static final Long UPDATED_COSTE_ALCLIENTE = 2L;

    private static final Boolean DEFAULT_OCULTAR_COMERCIALES = false;
    private static final Boolean UPDATED_OCULTAR_COMERCIALES = true;

    @Autowired
    private ClienteRepository clienteRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restClienteMockMvc;

    private Cliente cliente;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ClienteResource clienteResource = new ClienteResource(clienteRepository);
        this.restClienteMockMvc = MockMvcBuilders.standaloneSetup(clienteResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Cliente createEntity(EntityManager em) {
        Cliente cliente = new Cliente()
            .nombre(DEFAULT_NOMBRE)
            .cif(DEFAULT_CIF)
            .apoderado(DEFAULT_APODERADO)
            .nacimiento(DEFAULT_NACIMIENTO)
            .dni(DEFAULT_DNI)
            .personaContacto(DEFAULT_PERSONA_CONTACTO)
            .correo(DEFAULT_CORREO)
            .telefono(DEFAULT_TELEFONO)
            .direccion(DEFAULT_DIRECCION)
            .localidad(DEFAULT_LOCALIDAD)
            .provincia(DEFAULT_PROVINCIA)
            .codigoPostal(DEFAULT_CODIGO_POSTAL)
            .cta(DEFAULT_CTA)
            .observaciones(DEFAULT_OBSERVACIONES)
            .penalizacionesVF(DEFAULT_PENALIZACIONES_VF)
            .penalizacionesUn4tel(DEFAULT_PENALIZACIONES_UN_4_TEL)
            .liberalizaciones(DEFAULT_LIBERALIZACIONES)
            .costeAlcliente(DEFAULT_COSTE_ALCLIENTE)
            .ocultarComerciales(DEFAULT_OCULTAR_COMERCIALES);
        return cliente;
    }

    @Before
    public void initTest() {
        cliente = createEntity(em);
    }

    @Test
    @Transactional
    public void createCliente() throws Exception {
        int databaseSizeBeforeCreate = clienteRepository.findAll().size();

        // Create the Cliente
        restClienteMockMvc.perform(post("/api/clientes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cliente)))
            .andExpect(status().isCreated());

        // Validate the Cliente in the database
        List<Cliente> clienteList = clienteRepository.findAll();
        assertThat(clienteList).hasSize(databaseSizeBeforeCreate + 1);
        Cliente testCliente = clienteList.get(clienteList.size() - 1);
        assertThat(testCliente.getNombre()).isEqualTo(DEFAULT_NOMBRE);
        assertThat(testCliente.getCif()).isEqualTo(DEFAULT_CIF);
        assertThat(testCliente.getApoderado()).isEqualTo(DEFAULT_APODERADO);
        assertThat(testCliente.getNacimiento()).isEqualTo(DEFAULT_NACIMIENTO);
        assertThat(testCliente.getDni()).isEqualTo(DEFAULT_DNI);
        assertThat(testCliente.getPersonaContacto()).isEqualTo(DEFAULT_PERSONA_CONTACTO);
        assertThat(testCliente.getCorreo()).isEqualTo(DEFAULT_CORREO);
        assertThat(testCliente.getTelefono()).isEqualTo(DEFAULT_TELEFONO);
        assertThat(testCliente.getDireccion()).isEqualTo(DEFAULT_DIRECCION);
        assertThat(testCliente.getLocalidad()).isEqualTo(DEFAULT_LOCALIDAD);
        assertThat(testCliente.getProvincia()).isEqualTo(DEFAULT_PROVINCIA);
        assertThat(testCliente.getCodigoPostal()).isEqualTo(DEFAULT_CODIGO_POSTAL);
        assertThat(testCliente.getCta()).isEqualTo(DEFAULT_CTA);
        assertThat(testCliente.getObservaciones()).isEqualTo(DEFAULT_OBSERVACIONES);
        assertThat(testCliente.getPenalizacionesVF()).isEqualTo(DEFAULT_PENALIZACIONES_VF);
        assertThat(testCliente.getPenalizacionesUn4tel()).isEqualTo(DEFAULT_PENALIZACIONES_UN_4_TEL);
        assertThat(testCliente.getLiberalizaciones()).isEqualTo(DEFAULT_LIBERALIZACIONES);
        assertThat(testCliente.getCosteAlcliente()).isEqualTo(DEFAULT_COSTE_ALCLIENTE);
        assertThat(testCliente.isOcultarComerciales()).isEqualTo(DEFAULT_OCULTAR_COMERCIALES);
    }

    @Test
    @Transactional
    public void createClienteWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = clienteRepository.findAll().size();

        // Create the Cliente with an existing ID
        cliente.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restClienteMockMvc.perform(post("/api/clientes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cliente)))
            .andExpect(status().isBadRequest());

        // Validate the Cliente in the database
        List<Cliente> clienteList = clienteRepository.findAll();
        assertThat(clienteList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNacimientoIsRequired() throws Exception {
        int databaseSizeBeforeTest = clienteRepository.findAll().size();
        // set the field null
        cliente.setNacimiento(null);

        // Create the Cliente, which fails.

        restClienteMockMvc.perform(post("/api/clientes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cliente)))
            .andExpect(status().isBadRequest());

        List<Cliente> clienteList = clienteRepository.findAll();
        assertThat(clienteList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllClientes() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList
        restClienteMockMvc.perform(get("/api/clientes?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(cliente.getId().intValue())))
            .andExpect(jsonPath("$.[*].nombre").value(hasItem(DEFAULT_NOMBRE.toString())))
            .andExpect(jsonPath("$.[*].cif").value(hasItem(DEFAULT_CIF.toString())))
            .andExpect(jsonPath("$.[*].apoderado").value(hasItem(DEFAULT_APODERADO.toString())))
            .andExpect(jsonPath("$.[*].nacimiento").value(hasItem(DEFAULT_NACIMIENTO.toString())))
            .andExpect(jsonPath("$.[*].dni").value(hasItem(DEFAULT_DNI.toString())))
            .andExpect(jsonPath("$.[*].personaContacto").value(hasItem(DEFAULT_PERSONA_CONTACTO.toString())))
            .andExpect(jsonPath("$.[*].correo").value(hasItem(DEFAULT_CORREO.toString())))
            .andExpect(jsonPath("$.[*].telefono").value(hasItem(DEFAULT_TELEFONO.intValue())))
            .andExpect(jsonPath("$.[*].direccion").value(hasItem(DEFAULT_DIRECCION.toString())))
            .andExpect(jsonPath("$.[*].localidad").value(hasItem(DEFAULT_LOCALIDAD.toString())))
            .andExpect(jsonPath("$.[*].provincia").value(hasItem(DEFAULT_PROVINCIA.toString())))
            .andExpect(jsonPath("$.[*].codigoPostal").value(hasItem(DEFAULT_CODIGO_POSTAL.intValue())))
            .andExpect(jsonPath("$.[*].cta").value(hasItem(DEFAULT_CTA.toString())))
            .andExpect(jsonPath("$.[*].observaciones").value(hasItem(DEFAULT_OBSERVACIONES.toString())))
            .andExpect(jsonPath("$.[*].penalizacionesVF").value(hasItem(DEFAULT_PENALIZACIONES_VF.intValue())))
            .andExpect(jsonPath("$.[*].penalizacionesUn4tel").value(hasItem(DEFAULT_PENALIZACIONES_UN_4_TEL.intValue())))
            .andExpect(jsonPath("$.[*].liberalizaciones").value(hasItem(DEFAULT_LIBERALIZACIONES.intValue())))
            .andExpect(jsonPath("$.[*].costeAlcliente").value(hasItem(DEFAULT_COSTE_ALCLIENTE.intValue())))
            .andExpect(jsonPath("$.[*].ocultarComerciales").value(hasItem(DEFAULT_OCULTAR_COMERCIALES.booleanValue())));
    }
    
    @Test
    @Transactional
    public void getCliente() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get the cliente
        restClienteMockMvc.perform(get("/api/clientes/{id}", cliente.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(cliente.getId().intValue()))
            .andExpect(jsonPath("$.nombre").value(DEFAULT_NOMBRE.toString()))
            .andExpect(jsonPath("$.cif").value(DEFAULT_CIF.toString()))
            .andExpect(jsonPath("$.apoderado").value(DEFAULT_APODERADO.toString()))
            .andExpect(jsonPath("$.nacimiento").value(DEFAULT_NACIMIENTO.toString()))
            .andExpect(jsonPath("$.dni").value(DEFAULT_DNI.toString()))
            .andExpect(jsonPath("$.personaContacto").value(DEFAULT_PERSONA_CONTACTO.toString()))
            .andExpect(jsonPath("$.correo").value(DEFAULT_CORREO.toString()))
            .andExpect(jsonPath("$.telefono").value(DEFAULT_TELEFONO.intValue()))
            .andExpect(jsonPath("$.direccion").value(DEFAULT_DIRECCION.toString()))
            .andExpect(jsonPath("$.localidad").value(DEFAULT_LOCALIDAD.toString()))
            .andExpect(jsonPath("$.provincia").value(DEFAULT_PROVINCIA.toString()))
            .andExpect(jsonPath("$.codigoPostal").value(DEFAULT_CODIGO_POSTAL.intValue()))
            .andExpect(jsonPath("$.cta").value(DEFAULT_CTA.toString()))
            .andExpect(jsonPath("$.observaciones").value(DEFAULT_OBSERVACIONES.toString()))
            .andExpect(jsonPath("$.penalizacionesVF").value(DEFAULT_PENALIZACIONES_VF.intValue()))
            .andExpect(jsonPath("$.penalizacionesUn4tel").value(DEFAULT_PENALIZACIONES_UN_4_TEL.intValue()))
            .andExpect(jsonPath("$.liberalizaciones").value(DEFAULT_LIBERALIZACIONES.intValue()))
            .andExpect(jsonPath("$.costeAlcliente").value(DEFAULT_COSTE_ALCLIENTE.intValue()))
            .andExpect(jsonPath("$.ocultarComerciales").value(DEFAULT_OCULTAR_COMERCIALES.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingCliente() throws Exception {
        // Get the cliente
        restClienteMockMvc.perform(get("/api/clientes/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCliente() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        int databaseSizeBeforeUpdate = clienteRepository.findAll().size();

        // Update the cliente
        Cliente updatedCliente = clienteRepository.findById(cliente.getId()).get();
        // Disconnect from session so that the updates on updatedCliente are not directly saved in db
        em.detach(updatedCliente);
        updatedCliente
            .nombre(UPDATED_NOMBRE)
            .cif(UPDATED_CIF)
            .apoderado(UPDATED_APODERADO)
            .nacimiento(UPDATED_NACIMIENTO)
            .dni(UPDATED_DNI)
            .personaContacto(UPDATED_PERSONA_CONTACTO)
            .correo(UPDATED_CORREO)
            .telefono(UPDATED_TELEFONO)
            .direccion(UPDATED_DIRECCION)
            .localidad(UPDATED_LOCALIDAD)
            .provincia(UPDATED_PROVINCIA)
            .codigoPostal(UPDATED_CODIGO_POSTAL)
            .cta(UPDATED_CTA)
            .observaciones(UPDATED_OBSERVACIONES)
            .penalizacionesVF(UPDATED_PENALIZACIONES_VF)
            .penalizacionesUn4tel(UPDATED_PENALIZACIONES_UN_4_TEL)
            .liberalizaciones(UPDATED_LIBERALIZACIONES)
            .costeAlcliente(UPDATED_COSTE_ALCLIENTE)
            .ocultarComerciales(UPDATED_OCULTAR_COMERCIALES);

        restClienteMockMvc.perform(put("/api/clientes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedCliente)))
            .andExpect(status().isOk());

        // Validate the Cliente in the database
        List<Cliente> clienteList = clienteRepository.findAll();
        assertThat(clienteList).hasSize(databaseSizeBeforeUpdate);
        Cliente testCliente = clienteList.get(clienteList.size() - 1);
        assertThat(testCliente.getNombre()).isEqualTo(UPDATED_NOMBRE);
        assertThat(testCliente.getCif()).isEqualTo(UPDATED_CIF);
        assertThat(testCliente.getApoderado()).isEqualTo(UPDATED_APODERADO);
        assertThat(testCliente.getNacimiento()).isEqualTo(UPDATED_NACIMIENTO);
        assertThat(testCliente.getDni()).isEqualTo(UPDATED_DNI);
        assertThat(testCliente.getPersonaContacto()).isEqualTo(UPDATED_PERSONA_CONTACTO);
        assertThat(testCliente.getCorreo()).isEqualTo(UPDATED_CORREO);
        assertThat(testCliente.getTelefono()).isEqualTo(UPDATED_TELEFONO);
        assertThat(testCliente.getDireccion()).isEqualTo(UPDATED_DIRECCION);
        assertThat(testCliente.getLocalidad()).isEqualTo(UPDATED_LOCALIDAD);
        assertThat(testCliente.getProvincia()).isEqualTo(UPDATED_PROVINCIA);
        assertThat(testCliente.getCodigoPostal()).isEqualTo(UPDATED_CODIGO_POSTAL);
        assertThat(testCliente.getCta()).isEqualTo(UPDATED_CTA);
        assertThat(testCliente.getObservaciones()).isEqualTo(UPDATED_OBSERVACIONES);
        assertThat(testCliente.getPenalizacionesVF()).isEqualTo(UPDATED_PENALIZACIONES_VF);
        assertThat(testCliente.getPenalizacionesUn4tel()).isEqualTo(UPDATED_PENALIZACIONES_UN_4_TEL);
        assertThat(testCliente.getLiberalizaciones()).isEqualTo(UPDATED_LIBERALIZACIONES);
        assertThat(testCliente.getCosteAlcliente()).isEqualTo(UPDATED_COSTE_ALCLIENTE);
        assertThat(testCliente.isOcultarComerciales()).isEqualTo(UPDATED_OCULTAR_COMERCIALES);
    }

    @Test
    @Transactional
    public void updateNonExistingCliente() throws Exception {
        int databaseSizeBeforeUpdate = clienteRepository.findAll().size();

        // Create the Cliente

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restClienteMockMvc.perform(put("/api/clientes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cliente)))
            .andExpect(status().isBadRequest());

        // Validate the Cliente in the database
        List<Cliente> clienteList = clienteRepository.findAll();
        assertThat(clienteList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteCliente() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        int databaseSizeBeforeDelete = clienteRepository.findAll().size();

        // Delete the cliente
        restClienteMockMvc.perform(delete("/api/clientes/{id}", cliente.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Cliente> clienteList = clienteRepository.findAll();
        assertThat(clienteList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Cliente.class);
        Cliente cliente1 = new Cliente();
        cliente1.setId(1L);
        Cliente cliente2 = new Cliente();
        cliente2.setId(cliente1.getId());
        assertThat(cliente1).isEqualTo(cliente2);
        cliente2.setId(2L);
        assertThat(cliente1).isNotEqualTo(cliente2);
        cliente1.setId(null);
        assertThat(cliente1).isNotEqualTo(cliente2);
    }
}
