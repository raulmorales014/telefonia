package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.TelefoniaApp;

import com.mycompany.myapp.domain.Permanencia;
import com.mycompany.myapp.repository.PermanenciaRepository;
import com.mycompany.myapp.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;


import static com.mycompany.myapp.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the PermanenciaResource REST controller.
 *
 * @see PermanenciaResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TelefoniaApp.class)
public class PermanenciaResourceIntTest {

    private static final String DEFAULT_NOMBRE = "AAAAAAAAAA";
    private static final String UPDATED_NOMBRE = "BBBBBBBBBB";

    @Autowired
    private PermanenciaRepository permanenciaRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restPermanenciaMockMvc;

    private Permanencia permanencia;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final PermanenciaResource permanenciaResource = new PermanenciaResource(permanenciaRepository);
        this.restPermanenciaMockMvc = MockMvcBuilders.standaloneSetup(permanenciaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Permanencia createEntity(EntityManager em) {
        Permanencia permanencia = new Permanencia()
            .nombre(DEFAULT_NOMBRE);
        return permanencia;
    }

    @Before
    public void initTest() {
        permanencia = createEntity(em);
    }

    @Test
    @Transactional
    public void createPermanencia() throws Exception {
        int databaseSizeBeforeCreate = permanenciaRepository.findAll().size();

        // Create the Permanencia
        restPermanenciaMockMvc.perform(post("/api/permanencias")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(permanencia)))
            .andExpect(status().isCreated());

        // Validate the Permanencia in the database
        List<Permanencia> permanenciaList = permanenciaRepository.findAll();
        assertThat(permanenciaList).hasSize(databaseSizeBeforeCreate + 1);
        Permanencia testPermanencia = permanenciaList.get(permanenciaList.size() - 1);
        assertThat(testPermanencia.getNombre()).isEqualTo(DEFAULT_NOMBRE);
    }

    @Test
    @Transactional
    public void createPermanenciaWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = permanenciaRepository.findAll().size();

        // Create the Permanencia with an existing ID
        permanencia.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPermanenciaMockMvc.perform(post("/api/permanencias")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(permanencia)))
            .andExpect(status().isBadRequest());

        // Validate the Permanencia in the database
        List<Permanencia> permanenciaList = permanenciaRepository.findAll();
        assertThat(permanenciaList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllPermanencias() throws Exception {
        // Initialize the database
        permanenciaRepository.saveAndFlush(permanencia);

        // Get all the permanenciaList
        restPermanenciaMockMvc.perform(get("/api/permanencias?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(permanencia.getId().intValue())))
            .andExpect(jsonPath("$.[*].nombre").value(hasItem(DEFAULT_NOMBRE.toString())));
    }
    
    @Test
    @Transactional
    public void getPermanencia() throws Exception {
        // Initialize the database
        permanenciaRepository.saveAndFlush(permanencia);

        // Get the permanencia
        restPermanenciaMockMvc.perform(get("/api/permanencias/{id}", permanencia.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(permanencia.getId().intValue()))
            .andExpect(jsonPath("$.nombre").value(DEFAULT_NOMBRE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingPermanencia() throws Exception {
        // Get the permanencia
        restPermanenciaMockMvc.perform(get("/api/permanencias/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePermanencia() throws Exception {
        // Initialize the database
        permanenciaRepository.saveAndFlush(permanencia);

        int databaseSizeBeforeUpdate = permanenciaRepository.findAll().size();

        // Update the permanencia
        Permanencia updatedPermanencia = permanenciaRepository.findById(permanencia.getId()).get();
        // Disconnect from session so that the updates on updatedPermanencia are not directly saved in db
        em.detach(updatedPermanencia);
        updatedPermanencia
            .nombre(UPDATED_NOMBRE);

        restPermanenciaMockMvc.perform(put("/api/permanencias")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedPermanencia)))
            .andExpect(status().isOk());

        // Validate the Permanencia in the database
        List<Permanencia> permanenciaList = permanenciaRepository.findAll();
        assertThat(permanenciaList).hasSize(databaseSizeBeforeUpdate);
        Permanencia testPermanencia = permanenciaList.get(permanenciaList.size() - 1);
        assertThat(testPermanencia.getNombre()).isEqualTo(UPDATED_NOMBRE);
    }

    @Test
    @Transactional
    public void updateNonExistingPermanencia() throws Exception {
        int databaseSizeBeforeUpdate = permanenciaRepository.findAll().size();

        // Create the Permanencia

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPermanenciaMockMvc.perform(put("/api/permanencias")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(permanencia)))
            .andExpect(status().isBadRequest());

        // Validate the Permanencia in the database
        List<Permanencia> permanenciaList = permanenciaRepository.findAll();
        assertThat(permanenciaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deletePermanencia() throws Exception {
        // Initialize the database
        permanenciaRepository.saveAndFlush(permanencia);

        int databaseSizeBeforeDelete = permanenciaRepository.findAll().size();

        // Delete the permanencia
        restPermanenciaMockMvc.perform(delete("/api/permanencias/{id}", permanencia.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Permanencia> permanenciaList = permanenciaRepository.findAll();
        assertThat(permanenciaList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Permanencia.class);
        Permanencia permanencia1 = new Permanencia();
        permanencia1.setId(1L);
        Permanencia permanencia2 = new Permanencia();
        permanencia2.setId(permanencia1.getId());
        assertThat(permanencia1).isEqualTo(permanencia2);
        permanencia2.setId(2L);
        assertThat(permanencia1).isNotEqualTo(permanencia2);
        permanencia1.setId(null);
        assertThat(permanencia1).isNotEqualTo(permanencia2);
    }
}
