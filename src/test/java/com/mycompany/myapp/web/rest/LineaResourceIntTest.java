package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.TelefoniaApp;

import com.mycompany.myapp.domain.Linea;
import com.mycompany.myapp.repository.LineaRepository;
import com.mycompany.myapp.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;


import static com.mycompany.myapp.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the LineaResource REST controller.
 *
 * @see LineaResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TelefoniaApp.class)
public class LineaResourceIntTest {

    private static final Long DEFAULT_LINEA = 1L;
    private static final Long UPDATED_LINEA = 2L;

    private static final Boolean DEFAULT_ES_NUEVA = false;
    private static final Boolean UPDATED_ES_NUEVA = true;

    private static final Long DEFAULT_DESCUENTO = 1L;
    private static final Long UPDATED_DESCUENTO = 2L;

    private static final Boolean DEFAULT_PORTA = false;
    private static final Boolean UPDATED_PORTA = true;

    private static final String DEFAULT_PORTA_NOMBRE = "AAAAAAAAAA";
    private static final String UPDATED_PORTA_NOMBRE = "BBBBBBBBBB";

    private static final String DEFAULT_PORTA_CIF = "AAAAAAAAAA";
    private static final String UPDATED_PORTA_CIF = "BBBBBBBBBB";

    private static final String DEFAULT_PORTA_OPERADOR = "AAAAAAAAAA";
    private static final String UPDATED_PORTA_OPERADOR = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_PROVISION = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_PROVISION = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_ACTIVACION = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_ACTIVACION = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_PAQUETE_NUMERO = "AAAAAAAAAA";
    private static final String UPDATED_PAQUETE_NUMERO = "BBBBBBBBBB";

    private static final String DEFAULT_TERMINAL = "AAAAAAAAAA";
    private static final String UPDATED_TERMINAL = "BBBBBBBBBB";

    private static final Float DEFAULT_PAGO_INICIAL = 1F;
    private static final Float UPDATED_PAGO_INICIAL = 2F;

    private static final Long DEFAULT_CUOTAS = 1L;
    private static final Long UPDATED_CUOTAS = 2L;

    private static final Float DEFAULT_PAGO_UNICO = 1F;
    private static final Float UPDATED_PAGO_UNICO = 2F;

    private static final String DEFAULT_OBSERVACION = "AAAAAAAAAA";
    private static final String UPDATED_OBSERVACION = "BBBBBBBBBB";

    private static final String DEFAULT_OBSERVACION_ADICIONAL = "AAAAAAAAAA";
    private static final String UPDATED_OBSERVACION_ADICIONAL = "BBBBBBBBBB";

    @Autowired
    private LineaRepository lineaRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restLineaMockMvc;

    private Linea linea;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final LineaResource lineaResource = new LineaResource(lineaRepository);
        this.restLineaMockMvc = MockMvcBuilders.standaloneSetup(lineaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Linea createEntity(EntityManager em) {
        Linea linea = new Linea()
            .linea(DEFAULT_LINEA)
            .esNueva(DEFAULT_ES_NUEVA)
            .descuento(DEFAULT_DESCUENTO)
            .porta(DEFAULT_PORTA)
            .portaNombre(DEFAULT_PORTA_NOMBRE)
            .portaCif(DEFAULT_PORTA_CIF)
            .portaOperador(DEFAULT_PORTA_OPERADOR)
            .provision(DEFAULT_PROVISION)
            .activacion(DEFAULT_ACTIVACION)
            .paqueteNumero(DEFAULT_PAQUETE_NUMERO)
            .terminal(DEFAULT_TERMINAL)
            .pagoInicial(DEFAULT_PAGO_INICIAL)
            .cuotas(DEFAULT_CUOTAS)
            .pagoUnico(DEFAULT_PAGO_UNICO)
            .observacion(DEFAULT_OBSERVACION)
            .observacionAdicional(DEFAULT_OBSERVACION_ADICIONAL);
        return linea;
    }

    @Before
    public void initTest() {
        linea = createEntity(em);
    }

    @Test
    @Transactional
    public void createLinea() throws Exception {
        int databaseSizeBeforeCreate = lineaRepository.findAll().size();

        // Create the Linea
        restLineaMockMvc.perform(post("/api/lineas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(linea)))
            .andExpect(status().isCreated());

        // Validate the Linea in the database
        List<Linea> lineaList = lineaRepository.findAll();
        assertThat(lineaList).hasSize(databaseSizeBeforeCreate + 1);
        Linea testLinea = lineaList.get(lineaList.size() - 1);
        assertThat(testLinea.getLinea()).isEqualTo(DEFAULT_LINEA);
        assertThat(testLinea.isEsNueva()).isEqualTo(DEFAULT_ES_NUEVA);
        assertThat(testLinea.getDescuento()).isEqualTo(DEFAULT_DESCUENTO);
        assertThat(testLinea.isPorta()).isEqualTo(DEFAULT_PORTA);
        assertThat(testLinea.getPortaNombre()).isEqualTo(DEFAULT_PORTA_NOMBRE);
        assertThat(testLinea.getPortaCif()).isEqualTo(DEFAULT_PORTA_CIF);
        assertThat(testLinea.getPortaOperador()).isEqualTo(DEFAULT_PORTA_OPERADOR);
        assertThat(testLinea.getProvision()).isEqualTo(DEFAULT_PROVISION);
        assertThat(testLinea.getActivacion()).isEqualTo(DEFAULT_ACTIVACION);
        assertThat(testLinea.getPaqueteNumero()).isEqualTo(DEFAULT_PAQUETE_NUMERO);
        assertThat(testLinea.getTerminal()).isEqualTo(DEFAULT_TERMINAL);
        assertThat(testLinea.getPagoInicial()).isEqualTo(DEFAULT_PAGO_INICIAL);
        assertThat(testLinea.getCuotas()).isEqualTo(DEFAULT_CUOTAS);
        assertThat(testLinea.getPagoUnico()).isEqualTo(DEFAULT_PAGO_UNICO);
        assertThat(testLinea.getObservacion()).isEqualTo(DEFAULT_OBSERVACION);
        assertThat(testLinea.getObservacionAdicional()).isEqualTo(DEFAULT_OBSERVACION_ADICIONAL);
    }

    @Test
    @Transactional
    public void createLineaWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = lineaRepository.findAll().size();

        // Create the Linea with an existing ID
        linea.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restLineaMockMvc.perform(post("/api/lineas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(linea)))
            .andExpect(status().isBadRequest());

        // Validate the Linea in the database
        List<Linea> lineaList = lineaRepository.findAll();
        assertThat(lineaList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllLineas() throws Exception {
        // Initialize the database
        lineaRepository.saveAndFlush(linea);

        // Get all the lineaList
        restLineaMockMvc.perform(get("/api/lineas?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(linea.getId().intValue())))
            .andExpect(jsonPath("$.[*].linea").value(hasItem(DEFAULT_LINEA.intValue())))
            .andExpect(jsonPath("$.[*].esNueva").value(hasItem(DEFAULT_ES_NUEVA.booleanValue())))
            .andExpect(jsonPath("$.[*].descuento").value(hasItem(DEFAULT_DESCUENTO.intValue())))
            .andExpect(jsonPath("$.[*].porta").value(hasItem(DEFAULT_PORTA.booleanValue())))
            .andExpect(jsonPath("$.[*].portaNombre").value(hasItem(DEFAULT_PORTA_NOMBRE.toString())))
            .andExpect(jsonPath("$.[*].portaCif").value(hasItem(DEFAULT_PORTA_CIF.toString())))
            .andExpect(jsonPath("$.[*].portaOperador").value(hasItem(DEFAULT_PORTA_OPERADOR.toString())))
            .andExpect(jsonPath("$.[*].provision").value(hasItem(DEFAULT_PROVISION.toString())))
            .andExpect(jsonPath("$.[*].activacion").value(hasItem(DEFAULT_ACTIVACION.toString())))
            .andExpect(jsonPath("$.[*].paqueteNumero").value(hasItem(DEFAULT_PAQUETE_NUMERO.toString())))
            .andExpect(jsonPath("$.[*].terminal").value(hasItem(DEFAULT_TERMINAL.toString())))
            .andExpect(jsonPath("$.[*].pagoInicial").value(hasItem(DEFAULT_PAGO_INICIAL.doubleValue())))
            .andExpect(jsonPath("$.[*].cuotas").value(hasItem(DEFAULT_CUOTAS.intValue())))
            .andExpect(jsonPath("$.[*].pagoUnico").value(hasItem(DEFAULT_PAGO_UNICO.doubleValue())))
            .andExpect(jsonPath("$.[*].observacion").value(hasItem(DEFAULT_OBSERVACION.toString())))
            .andExpect(jsonPath("$.[*].observacionAdicional").value(hasItem(DEFAULT_OBSERVACION_ADICIONAL.toString())));
    }
    
    @Test
    @Transactional
    public void getLinea() throws Exception {
        // Initialize the database
        lineaRepository.saveAndFlush(linea);

        // Get the linea
        restLineaMockMvc.perform(get("/api/lineas/{id}", linea.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(linea.getId().intValue()))
            .andExpect(jsonPath("$.linea").value(DEFAULT_LINEA.intValue()))
            .andExpect(jsonPath("$.esNueva").value(DEFAULT_ES_NUEVA.booleanValue()))
            .andExpect(jsonPath("$.descuento").value(DEFAULT_DESCUENTO.intValue()))
            .andExpect(jsonPath("$.porta").value(DEFAULT_PORTA.booleanValue()))
            .andExpect(jsonPath("$.portaNombre").value(DEFAULT_PORTA_NOMBRE.toString()))
            .andExpect(jsonPath("$.portaCif").value(DEFAULT_PORTA_CIF.toString()))
            .andExpect(jsonPath("$.portaOperador").value(DEFAULT_PORTA_OPERADOR.toString()))
            .andExpect(jsonPath("$.provision").value(DEFAULT_PROVISION.toString()))
            .andExpect(jsonPath("$.activacion").value(DEFAULT_ACTIVACION.toString()))
            .andExpect(jsonPath("$.paqueteNumero").value(DEFAULT_PAQUETE_NUMERO.toString()))
            .andExpect(jsonPath("$.terminal").value(DEFAULT_TERMINAL.toString()))
            .andExpect(jsonPath("$.pagoInicial").value(DEFAULT_PAGO_INICIAL.doubleValue()))
            .andExpect(jsonPath("$.cuotas").value(DEFAULT_CUOTAS.intValue()))
            .andExpect(jsonPath("$.pagoUnico").value(DEFAULT_PAGO_UNICO.doubleValue()))
            .andExpect(jsonPath("$.observacion").value(DEFAULT_OBSERVACION.toString()))
            .andExpect(jsonPath("$.observacionAdicional").value(DEFAULT_OBSERVACION_ADICIONAL.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingLinea() throws Exception {
        // Get the linea
        restLineaMockMvc.perform(get("/api/lineas/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateLinea() throws Exception {
        // Initialize the database
        lineaRepository.saveAndFlush(linea);

        int databaseSizeBeforeUpdate = lineaRepository.findAll().size();

        // Update the linea
        Linea updatedLinea = lineaRepository.findById(linea.getId()).get();
        // Disconnect from session so that the updates on updatedLinea are not directly saved in db
        em.detach(updatedLinea);
        updatedLinea
            .linea(UPDATED_LINEA)
            .esNueva(UPDATED_ES_NUEVA)
            .descuento(UPDATED_DESCUENTO)
            .porta(UPDATED_PORTA)
            .portaNombre(UPDATED_PORTA_NOMBRE)
            .portaCif(UPDATED_PORTA_CIF)
            .portaOperador(UPDATED_PORTA_OPERADOR)
            .provision(UPDATED_PROVISION)
            .activacion(UPDATED_ACTIVACION)
            .paqueteNumero(UPDATED_PAQUETE_NUMERO)
            .terminal(UPDATED_TERMINAL)
            .pagoInicial(UPDATED_PAGO_INICIAL)
            .cuotas(UPDATED_CUOTAS)
            .pagoUnico(UPDATED_PAGO_UNICO)
            .observacion(UPDATED_OBSERVACION)
            .observacionAdicional(UPDATED_OBSERVACION_ADICIONAL);

        restLineaMockMvc.perform(put("/api/lineas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedLinea)))
            .andExpect(status().isOk());

        // Validate the Linea in the database
        List<Linea> lineaList = lineaRepository.findAll();
        assertThat(lineaList).hasSize(databaseSizeBeforeUpdate);
        Linea testLinea = lineaList.get(lineaList.size() - 1);
        assertThat(testLinea.getLinea()).isEqualTo(UPDATED_LINEA);
        assertThat(testLinea.isEsNueva()).isEqualTo(UPDATED_ES_NUEVA);
        assertThat(testLinea.getDescuento()).isEqualTo(UPDATED_DESCUENTO);
        assertThat(testLinea.isPorta()).isEqualTo(UPDATED_PORTA);
        assertThat(testLinea.getPortaNombre()).isEqualTo(UPDATED_PORTA_NOMBRE);
        assertThat(testLinea.getPortaCif()).isEqualTo(UPDATED_PORTA_CIF);
        assertThat(testLinea.getPortaOperador()).isEqualTo(UPDATED_PORTA_OPERADOR);
        assertThat(testLinea.getProvision()).isEqualTo(UPDATED_PROVISION);
        assertThat(testLinea.getActivacion()).isEqualTo(UPDATED_ACTIVACION);
        assertThat(testLinea.getPaqueteNumero()).isEqualTo(UPDATED_PAQUETE_NUMERO);
        assertThat(testLinea.getTerminal()).isEqualTo(UPDATED_TERMINAL);
        assertThat(testLinea.getPagoInicial()).isEqualTo(UPDATED_PAGO_INICIAL);
        assertThat(testLinea.getCuotas()).isEqualTo(UPDATED_CUOTAS);
        assertThat(testLinea.getPagoUnico()).isEqualTo(UPDATED_PAGO_UNICO);
        assertThat(testLinea.getObservacion()).isEqualTo(UPDATED_OBSERVACION);
        assertThat(testLinea.getObservacionAdicional()).isEqualTo(UPDATED_OBSERVACION_ADICIONAL);
    }

    @Test
    @Transactional
    public void updateNonExistingLinea() throws Exception {
        int databaseSizeBeforeUpdate = lineaRepository.findAll().size();

        // Create the Linea

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restLineaMockMvc.perform(put("/api/lineas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(linea)))
            .andExpect(status().isBadRequest());

        // Validate the Linea in the database
        List<Linea> lineaList = lineaRepository.findAll();
        assertThat(lineaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteLinea() throws Exception {
        // Initialize the database
        lineaRepository.saveAndFlush(linea);

        int databaseSizeBeforeDelete = lineaRepository.findAll().size();

        // Delete the linea
        restLineaMockMvc.perform(delete("/api/lineas/{id}", linea.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Linea> lineaList = lineaRepository.findAll();
        assertThat(lineaList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Linea.class);
        Linea linea1 = new Linea();
        linea1.setId(1L);
        Linea linea2 = new Linea();
        linea2.setId(linea1.getId());
        assertThat(linea1).isEqualTo(linea2);
        linea2.setId(2L);
        assertThat(linea1).isNotEqualTo(linea2);
        linea1.setId(null);
        assertThat(linea1).isNotEqualTo(linea2);
    }
}
