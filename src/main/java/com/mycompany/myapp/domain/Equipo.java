package com.mycompany.myapp.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Equipo.
 */
@Entity
@Table(name = "equipo")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Equipo implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nombre")
    private String nombre;

    @OneToMany(mappedBy = "equipo")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Comercial> comercials = new HashSet<>();
    @OneToMany(mappedBy = "equipo")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Gasto> gastos = new HashSet<>();
    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public Equipo nombre(String nombre) {
        this.nombre = nombre;
        return this;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Set<Comercial> getComercials() {
        return comercials;
    }

    public Equipo comercials(Set<Comercial> comercials) {
        this.comercials = comercials;
        return this;
    }

    public Equipo addComercial(Comercial comercial) {
        this.comercials.add(comercial);
        comercial.setEquipo(this);
        return this;
    }

    public Equipo removeComercial(Comercial comercial) {
        this.comercials.remove(comercial);
        comercial.setEquipo(null);
        return this;
    }

    public void setComercials(Set<Comercial> comercials) {
        this.comercials = comercials;
    }

    public Set<Gasto> getGastos() {
        return gastos;
    }

    public Equipo gastos(Set<Gasto> gastos) {
        this.gastos = gastos;
        return this;
    }

    public Equipo addGasto(Gasto gasto) {
        this.gastos.add(gasto);
        gasto.setEquipo(this);
        return this;
    }

    public Equipo removeGasto(Gasto gasto) {
        this.gastos.remove(gasto);
        gasto.setEquipo(null);
        return this;
    }

    public void setGastos(Set<Gasto> gastos) {
        this.gastos = gastos;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Equipo equipo = (Equipo) o;
        if (equipo.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), equipo.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Equipo{" +
            "id=" + getId() +
            ", nombre='" + getNombre() + "'" +
            "}";
    }
}
