package com.mycompany.myapp.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Login.
 */
@Entity
@Table(name = "login")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Login implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nombre")
    private String nombre;

    @Column(name = "pass")
    private String pass;

    @OneToMany(mappedBy = "login")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Comercial> comercials = new HashSet<>();
    @ManyToOne
    @JsonIgnoreProperties("logins")
    private Perfil perfil;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public Login nombre(String nombre) {
        this.nombre = nombre;
        return this;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPass() {
        return pass;
    }

    public Login pass(String pass) {
        this.pass = pass;
        return this;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public Set<Comercial> getComercials() {
        return comercials;
    }

    public Login comercials(Set<Comercial> comercials) {
        this.comercials = comercials;
        return this;
    }

    public Login addComercial(Comercial comercial) {
        this.comercials.add(comercial);
        comercial.setLogin(this);
        return this;
    }

    public Login removeComercial(Comercial comercial) {
        this.comercials.remove(comercial);
        comercial.setLogin(null);
        return this;
    }

    public void setComercials(Set<Comercial> comercials) {
        this.comercials = comercials;
    }

    public Perfil getPerfil() {
        return perfil;
    }

    public Login perfil(Perfil perfil) {
        this.perfil = perfil;
        return this;
    }

    public void setPerfil(Perfil perfil) {
        this.perfil = perfil;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Login login = (Login) o;
        if (login.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), login.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Login{" +
            "id=" + getId() +
            ", nombre='" + getNombre() + "'" +
            ", pass='" + getPass() + "'" +
            "}";
    }
}
