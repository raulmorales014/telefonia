package com.mycompany.myapp.domain.enumeration;

/**
 * The Color enumeration.
 */
public enum Color {
    RojoClaro, Azul, Turquesa, Verde, Amarillo, Naranja, Rojo, Negro
}
