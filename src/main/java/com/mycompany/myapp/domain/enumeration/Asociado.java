package com.mycompany.myapp.domain.enumeration;

/**
 * The Asociado enumeration.
 */
public enum Asociado {
    Empresa, Cliente, Comercial, Equipo
}
