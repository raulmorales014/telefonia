package com.mycompany.myapp.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A TramitadoPor.
 */
@Entity
@Table(name = "tramitado_por")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class TramitadoPor implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nombre")
    private String nombre;

    @OneToMany(mappedBy = "tramitadoPor")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Oferta> ofertas = new HashSet<>();
    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public TramitadoPor nombre(String nombre) {
        this.nombre = nombre;
        return this;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Set<Oferta> getOfertas() {
        return ofertas;
    }

    public TramitadoPor ofertas(Set<Oferta> ofertas) {
        this.ofertas = ofertas;
        return this;
    }

    public TramitadoPor addOferta(Oferta oferta) {
        this.ofertas.add(oferta);
        oferta.setTramitadoPor(this);
        return this;
    }

    public TramitadoPor removeOferta(Oferta oferta) {
        this.ofertas.remove(oferta);
        oferta.setTramitadoPor(null);
        return this;
    }

    public void setOfertas(Set<Oferta> ofertas) {
        this.ofertas = ofertas;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TramitadoPor tramitadoPor = (TramitadoPor) o;
        if (tramitadoPor.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), tramitadoPor.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "TramitadoPor{" +
            "id=" + getId() +
            ", nombre='" + getNombre() + "'" +
            "}";
    }
}
