package com.mycompany.myapp.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A Linea.
 */
@Entity
@Table(name = "linea")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Linea implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "linea")
    private Long linea;

    @Column(name = "es_nueva")
    private Boolean esNueva;

    @Column(name = "descuento")
    private Long descuento;

    @Column(name = "porta")
    private Boolean porta;

    @Column(name = "porta_nombre")
    private String portaNombre;

    @Column(name = "porta_cif")
    private String portaCif;

    @Column(name = "porta_operador")
    private String portaOperador;

    @Column(name = "provision")
    private LocalDate provision;

    @Column(name = "activacion")
    private LocalDate activacion;

    @Column(name = "paquete_numero")
    private String paqueteNumero;

    @Column(name = "terminal")
    private String terminal;

    @Column(name = "pago_inicial")
    private Float pagoInicial;

    @Column(name = "cuotas")
    private Long cuotas;

    @Column(name = "pago_unico")
    private Float pagoUnico;

    @Column(name = "observacion")
    private String observacion;

    @Column(name = "observacion_adicional")
    private String observacionAdicional;

    @ManyToOne
    @JsonIgnoreProperties("lineas")
    private Producto producto;

    @ManyToOne
    @JsonIgnoreProperties("lineas")
    private Permanencia permanencia;

    @ManyToOne
    @JsonIgnoreProperties("lineas")
    private Paquete paquete;

    @ManyToOne
    @JsonIgnoreProperties("lineas")
    private Oferta oferta;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getLinea() {
        return linea;
    }

    public Linea linea(Long linea) {
        this.linea = linea;
        return this;
    }

    public void setLinea(Long linea) {
        this.linea = linea;
    }

    public Boolean isEsNueva() {
        return esNueva;
    }

    public Linea esNueva(Boolean esNueva) {
        this.esNueva = esNueva;
        return this;
    }

    public void setEsNueva(Boolean esNueva) {
        this.esNueva = esNueva;
    }

    public Long getDescuento() {
        return descuento;
    }

    public Linea descuento(Long descuento) {
        this.descuento = descuento;
        return this;
    }

    public void setDescuento(Long descuento) {
        this.descuento = descuento;
    }

    public Boolean isPorta() {
        return porta;
    }

    public Linea porta(Boolean porta) {
        this.porta = porta;
        return this;
    }

    public void setPorta(Boolean porta) {
        this.porta = porta;
    }

    public String getPortaNombre() {
        return portaNombre;
    }

    public Linea portaNombre(String portaNombre) {
        this.portaNombre = portaNombre;
        return this;
    }

    public void setPortaNombre(String portaNombre) {
        this.portaNombre = portaNombre;
    }

    public String getPortaCif() {
        return portaCif;
    }

    public Linea portaCif(String portaCif) {
        this.portaCif = portaCif;
        return this;
    }

    public void setPortaCif(String portaCif) {
        this.portaCif = portaCif;
    }

    public String getPortaOperador() {
        return portaOperador;
    }

    public Linea portaOperador(String portaOperador) {
        this.portaOperador = portaOperador;
        return this;
    }

    public void setPortaOperador(String portaOperador) {
        this.portaOperador = portaOperador;
    }

    public LocalDate getProvision() {
        return provision;
    }

    public Linea provision(LocalDate provision) {
        this.provision = provision;
        return this;
    }

    public void setProvision(LocalDate provision) {
        this.provision = provision;
    }

    public LocalDate getActivacion() {
        return activacion;
    }

    public Linea activacion(LocalDate activacion) {
        this.activacion = activacion;
        return this;
    }

    public void setActivacion(LocalDate activacion) {
        this.activacion = activacion;
    }

    public String getPaqueteNumero() {
        return paqueteNumero;
    }

    public Linea paqueteNumero(String paqueteNumero) {
        this.paqueteNumero = paqueteNumero;
        return this;
    }

    public void setPaqueteNumero(String paqueteNumero) {
        this.paqueteNumero = paqueteNumero;
    }

    public String getTerminal() {
        return terminal;
    }

    public Linea terminal(String terminal) {
        this.terminal = terminal;
        return this;
    }

    public void setTerminal(String terminal) {
        this.terminal = terminal;
    }

    public Float getPagoInicial() {
        return pagoInicial;
    }

    public Linea pagoInicial(Float pagoInicial) {
        this.pagoInicial = pagoInicial;
        return this;
    }

    public void setPagoInicial(Float pagoInicial) {
        this.pagoInicial = pagoInicial;
    }

    public Long getCuotas() {
        return cuotas;
    }

    public Linea cuotas(Long cuotas) {
        this.cuotas = cuotas;
        return this;
    }

    public void setCuotas(Long cuotas) {
        this.cuotas = cuotas;
    }

    public Float getPagoUnico() {
        return pagoUnico;
    }

    public Linea pagoUnico(Float pagoUnico) {
        this.pagoUnico = pagoUnico;
        return this;
    }

    public void setPagoUnico(Float pagoUnico) {
        this.pagoUnico = pagoUnico;
    }

    public String getObservacion() {
        return observacion;
    }

    public Linea observacion(String observacion) {
        this.observacion = observacion;
        return this;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getObservacionAdicional() {
        return observacionAdicional;
    }

    public Linea observacionAdicional(String observacionAdicional) {
        this.observacionAdicional = observacionAdicional;
        return this;
    }

    public void setObservacionAdicional(String observacionAdicional) {
        this.observacionAdicional = observacionAdicional;
    }

    public Producto getProducto() {
        return producto;
    }

    public Linea producto(Producto producto) {
        this.producto = producto;
        return this;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public Permanencia getPermanencia() {
        return permanencia;
    }

    public Linea permanencia(Permanencia permanencia) {
        this.permanencia = permanencia;
        return this;
    }

    public void setPermanencia(Permanencia permanencia) {
        this.permanencia = permanencia;
    }

    public Paquete getPaquete() {
        return paquete;
    }

    public Linea paquete(Paquete paquete) {
        this.paquete = paquete;
        return this;
    }

    public void setPaquete(Paquete paquete) {
        this.paquete = paquete;
    }

    public Oferta getOferta() {
        return oferta;
    }

    public Linea oferta(Oferta oferta) {
        this.oferta = oferta;
        return this;
    }

    public void setOferta(Oferta oferta) {
        this.oferta = oferta;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Linea linea = (Linea) o;
        if (linea.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), linea.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Linea{" +
            "id=" + getId() +
            ", linea=" + getLinea() +
            ", esNueva='" + isEsNueva() + "'" +
            ", descuento=" + getDescuento() +
            ", porta='" + isPorta() + "'" +
            ", portaNombre='" + getPortaNombre() + "'" +
            ", portaCif='" + getPortaCif() + "'" +
            ", portaOperador='" + getPortaOperador() + "'" +
            ", provision='" + getProvision() + "'" +
            ", activacion='" + getActivacion() + "'" +
            ", paqueteNumero='" + getPaqueteNumero() + "'" +
            ", terminal='" + getTerminal() + "'" +
            ", pagoInicial=" + getPagoInicial() +
            ", cuotas=" + getCuotas() +
            ", pagoUnico=" + getPagoUnico() +
            ", observacion='" + getObservacion() + "'" +
            ", observacionAdicional='" + getObservacionAdicional() + "'" +
            "}";
    }
}
