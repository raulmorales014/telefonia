package com.mycompany.myapp.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

import com.mycompany.myapp.domain.enumeration.Asociado;

/**
 * A Gasto.
 */
@Entity
@Table(name = "gasto")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Gasto implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nombre")
    private String nombre;

    @Column(name = "fecha")
    private LocalDate fecha;

    @Column(name = "precio")
    private Float precio;

    @Enumerated(EnumType.STRING)
    @Column(name = "asociado")
    private Asociado asociado;

    @ManyToOne
    @JsonIgnoreProperties("gastos")
    private Cliente cliente;

    @ManyToOne
    @JsonIgnoreProperties("gastos")
    private Empresa empresa;

    @ManyToOne
    @JsonIgnoreProperties("gastos")
    private Comercial comercial;

    @ManyToOne
    @JsonIgnoreProperties("gastos")
    private Equipo equipo;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public Gasto nombre(String nombre) {
        this.nombre = nombre;
        return this;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public LocalDate getFecha() {
        return fecha;
    }

    public Gasto fecha(LocalDate fecha) {
        this.fecha = fecha;
        return this;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }

    public Float getPrecio() {
        return precio;
    }

    public Gasto precio(Float precio) {
        this.precio = precio;
        return this;
    }

    public void setPrecio(Float precio) {
        this.precio = precio;
    }

    public Asociado getAsociado() {
        return asociado;
    }

    public Gasto asociado(Asociado asociado) {
        this.asociado = asociado;
        return this;
    }

    public void setAsociado(Asociado asociado) {
        this.asociado = asociado;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public Gasto cliente(Cliente cliente) {
        this.cliente = cliente;
        return this;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public Gasto empresa(Empresa empresa) {
        this.empresa = empresa;
        return this;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public Comercial getComercial() {
        return comercial;
    }

    public Gasto comercial(Comercial comercial) {
        this.comercial = comercial;
        return this;
    }

    public void setComercial(Comercial comercial) {
        this.comercial = comercial;
    }

    public Equipo getEquipo() {
        return equipo;
    }

    public Gasto equipo(Equipo equipo) {
        this.equipo = equipo;
        return this;
    }

    public void setEquipo(Equipo equipo) {
        this.equipo = equipo;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Gasto gasto = (Gasto) o;
        if (gasto.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), gasto.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Gasto{" +
            "id=" + getId() +
            ", nombre='" + getNombre() + "'" +
            ", fecha='" + getFecha() + "'" +
            ", precio=" + getPrecio() +
            ", asociado='" + getAsociado() + "'" +
            "}";
    }
}
