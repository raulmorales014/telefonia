package com.mycompany.myapp.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Oferta.
 */
@Entity
@Table(name = "oferta")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Oferta implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "numero_oferta")
    private String numeroOferta;

    @Column(name = "fecha")
    private LocalDate fecha;

    @Column(name = "firmado")
    private Boolean firmado;

    @OneToMany(mappedBy = "oferta")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Linea> lineas = new HashSet<>();
    @ManyToOne
    @JsonIgnoreProperties("ofertas")
    private EstadoPorcentual estadoPorcentual;

    @ManyToOne
    @JsonIgnoreProperties("ofertas")
    private Comercial comercial;

    @ManyToOne
    @JsonIgnoreProperties("ofertas")
    private Cliente cliente;

    @ManyToOne
    @JsonIgnoreProperties("ofertas")
    private TramitadoPor tramitadoPor;

    @ManyToOne
    @JsonIgnoreProperties("ofertas")
    private Subestado subestado;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumeroOferta() {
        return numeroOferta;
    }

    public Oferta numeroOferta(String numeroOferta) {
        this.numeroOferta = numeroOferta;
        return this;
    }

    public void setNumeroOferta(String numeroOferta) {
        this.numeroOferta = numeroOferta;
    }

    public LocalDate getFecha() {
        return fecha;
    }

    public Oferta fecha(LocalDate fecha) {
        this.fecha = fecha;
        return this;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }

    public Boolean isFirmado() {
        return firmado;
    }

    public Oferta firmado(Boolean firmado) {
        this.firmado = firmado;
        return this;
    }

    public void setFirmado(Boolean firmado) {
        this.firmado = firmado;
    }

    public Set<Linea> getLineas() {
        return lineas;
    }

    public Oferta lineas(Set<Linea> lineas) {
        this.lineas = lineas;
        return this;
    }

    public Oferta addLinea(Linea linea) {
        this.lineas.add(linea);
        linea.setOferta(this);
        return this;
    }

    public Oferta removeLinea(Linea linea) {
        this.lineas.remove(linea);
        linea.setOferta(null);
        return this;
    }

    public void setLineas(Set<Linea> lineas) {
        this.lineas = lineas;
    }

    public EstadoPorcentual getEstadoPorcentual() {
        return estadoPorcentual;
    }

    public Oferta estadoPorcentual(EstadoPorcentual estadoPorcentual) {
        this.estadoPorcentual = estadoPorcentual;
        return this;
    }

    public void setEstadoPorcentual(EstadoPorcentual estadoPorcentual) {
        this.estadoPorcentual = estadoPorcentual;
    }

    public Comercial getComercial() {
        return comercial;
    }

    public Oferta comercial(Comercial comercial) {
        this.comercial = comercial;
        return this;
    }

    public void setComercial(Comercial comercial) {
        this.comercial = comercial;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public Oferta cliente(Cliente cliente) {
        this.cliente = cliente;
        return this;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public TramitadoPor getTramitadoPor() {
        return tramitadoPor;
    }

    public Oferta tramitadoPor(TramitadoPor tramitadoPor) {
        this.tramitadoPor = tramitadoPor;
        return this;
    }

    public void setTramitadoPor(TramitadoPor tramitadoPor) {
        this.tramitadoPor = tramitadoPor;
    }

    public Subestado getSubestado() {
        return subestado;
    }

    public Oferta subestado(Subestado subestado) {
        this.subestado = subestado;
        return this;
    }

    public void setSubestado(Subestado subestado) {
        this.subestado = subestado;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Oferta oferta = (Oferta) o;
        if (oferta.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), oferta.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Oferta{" +
            "id=" + getId() +
            ", numeroOferta='" + getNumeroOferta() + "'" +
            ", fecha='" + getFecha() + "'" +
            ", firmado='" + isFirmado() + "'" +
            "}";
    }
}
