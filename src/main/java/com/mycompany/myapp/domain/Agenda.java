package com.mycompany.myapp.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

import com.mycompany.myapp.domain.enumeration.Color;

/**
 * A Agenda.
 */
@Entity
@Table(name = "agenda")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Agenda implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "titulo")
    private String titulo;

    @Enumerated(EnumType.STRING)
    @Column(name = "color")
    private Color color;

    @Column(name = "inicio")
    private Instant inicio;

    @Column(name = "fin")
    private Instant fin;

    @Column(name = "observacion")
    private String observacion;

    @ManyToOne
    @JsonIgnoreProperties("agenda")
    private EstadoPorcentual estadoPorcentual;

    @ManyToOne
    @JsonIgnoreProperties("agenda")
    private Comercial comercial;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public Agenda titulo(String titulo) {
        this.titulo = titulo;
        return this;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public Color getColor() {
        return color;
    }

    public Agenda color(Color color) {
        this.color = color;
        return this;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public Instant getInicio() {
        return inicio;
    }

    public Agenda inicio(Instant inicio) {
        this.inicio = inicio;
        return this;
    }

    public void setInicio(Instant inicio) {
        this.inicio = inicio;
    }

    public Instant getFin() {
        return fin;
    }

    public Agenda fin(Instant fin) {
        this.fin = fin;
        return this;
    }

    public void setFin(Instant fin) {
        this.fin = fin;
    }

    public String getObservacion() {
        return observacion;
    }

    public Agenda observacion(String observacion) {
        this.observacion = observacion;
        return this;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public EstadoPorcentual getEstadoPorcentual() {
        return estadoPorcentual;
    }

    public Agenda estadoPorcentual(EstadoPorcentual estadoPorcentual) {
        this.estadoPorcentual = estadoPorcentual;
        return this;
    }

    public void setEstadoPorcentual(EstadoPorcentual estadoPorcentual) {
        this.estadoPorcentual = estadoPorcentual;
    }

    public Comercial getComercial() {
        return comercial;
    }

    public Agenda comercial(Comercial comercial) {
        this.comercial = comercial;
        return this;
    }

    public void setComercial(Comercial comercial) {
        this.comercial = comercial;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Agenda agenda = (Agenda) o;
        if (agenda.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), agenda.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Agenda{" +
            "id=" + getId() +
            ", titulo='" + getTitulo() + "'" +
            ", color='" + getColor() + "'" +
            ", inicio='" + getInicio() + "'" +
            ", fin='" + getFin() + "'" +
            ", observacion='" + getObservacion() + "'" +
            "}";
    }
}
