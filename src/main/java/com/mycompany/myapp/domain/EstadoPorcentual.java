package com.mycompany.myapp.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A EstadoPorcentual.
 */
@Entity
@Table(name = "estado_porcentual")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class EstadoPorcentual implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nombre")
    private String nombre;

    @OneToMany(mappedBy = "estadoPorcentual")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Oferta> ofertas = new HashSet<>();
    @OneToMany(mappedBy = "estadoPorcentual")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Agenda> agenda = new HashSet<>();
    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public EstadoPorcentual nombre(String nombre) {
        this.nombre = nombre;
        return this;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Set<Oferta> getOfertas() {
        return ofertas;
    }

    public EstadoPorcentual ofertas(Set<Oferta> ofertas) {
        this.ofertas = ofertas;
        return this;
    }

    public EstadoPorcentual addOferta(Oferta oferta) {
        this.ofertas.add(oferta);
        oferta.setEstadoPorcentual(this);
        return this;
    }

    public EstadoPorcentual removeOferta(Oferta oferta) {
        this.ofertas.remove(oferta);
        oferta.setEstadoPorcentual(null);
        return this;
    }

    public void setOfertas(Set<Oferta> ofertas) {
        this.ofertas = ofertas;
    }

    public Set<Agenda> getAgenda() {
        return agenda;
    }

    public EstadoPorcentual agenda(Set<Agenda> agenda) {
        this.agenda = agenda;
        return this;
    }

    public EstadoPorcentual addAgenda(Agenda agenda) {
        this.agenda.add(agenda);
        agenda.setEstadoPorcentual(this);
        return this;
    }

    public EstadoPorcentual removeAgenda(Agenda agenda) {
        this.agenda.remove(agenda);
        agenda.setEstadoPorcentual(null);
        return this;
    }

    public void setAgenda(Set<Agenda> agenda) {
        this.agenda = agenda;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        EstadoPorcentual estadoPorcentual = (EstadoPorcentual) o;
        if (estadoPorcentual.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), estadoPorcentual.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "EstadoPorcentual{" +
            "id=" + getId() +
            ", nombre='" + getNombre() + "'" +
            "}";
    }
}
