package com.mycompany.myapp.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Producto.
 */
@Entity
@Table(name = "producto")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Producto implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nombre")
    private String nombre;

    @Column(name = "spirit")
    private String spirit;

    @Column(name = "ow")
    private String ow;

    @Column(name = "comision_con_permanencia")
    private Long comisionConPermanencia;

    @Column(name = "comision_sin_permanencia")
    private Long comisionSinPermanencia;

    @Column(name = "rappel")
    private Float rappel;

    @Column(name = "precio_venta")
    private Float precioVenta;

    @OneToMany(mappedBy = "producto")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Linea> lineas = new HashSet<>();
    @ManyToOne
    @JsonIgnoreProperties("productos")
    private Familia familia;

    @ManyToOne
    @JsonIgnoreProperties("productos")
    private CuentaLinea cuentaLinea;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public Producto nombre(String nombre) {
        this.nombre = nombre;
        return this;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getSpirit() {
        return spirit;
    }

    public Producto spirit(String spirit) {
        this.spirit = spirit;
        return this;
    }

    public void setSpirit(String spirit) {
        this.spirit = spirit;
    }

    public String getOw() {
        return ow;
    }

    public Producto ow(String ow) {
        this.ow = ow;
        return this;
    }

    public void setOw(String ow) {
        this.ow = ow;
    }

    public Long getComisionConPermanencia() {
        return comisionConPermanencia;
    }

    public Producto comisionConPermanencia(Long comisionConPermanencia) {
        this.comisionConPermanencia = comisionConPermanencia;
        return this;
    }

    public void setComisionConPermanencia(Long comisionConPermanencia) {
        this.comisionConPermanencia = comisionConPermanencia;
    }

    public Long getComisionSinPermanencia() {
        return comisionSinPermanencia;
    }

    public Producto comisionSinPermanencia(Long comisionSinPermanencia) {
        this.comisionSinPermanencia = comisionSinPermanencia;
        return this;
    }

    public void setComisionSinPermanencia(Long comisionSinPermanencia) {
        this.comisionSinPermanencia = comisionSinPermanencia;
    }

    public Float getRappel() {
        return rappel;
    }

    public Producto rappel(Float rappel) {
        this.rappel = rappel;
        return this;
    }

    public void setRappel(Float rappel) {
        this.rappel = rappel;
    }

    public Float getPrecioVenta() {
        return precioVenta;
    }

    public Producto precioVenta(Float precioVenta) {
        this.precioVenta = precioVenta;
        return this;
    }

    public void setPrecioVenta(Float precioVenta) {
        this.precioVenta = precioVenta;
    }

    public Set<Linea> getLineas() {
        return lineas;
    }

    public Producto lineas(Set<Linea> lineas) {
        this.lineas = lineas;
        return this;
    }

    public Producto addLinea(Linea linea) {
        this.lineas.add(linea);
        linea.setProducto(this);
        return this;
    }

    public Producto removeLinea(Linea linea) {
        this.lineas.remove(linea);
        linea.setProducto(null);
        return this;
    }

    public void setLineas(Set<Linea> lineas) {
        this.lineas = lineas;
    }

    public Familia getFamilia() {
        return familia;
    }

    public Producto familia(Familia familia) {
        this.familia = familia;
        return this;
    }

    public void setFamilia(Familia familia) {
        this.familia = familia;
    }

    public CuentaLinea getCuentaLinea() {
        return cuentaLinea;
    }

    public Producto cuentaLinea(CuentaLinea cuentaLinea) {
        this.cuentaLinea = cuentaLinea;
        return this;
    }

    public void setCuentaLinea(CuentaLinea cuentaLinea) {
        this.cuentaLinea = cuentaLinea;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Producto producto = (Producto) o;
        if (producto.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), producto.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Producto{" +
            "id=" + getId() +
            ", nombre='" + getNombre() + "'" +
            ", spirit='" + getSpirit() + "'" +
            ", ow='" + getOw() + "'" +
            ", comisionConPermanencia=" + getComisionConPermanencia() +
            ", comisionSinPermanencia=" + getComisionSinPermanencia() +
            ", rappel=" + getRappel() +
            ", precioVenta=" + getPrecioVenta() +
            "}";
    }
}
