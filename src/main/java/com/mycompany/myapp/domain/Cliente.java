package com.mycompany.myapp.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Cliente.
 */
@Entity
@Table(name = "cliente")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Cliente implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nombre")
    private String nombre;

    @Column(name = "cif")
    private String cif;

    @Column(name = "apoderado")
    private String apoderado;

    @NotNull
    @Column(name = "nacimiento", nullable = false)
    private LocalDate nacimiento;

    @Column(name = "dni")
    private String dni;

    @Column(name = "persona_contacto")
    private String personaContacto;

    @Column(name = "correo")
    private String correo;

    @Column(name = "telefono")
    private Long telefono;

    @Column(name = "direccion")
    private String direccion;

    @Column(name = "localidad")
    private String localidad;

    @Column(name = "provincia")
    private String provincia;

    @Column(name = "codigo_postal")
    private Long codigoPostal;

    @Column(name = "cta")
    private String cta;

    @Column(name = "observaciones")
    private String observaciones;

    @Column(name = "penalizaciones_vf")
    private Long penalizacionesVF;

    @Column(name = "penalizaciones_un_4_tel")
    private Long penalizacionesUn4tel;

    @Column(name = "liberalizaciones")
    private Long liberalizaciones;

    @Column(name = "coste_alcliente")
    private Long costeAlcliente;

    @Column(name = "ocultar_comerciales")
    private Boolean ocultarComerciales;

    @OneToMany(mappedBy = "cliente")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Oferta> ofertas = new HashSet<>();
    @OneToMany(mappedBy = "cliente")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Gasto> gastos = new HashSet<>();
    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public Cliente nombre(String nombre) {
        this.nombre = nombre;
        return this;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCif() {
        return cif;
    }

    public Cliente cif(String cif) {
        this.cif = cif;
        return this;
    }

    public void setCif(String cif) {
        this.cif = cif;
    }

    public String getApoderado() {
        return apoderado;
    }

    public Cliente apoderado(String apoderado) {
        this.apoderado = apoderado;
        return this;
    }

    public void setApoderado(String apoderado) {
        this.apoderado = apoderado;
    }

    public LocalDate getNacimiento() {
        return nacimiento;
    }

    public Cliente nacimiento(LocalDate nacimiento) {
        this.nacimiento = nacimiento;
        return this;
    }

    public void setNacimiento(LocalDate nacimiento) {
        this.nacimiento = nacimiento;
    }

    public String getDni() {
        return dni;
    }

    public Cliente dni(String dni) {
        this.dni = dni;
        return this;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getPersonaContacto() {
        return personaContacto;
    }

    public Cliente personaContacto(String personaContacto) {
        this.personaContacto = personaContacto;
        return this;
    }

    public void setPersonaContacto(String personaContacto) {
        this.personaContacto = personaContacto;
    }

    public String getCorreo() {
        return correo;
    }

    public Cliente correo(String correo) {
        this.correo = correo;
        return this;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public Long getTelefono() {
        return telefono;
    }

    public Cliente telefono(Long telefono) {
        this.telefono = telefono;
        return this;
    }

    public void setTelefono(Long telefono) {
        this.telefono = telefono;
    }

    public String getDireccion() {
        return direccion;
    }

    public Cliente direccion(String direccion) {
        this.direccion = direccion;
        return this;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getLocalidad() {
        return localidad;
    }

    public Cliente localidad(String localidad) {
        this.localidad = localidad;
        return this;
    }

    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }

    public String getProvincia() {
        return provincia;
    }

    public Cliente provincia(String provincia) {
        this.provincia = provincia;
        return this;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public Long getCodigoPostal() {
        return codigoPostal;
    }

    public Cliente codigoPostal(Long codigoPostal) {
        this.codigoPostal = codigoPostal;
        return this;
    }

    public void setCodigoPostal(Long codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    public String getCta() {
        return cta;
    }

    public Cliente cta(String cta) {
        this.cta = cta;
        return this;
    }

    public void setCta(String cta) {
        this.cta = cta;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public Cliente observaciones(String observaciones) {
        this.observaciones = observaciones;
        return this;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public Long getPenalizacionesVF() {
        return penalizacionesVF;
    }

    public Cliente penalizacionesVF(Long penalizacionesVF) {
        this.penalizacionesVF = penalizacionesVF;
        return this;
    }

    public void setPenalizacionesVF(Long penalizacionesVF) {
        this.penalizacionesVF = penalizacionesVF;
    }

    public Long getPenalizacionesUn4tel() {
        return penalizacionesUn4tel;
    }

    public Cliente penalizacionesUn4tel(Long penalizacionesUn4tel) {
        this.penalizacionesUn4tel = penalizacionesUn4tel;
        return this;
    }

    public void setPenalizacionesUn4tel(Long penalizacionesUn4tel) {
        this.penalizacionesUn4tel = penalizacionesUn4tel;
    }

    public Long getLiberalizaciones() {
        return liberalizaciones;
    }

    public Cliente liberalizaciones(Long liberalizaciones) {
        this.liberalizaciones = liberalizaciones;
        return this;
    }

    public void setLiberalizaciones(Long liberalizaciones) {
        this.liberalizaciones = liberalizaciones;
    }

    public Long getCosteAlcliente() {
        return costeAlcliente;
    }

    public Cliente costeAlcliente(Long costeAlcliente) {
        this.costeAlcliente = costeAlcliente;
        return this;
    }

    public void setCosteAlcliente(Long costeAlcliente) {
        this.costeAlcliente = costeAlcliente;
    }

    public Boolean isOcultarComerciales() {
        return ocultarComerciales;
    }

    public Cliente ocultarComerciales(Boolean ocultarComerciales) {
        this.ocultarComerciales = ocultarComerciales;
        return this;
    }

    public void setOcultarComerciales(Boolean ocultarComerciales) {
        this.ocultarComerciales = ocultarComerciales;
    }

    public Set<Oferta> getOfertas() {
        return ofertas;
    }

    public Cliente ofertas(Set<Oferta> ofertas) {
        this.ofertas = ofertas;
        return this;
    }

    public Cliente addOferta(Oferta oferta) {
        this.ofertas.add(oferta);
        oferta.setCliente(this);
        return this;
    }

    public Cliente removeOferta(Oferta oferta) {
        this.ofertas.remove(oferta);
        oferta.setCliente(null);
        return this;
    }

    public void setOfertas(Set<Oferta> ofertas) {
        this.ofertas = ofertas;
    }

    public Set<Gasto> getGastos() {
        return gastos;
    }

    public Cliente gastos(Set<Gasto> gastos) {
        this.gastos = gastos;
        return this;
    }

    public Cliente addGasto(Gasto gasto) {
        this.gastos.add(gasto);
        gasto.setCliente(this);
        return this;
    }

    public Cliente removeGasto(Gasto gasto) {
        this.gastos.remove(gasto);
        gasto.setCliente(null);
        return this;
    }

    public void setGastos(Set<Gasto> gastos) {
        this.gastos = gastos;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Cliente cliente = (Cliente) o;
        if (cliente.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), cliente.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Cliente{" +
            "id=" + getId() +
            ", nombre='" + getNombre() + "'" +
            ", cif='" + getCif() + "'" +
            ", apoderado='" + getApoderado() + "'" +
            ", nacimiento='" + getNacimiento() + "'" +
            ", dni='" + getDni() + "'" +
            ", personaContacto='" + getPersonaContacto() + "'" +
            ", correo='" + getCorreo() + "'" +
            ", telefono=" + getTelefono() +
            ", direccion='" + getDireccion() + "'" +
            ", localidad='" + getLocalidad() + "'" +
            ", provincia='" + getProvincia() + "'" +
            ", codigoPostal=" + getCodigoPostal() +
            ", cta='" + getCta() + "'" +
            ", observaciones='" + getObservaciones() + "'" +
            ", penalizacionesVF=" + getPenalizacionesVF() +
            ", penalizacionesUn4tel=" + getPenalizacionesUn4tel() +
            ", liberalizaciones=" + getLiberalizaciones() +
            ", costeAlcliente=" + getCosteAlcliente() +
            ", ocultarComerciales='" + isOcultarComerciales() + "'" +
            "}";
    }
}
