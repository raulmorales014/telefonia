package com.mycompany.myapp.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Comercial.
 */
@Entity
@Table(name = "comercial")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Comercial implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nombre")
    private String nombre;

    @Column(name = "apellidos")
    private String apellidos;

    @Column(name = "dni")
    private String dni;

    @Column(name = "sfid")
    private String sfid;

    @OneToMany(mappedBy = "comercial")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Oferta> ofertas = new HashSet<>();
    @OneToMany(mappedBy = "comercial")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Gasto> gastos = new HashSet<>();
    @OneToMany(mappedBy = "comercial")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Agenda> agenda = new HashSet<>();
    @ManyToOne
    @JsonIgnoreProperties("comercials")
    private Equipo equipo;

    @ManyToOne
    @JsonIgnoreProperties("comercials")
    private Login login;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public Comercial nombre(String nombre) {
        this.nombre = nombre;
        return this;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public Comercial apellidos(String apellidos) {
        this.apellidos = apellidos;
        return this;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getDni() {
        return dni;
    }

    public Comercial dni(String dni) {
        this.dni = dni;
        return this;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getSfid() {
        return sfid;
    }

    public Comercial sfid(String sfid) {
        this.sfid = sfid;
        return this;
    }

    public void setSfid(String sfid) {
        this.sfid = sfid;
    }

    public Set<Oferta> getOfertas() {
        return ofertas;
    }

    public Comercial ofertas(Set<Oferta> ofertas) {
        this.ofertas = ofertas;
        return this;
    }

    public Comercial addOferta(Oferta oferta) {
        this.ofertas.add(oferta);
        oferta.setComercial(this);
        return this;
    }

    public Comercial removeOferta(Oferta oferta) {
        this.ofertas.remove(oferta);
        oferta.setComercial(null);
        return this;
    }

    public void setOfertas(Set<Oferta> ofertas) {
        this.ofertas = ofertas;
    }

    public Set<Gasto> getGastos() {
        return gastos;
    }

    public Comercial gastos(Set<Gasto> gastos) {
        this.gastos = gastos;
        return this;
    }

    public Comercial addGasto(Gasto gasto) {
        this.gastos.add(gasto);
        gasto.setComercial(this);
        return this;
    }

    public Comercial removeGasto(Gasto gasto) {
        this.gastos.remove(gasto);
        gasto.setComercial(null);
        return this;
    }

    public void setGastos(Set<Gasto> gastos) {
        this.gastos = gastos;
    }

    public Set<Agenda> getAgenda() {
        return agenda;
    }

    public Comercial agenda(Set<Agenda> agenda) {
        this.agenda = agenda;
        return this;
    }

    public Comercial addAgenda(Agenda agenda) {
        this.agenda.add(agenda);
        agenda.setComercial(this);
        return this;
    }

    public Comercial removeAgenda(Agenda agenda) {
        this.agenda.remove(agenda);
        agenda.setComercial(null);
        return this;
    }

    public void setAgenda(Set<Agenda> agenda) {
        this.agenda = agenda;
    }

    public Equipo getEquipo() {
        return equipo;
    }

    public Comercial equipo(Equipo equipo) {
        this.equipo = equipo;
        return this;
    }

    public void setEquipo(Equipo equipo) {
        this.equipo = equipo;
    }

    public Login getLogin() {
        return login;
    }

    public Comercial login(Login login) {
        this.login = login;
        return this;
    }

    public void setLogin(Login login) {
        this.login = login;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Comercial comercial = (Comercial) o;
        if (comercial.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), comercial.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Comercial{" +
            "id=" + getId() +
            ", nombre='" + getNombre() + "'" +
            ", apellidos='" + getApellidos() + "'" +
            ", dni='" + getDni() + "'" +
            ", sfid='" + getSfid() + "'" +
            "}";
    }
}
