package com.mycompany.myapp.web.rest;
import com.mycompany.myapp.domain.Subestado;
import com.mycompany.myapp.repository.SubestadoRepository;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;
import com.mycompany.myapp.web.rest.util.HeaderUtil;
import com.mycompany.myapp.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Subestado.
 */
@RestController
@RequestMapping("/api")
public class SubestadoResource {

    private final Logger log = LoggerFactory.getLogger(SubestadoResource.class);

    private static final String ENTITY_NAME = "subestado";

    private final SubestadoRepository subestadoRepository;

    public SubestadoResource(SubestadoRepository subestadoRepository) {
        this.subestadoRepository = subestadoRepository;
    }

    /**
     * POST  /subestados : Create a new subestado.
     *
     * @param subestado the subestado to create
     * @return the ResponseEntity with status 201 (Created) and with body the new subestado, or with status 400 (Bad Request) if the subestado has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/subestados")
    public ResponseEntity<Subestado> createSubestado(@RequestBody Subestado subestado) throws URISyntaxException {
        log.debug("REST request to save Subestado : {}", subestado);
        if (subestado.getId() != null) {
            throw new BadRequestAlertException("A new subestado cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Subestado result = subestadoRepository.save(subestado);
        return ResponseEntity.created(new URI("/api/subestados/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /subestados : Updates an existing subestado.
     *
     * @param subestado the subestado to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated subestado,
     * or with status 400 (Bad Request) if the subestado is not valid,
     * or with status 500 (Internal Server Error) if the subestado couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/subestados")
    public ResponseEntity<Subestado> updateSubestado(@RequestBody Subestado subestado) throws URISyntaxException {
        log.debug("REST request to update Subestado : {}", subestado);
        if (subestado.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Subestado result = subestadoRepository.save(subestado);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, subestado.getId().toString()))
            .body(result);
    }

    /**
     * GET  /subestados : get all the subestados.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of subestados in body
     */
    @GetMapping("/subestados")
    public ResponseEntity<List<Subestado>> getAllSubestados(Pageable pageable) {
        log.debug("REST request to get a page of Subestados");
        Page<Subestado> page = subestadoRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/subestados");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * GET  /subestados/:id : get the "id" subestado.
     *
     * @param id the id of the subestado to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the subestado, or with status 404 (Not Found)
     */
    @GetMapping("/subestados/{id}")
    public ResponseEntity<Subestado> getSubestado(@PathVariable Long id) {
        log.debug("REST request to get Subestado : {}", id);
        Optional<Subestado> subestado = subestadoRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(subestado);
    }

    /**
     * DELETE  /subestados/:id : delete the "id" subestado.
     *
     * @param id the id of the subestado to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/subestados/{id}")
    public ResponseEntity<Void> deleteSubestado(@PathVariable Long id) {
        log.debug("REST request to delete Subestado : {}", id);
        subestadoRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
