package com.mycompany.myapp.web.rest;
import com.mycompany.myapp.domain.TipoLinea;
import com.mycompany.myapp.repository.TipoLineaRepository;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;
import com.mycompany.myapp.web.rest.util.HeaderUtil;
import com.mycompany.myapp.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing TipoLinea.
 */
@RestController
@RequestMapping("/api")
public class TipoLineaResource {

    private final Logger log = LoggerFactory.getLogger(TipoLineaResource.class);

    private static final String ENTITY_NAME = "tipoLinea";

    private final TipoLineaRepository tipoLineaRepository;

    public TipoLineaResource(TipoLineaRepository tipoLineaRepository) {
        this.tipoLineaRepository = tipoLineaRepository;
    }

    /**
     * POST  /tipo-lineas : Create a new tipoLinea.
     *
     * @param tipoLinea the tipoLinea to create
     * @return the ResponseEntity with status 201 (Created) and with body the new tipoLinea, or with status 400 (Bad Request) if the tipoLinea has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/tipo-lineas")
    public ResponseEntity<TipoLinea> createTipoLinea(@RequestBody TipoLinea tipoLinea) throws URISyntaxException {
        log.debug("REST request to save TipoLinea : {}", tipoLinea);
        if (tipoLinea.getId() != null) {
            throw new BadRequestAlertException("A new tipoLinea cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TipoLinea result = tipoLineaRepository.save(tipoLinea);
        return ResponseEntity.created(new URI("/api/tipo-lineas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /tipo-lineas : Updates an existing tipoLinea.
     *
     * @param tipoLinea the tipoLinea to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated tipoLinea,
     * or with status 400 (Bad Request) if the tipoLinea is not valid,
     * or with status 500 (Internal Server Error) if the tipoLinea couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/tipo-lineas")
    public ResponseEntity<TipoLinea> updateTipoLinea(@RequestBody TipoLinea tipoLinea) throws URISyntaxException {
        log.debug("REST request to update TipoLinea : {}", tipoLinea);
        if (tipoLinea.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        TipoLinea result = tipoLineaRepository.save(tipoLinea);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, tipoLinea.getId().toString()))
            .body(result);
    }

    /**
     * GET  /tipo-lineas : get all the tipoLineas.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of tipoLineas in body
     */
    @GetMapping("/tipo-lineas")
    public ResponseEntity<List<TipoLinea>> getAllTipoLineas(Pageable pageable) {
        log.debug("REST request to get a page of TipoLineas");
        Page<TipoLinea> page = tipoLineaRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/tipo-lineas");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * GET  /tipo-lineas/:id : get the "id" tipoLinea.
     *
     * @param id the id of the tipoLinea to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the tipoLinea, or with status 404 (Not Found)
     */
    @GetMapping("/tipo-lineas/{id}")
    public ResponseEntity<TipoLinea> getTipoLinea(@PathVariable Long id) {
        log.debug("REST request to get TipoLinea : {}", id);
        Optional<TipoLinea> tipoLinea = tipoLineaRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(tipoLinea);
    }

    /**
     * DELETE  /tipo-lineas/:id : delete the "id" tipoLinea.
     *
     * @param id the id of the tipoLinea to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/tipo-lineas/{id}")
    public ResponseEntity<Void> deleteTipoLinea(@PathVariable Long id) {
        log.debug("REST request to delete TipoLinea : {}", id);
        tipoLineaRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
