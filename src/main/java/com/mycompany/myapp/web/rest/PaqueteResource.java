package com.mycompany.myapp.web.rest;
import com.mycompany.myapp.domain.Paquete;
import com.mycompany.myapp.repository.PaqueteRepository;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;
import com.mycompany.myapp.web.rest.util.HeaderUtil;
import com.mycompany.myapp.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Paquete.
 */
@RestController
@RequestMapping("/api")
public class PaqueteResource {

    private final Logger log = LoggerFactory.getLogger(PaqueteResource.class);

    private static final String ENTITY_NAME = "paquete";

    private final PaqueteRepository paqueteRepository;

    public PaqueteResource(PaqueteRepository paqueteRepository) {
        this.paqueteRepository = paqueteRepository;
    }

    /**
     * POST  /paquetes : Create a new paquete.
     *
     * @param paquete the paquete to create
     * @return the ResponseEntity with status 201 (Created) and with body the new paquete, or with status 400 (Bad Request) if the paquete has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/paquetes")
    public ResponseEntity<Paquete> createPaquete(@RequestBody Paquete paquete) throws URISyntaxException {
        log.debug("REST request to save Paquete : {}", paquete);
        if (paquete.getId() != null) {
            throw new BadRequestAlertException("A new paquete cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Paquete result = paqueteRepository.save(paquete);
        return ResponseEntity.created(new URI("/api/paquetes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /paquetes : Updates an existing paquete.
     *
     * @param paquete the paquete to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated paquete,
     * or with status 400 (Bad Request) if the paquete is not valid,
     * or with status 500 (Internal Server Error) if the paquete couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/paquetes")
    public ResponseEntity<Paquete> updatePaquete(@RequestBody Paquete paquete) throws URISyntaxException {
        log.debug("REST request to update Paquete : {}", paquete);
        if (paquete.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Paquete result = paqueteRepository.save(paquete);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, paquete.getId().toString()))
            .body(result);
    }

    /**
     * GET  /paquetes : get all the paquetes.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of paquetes in body
     */
    @GetMapping("/paquetes")
    public ResponseEntity<List<Paquete>> getAllPaquetes(Pageable pageable) {
        log.debug("REST request to get a page of Paquetes");
        Page<Paquete> page = paqueteRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/paquetes");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * GET  /paquetes/:id : get the "id" paquete.
     *
     * @param id the id of the paquete to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the paquete, or with status 404 (Not Found)
     */
    @GetMapping("/paquetes/{id}")
    public ResponseEntity<Paquete> getPaquete(@PathVariable Long id) {
        log.debug("REST request to get Paquete : {}", id);
        Optional<Paquete> paquete = paqueteRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(paquete);
    }

    /**
     * DELETE  /paquetes/:id : delete the "id" paquete.
     *
     * @param id the id of the paquete to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/paquetes/{id}")
    public ResponseEntity<Void> deletePaquete(@PathVariable Long id) {
        log.debug("REST request to delete Paquete : {}", id);
        paqueteRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
