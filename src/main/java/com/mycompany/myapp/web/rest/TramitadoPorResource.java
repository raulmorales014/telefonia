package com.mycompany.myapp.web.rest;
import com.mycompany.myapp.domain.TramitadoPor;
import com.mycompany.myapp.repository.TramitadoPorRepository;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;
import com.mycompany.myapp.web.rest.util.HeaderUtil;
import com.mycompany.myapp.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing TramitadoPor.
 */
@RestController
@RequestMapping("/api")
public class TramitadoPorResource {

    private final Logger log = LoggerFactory.getLogger(TramitadoPorResource.class);

    private static final String ENTITY_NAME = "tramitadoPor";

    private final TramitadoPorRepository tramitadoPorRepository;

    public TramitadoPorResource(TramitadoPorRepository tramitadoPorRepository) {
        this.tramitadoPorRepository = tramitadoPorRepository;
    }

    /**
     * POST  /tramitado-pors : Create a new tramitadoPor.
     *
     * @param tramitadoPor the tramitadoPor to create
     * @return the ResponseEntity with status 201 (Created) and with body the new tramitadoPor, or with status 400 (Bad Request) if the tramitadoPor has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/tramitado-pors")
    public ResponseEntity<TramitadoPor> createTramitadoPor(@RequestBody TramitadoPor tramitadoPor) throws URISyntaxException {
        log.debug("REST request to save TramitadoPor : {}", tramitadoPor);
        if (tramitadoPor.getId() != null) {
            throw new BadRequestAlertException("A new tramitadoPor cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TramitadoPor result = tramitadoPorRepository.save(tramitadoPor);
        return ResponseEntity.created(new URI("/api/tramitado-pors/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /tramitado-pors : Updates an existing tramitadoPor.
     *
     * @param tramitadoPor the tramitadoPor to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated tramitadoPor,
     * or with status 400 (Bad Request) if the tramitadoPor is not valid,
     * or with status 500 (Internal Server Error) if the tramitadoPor couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/tramitado-pors")
    public ResponseEntity<TramitadoPor> updateTramitadoPor(@RequestBody TramitadoPor tramitadoPor) throws URISyntaxException {
        log.debug("REST request to update TramitadoPor : {}", tramitadoPor);
        if (tramitadoPor.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        TramitadoPor result = tramitadoPorRepository.save(tramitadoPor);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, tramitadoPor.getId().toString()))
            .body(result);
    }

    /**
     * GET  /tramitado-pors : get all the tramitadoPors.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of tramitadoPors in body
     */
    @GetMapping("/tramitado-pors")
    public ResponseEntity<List<TramitadoPor>> getAllTramitadoPors(Pageable pageable) {
        log.debug("REST request to get a page of TramitadoPors");
        Page<TramitadoPor> page = tramitadoPorRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/tramitado-pors");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * GET  /tramitado-pors/:id : get the "id" tramitadoPor.
     *
     * @param id the id of the tramitadoPor to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the tramitadoPor, or with status 404 (Not Found)
     */
    @GetMapping("/tramitado-pors/{id}")
    public ResponseEntity<TramitadoPor> getTramitadoPor(@PathVariable Long id) {
        log.debug("REST request to get TramitadoPor : {}", id);
        Optional<TramitadoPor> tramitadoPor = tramitadoPorRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(tramitadoPor);
    }

    /**
     * DELETE  /tramitado-pors/:id : delete the "id" tramitadoPor.
     *
     * @param id the id of the tramitadoPor to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/tramitado-pors/{id}")
    public ResponseEntity<Void> deleteTramitadoPor(@PathVariable Long id) {
        log.debug("REST request to delete TramitadoPor : {}", id);
        tramitadoPorRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
