package com.mycompany.myapp.web.rest;
import com.mycompany.myapp.domain.Familia;
import com.mycompany.myapp.repository.FamiliaRepository;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;
import com.mycompany.myapp.web.rest.util.HeaderUtil;
import com.mycompany.myapp.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Familia.
 */
@RestController
@RequestMapping("/api")
public class FamiliaResource {

    private final Logger log = LoggerFactory.getLogger(FamiliaResource.class);

    private static final String ENTITY_NAME = "familia";

    private final FamiliaRepository familiaRepository;

    public FamiliaResource(FamiliaRepository familiaRepository) {
        this.familiaRepository = familiaRepository;
    }

    /**
     * POST  /familias : Create a new familia.
     *
     * @param familia the familia to create
     * @return the ResponseEntity with status 201 (Created) and with body the new familia, or with status 400 (Bad Request) if the familia has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/familias")
    public ResponseEntity<Familia> createFamilia(@RequestBody Familia familia) throws URISyntaxException {
        log.debug("REST request to save Familia : {}", familia);
        if (familia.getId() != null) {
            throw new BadRequestAlertException("A new familia cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Familia result = familiaRepository.save(familia);
        return ResponseEntity.created(new URI("/api/familias/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /familias : Updates an existing familia.
     *
     * @param familia the familia to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated familia,
     * or with status 400 (Bad Request) if the familia is not valid,
     * or with status 500 (Internal Server Error) if the familia couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/familias")
    public ResponseEntity<Familia> updateFamilia(@RequestBody Familia familia) throws URISyntaxException {
        log.debug("REST request to update Familia : {}", familia);
        if (familia.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Familia result = familiaRepository.save(familia);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, familia.getId().toString()))
            .body(result);
    }

    /**
     * GET  /familias : get all the familias.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of familias in body
     */
    @GetMapping("/familias")
    public ResponseEntity<List<Familia>> getAllFamilias(Pageable pageable) {
        log.debug("REST request to get a page of Familias");
        Page<Familia> page = familiaRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/familias");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * GET  /familias/:id : get the "id" familia.
     *
     * @param id the id of the familia to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the familia, or with status 404 (Not Found)
     */
    @GetMapping("/familias/{id}")
    public ResponseEntity<Familia> getFamilia(@PathVariable Long id) {
        log.debug("REST request to get Familia : {}", id);
        Optional<Familia> familia = familiaRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(familia);
    }

    /**
     * DELETE  /familias/:id : delete the "id" familia.
     *
     * @param id the id of the familia to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/familias/{id}")
    public ResponseEntity<Void> deleteFamilia(@PathVariable Long id) {
        log.debug("REST request to delete Familia : {}", id);
        familiaRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
