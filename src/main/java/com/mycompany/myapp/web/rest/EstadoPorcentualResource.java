package com.mycompany.myapp.web.rest;
import com.mycompany.myapp.domain.EstadoPorcentual;
import com.mycompany.myapp.repository.EstadoPorcentualRepository;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;
import com.mycompany.myapp.web.rest.util.HeaderUtil;
import com.mycompany.myapp.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing EstadoPorcentual.
 */
@RestController
@RequestMapping("/api")
public class EstadoPorcentualResource {

    private final Logger log = LoggerFactory.getLogger(EstadoPorcentualResource.class);

    private static final String ENTITY_NAME = "estadoPorcentual";

    private final EstadoPorcentualRepository estadoPorcentualRepository;

    public EstadoPorcentualResource(EstadoPorcentualRepository estadoPorcentualRepository) {
        this.estadoPorcentualRepository = estadoPorcentualRepository;
    }

    /**
     * POST  /estado-porcentuals : Create a new estadoPorcentual.
     *
     * @param estadoPorcentual the estadoPorcentual to create
     * @return the ResponseEntity with status 201 (Created) and with body the new estadoPorcentual, or with status 400 (Bad Request) if the estadoPorcentual has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/estado-porcentuals")
    public ResponseEntity<EstadoPorcentual> createEstadoPorcentual(@RequestBody EstadoPorcentual estadoPorcentual) throws URISyntaxException {
        log.debug("REST request to save EstadoPorcentual : {}", estadoPorcentual);
        if (estadoPorcentual.getId() != null) {
            throw new BadRequestAlertException("A new estadoPorcentual cannot already have an ID", ENTITY_NAME, "idexists");
        }
        EstadoPorcentual result = estadoPorcentualRepository.save(estadoPorcentual);
        return ResponseEntity.created(new URI("/api/estado-porcentuals/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /estado-porcentuals : Updates an existing estadoPorcentual.
     *
     * @param estadoPorcentual the estadoPorcentual to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated estadoPorcentual,
     * or with status 400 (Bad Request) if the estadoPorcentual is not valid,
     * or with status 500 (Internal Server Error) if the estadoPorcentual couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/estado-porcentuals")
    public ResponseEntity<EstadoPorcentual> updateEstadoPorcentual(@RequestBody EstadoPorcentual estadoPorcentual) throws URISyntaxException {
        log.debug("REST request to update EstadoPorcentual : {}", estadoPorcentual);
        if (estadoPorcentual.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        EstadoPorcentual result = estadoPorcentualRepository.save(estadoPorcentual);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, estadoPorcentual.getId().toString()))
            .body(result);
    }

    /**
     * GET  /estado-porcentuals : get all the estadoPorcentuals.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of estadoPorcentuals in body
     */
    @GetMapping("/estado-porcentuals")
    public ResponseEntity<List<EstadoPorcentual>> getAllEstadoPorcentuals(Pageable pageable) {
        log.debug("REST request to get a page of EstadoPorcentuals");
        Page<EstadoPorcentual> page = estadoPorcentualRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/estado-porcentuals");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * GET  /estado-porcentuals/:id : get the "id" estadoPorcentual.
     *
     * @param id the id of the estadoPorcentual to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the estadoPorcentual, or with status 404 (Not Found)
     */
    @GetMapping("/estado-porcentuals/{id}")
    public ResponseEntity<EstadoPorcentual> getEstadoPorcentual(@PathVariable Long id) {
        log.debug("REST request to get EstadoPorcentual : {}", id);
        Optional<EstadoPorcentual> estadoPorcentual = estadoPorcentualRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(estadoPorcentual);
    }

    /**
     * DELETE  /estado-porcentuals/:id : delete the "id" estadoPorcentual.
     *
     * @param id the id of the estadoPorcentual to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/estado-porcentuals/{id}")
    public ResponseEntity<Void> deleteEstadoPorcentual(@PathVariable Long id) {
        log.debug("REST request to delete EstadoPorcentual : {}", id);
        estadoPorcentualRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
