package com.mycompany.myapp.web.rest;
import com.mycompany.myapp.domain.Comercial;
import com.mycompany.myapp.repository.ComercialRepository;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;
import com.mycompany.myapp.web.rest.util.HeaderUtil;
import com.mycompany.myapp.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Comercial.
 */
@RestController
@RequestMapping("/api")
public class ComercialResource {

    private final Logger log = LoggerFactory.getLogger(ComercialResource.class);

    private static final String ENTITY_NAME = "comercial";

    private final ComercialRepository comercialRepository;

    public ComercialResource(ComercialRepository comercialRepository) {
        this.comercialRepository = comercialRepository;
    }

    /**
     * POST  /comercials : Create a new comercial.
     *
     * @param comercial the comercial to create
     * @return the ResponseEntity with status 201 (Created) and with body the new comercial, or with status 400 (Bad Request) if the comercial has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/comercials")
    public ResponseEntity<Comercial> createComercial(@RequestBody Comercial comercial) throws URISyntaxException {
        log.debug("REST request to save Comercial : {}", comercial);
        if (comercial.getId() != null) {
            throw new BadRequestAlertException("A new comercial cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Comercial result = comercialRepository.save(comercial);
        return ResponseEntity.created(new URI("/api/comercials/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /comercials : Updates an existing comercial.
     *
     * @param comercial the comercial to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated comercial,
     * or with status 400 (Bad Request) if the comercial is not valid,
     * or with status 500 (Internal Server Error) if the comercial couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/comercials")
    public ResponseEntity<Comercial> updateComercial(@RequestBody Comercial comercial) throws URISyntaxException {
        log.debug("REST request to update Comercial : {}", comercial);
        if (comercial.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Comercial result = comercialRepository.save(comercial);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, comercial.getId().toString()))
            .body(result);
    }

    /**
     * GET  /comercials : get all the comercials.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of comercials in body
     */
    @GetMapping("/comercials")
    public ResponseEntity<List<Comercial>> getAllComercials(Pageable pageable) {
        log.debug("REST request to get a page of Comercials");
        Page<Comercial> page = comercialRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/comercials");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * GET  /comercials/:id : get the "id" comercial.
     *
     * @param id the id of the comercial to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the comercial, or with status 404 (Not Found)
     */
    @GetMapping("/comercials/{id}")
    public ResponseEntity<Comercial> getComercial(@PathVariable Long id) {
        log.debug("REST request to get Comercial : {}", id);
        Optional<Comercial> comercial = comercialRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(comercial);
    }

    /**
     * DELETE  /comercials/:id : delete the "id" comercial.
     *
     * @param id the id of the comercial to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/comercials/{id}")
    public ResponseEntity<Void> deleteComercial(@PathVariable Long id) {
        log.debug("REST request to delete Comercial : {}", id);
        comercialRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
