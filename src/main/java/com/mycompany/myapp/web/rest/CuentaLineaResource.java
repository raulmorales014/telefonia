package com.mycompany.myapp.web.rest;
import com.mycompany.myapp.domain.CuentaLinea;
import com.mycompany.myapp.repository.CuentaLineaRepository;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;
import com.mycompany.myapp.web.rest.util.HeaderUtil;
import com.mycompany.myapp.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing CuentaLinea.
 */
@RestController
@RequestMapping("/api")
public class CuentaLineaResource {

    private final Logger log = LoggerFactory.getLogger(CuentaLineaResource.class);

    private static final String ENTITY_NAME = "cuentaLinea";

    private final CuentaLineaRepository cuentaLineaRepository;

    public CuentaLineaResource(CuentaLineaRepository cuentaLineaRepository) {
        this.cuentaLineaRepository = cuentaLineaRepository;
    }

    /**
     * POST  /cuenta-lineas : Create a new cuentaLinea.
     *
     * @param cuentaLinea the cuentaLinea to create
     * @return the ResponseEntity with status 201 (Created) and with body the new cuentaLinea, or with status 400 (Bad Request) if the cuentaLinea has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/cuenta-lineas")
    public ResponseEntity<CuentaLinea> createCuentaLinea(@RequestBody CuentaLinea cuentaLinea) throws URISyntaxException {
        log.debug("REST request to save CuentaLinea : {}", cuentaLinea);
        if (cuentaLinea.getId() != null) {
            throw new BadRequestAlertException("A new cuentaLinea cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CuentaLinea result = cuentaLineaRepository.save(cuentaLinea);
        return ResponseEntity.created(new URI("/api/cuenta-lineas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /cuenta-lineas : Updates an existing cuentaLinea.
     *
     * @param cuentaLinea the cuentaLinea to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated cuentaLinea,
     * or with status 400 (Bad Request) if the cuentaLinea is not valid,
     * or with status 500 (Internal Server Error) if the cuentaLinea couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/cuenta-lineas")
    public ResponseEntity<CuentaLinea> updateCuentaLinea(@RequestBody CuentaLinea cuentaLinea) throws URISyntaxException {
        log.debug("REST request to update CuentaLinea : {}", cuentaLinea);
        if (cuentaLinea.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        CuentaLinea result = cuentaLineaRepository.save(cuentaLinea);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, cuentaLinea.getId().toString()))
            .body(result);
    }

    /**
     * GET  /cuenta-lineas : get all the cuentaLineas.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of cuentaLineas in body
     */
    @GetMapping("/cuenta-lineas")
    public ResponseEntity<List<CuentaLinea>> getAllCuentaLineas(Pageable pageable) {
        log.debug("REST request to get a page of CuentaLineas");
        Page<CuentaLinea> page = cuentaLineaRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/cuenta-lineas");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * GET  /cuenta-lineas/:id : get the "id" cuentaLinea.
     *
     * @param id the id of the cuentaLinea to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the cuentaLinea, or with status 404 (Not Found)
     */
    @GetMapping("/cuenta-lineas/{id}")
    public ResponseEntity<CuentaLinea> getCuentaLinea(@PathVariable Long id) {
        log.debug("REST request to get CuentaLinea : {}", id);
        Optional<CuentaLinea> cuentaLinea = cuentaLineaRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(cuentaLinea);
    }

    /**
     * DELETE  /cuenta-lineas/:id : delete the "id" cuentaLinea.
     *
     * @param id the id of the cuentaLinea to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/cuenta-lineas/{id}")
    public ResponseEntity<Void> deleteCuentaLinea(@PathVariable Long id) {
        log.debug("REST request to delete CuentaLinea : {}", id);
        cuentaLineaRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
