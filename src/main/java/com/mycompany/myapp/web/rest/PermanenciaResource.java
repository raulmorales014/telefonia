package com.mycompany.myapp.web.rest;
import com.mycompany.myapp.domain.Permanencia;
import com.mycompany.myapp.repository.PermanenciaRepository;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;
import com.mycompany.myapp.web.rest.util.HeaderUtil;
import com.mycompany.myapp.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Permanencia.
 */
@RestController
@RequestMapping("/api")
public class PermanenciaResource {

    private final Logger log = LoggerFactory.getLogger(PermanenciaResource.class);

    private static final String ENTITY_NAME = "permanencia";

    private final PermanenciaRepository permanenciaRepository;

    public PermanenciaResource(PermanenciaRepository permanenciaRepository) {
        this.permanenciaRepository = permanenciaRepository;
    }

    /**
     * POST  /permanencias : Create a new permanencia.
     *
     * @param permanencia the permanencia to create
     * @return the ResponseEntity with status 201 (Created) and with body the new permanencia, or with status 400 (Bad Request) if the permanencia has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/permanencias")
    public ResponseEntity<Permanencia> createPermanencia(@RequestBody Permanencia permanencia) throws URISyntaxException {
        log.debug("REST request to save Permanencia : {}", permanencia);
        if (permanencia.getId() != null) {
            throw new BadRequestAlertException("A new permanencia cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Permanencia result = permanenciaRepository.save(permanencia);
        return ResponseEntity.created(new URI("/api/permanencias/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /permanencias : Updates an existing permanencia.
     *
     * @param permanencia the permanencia to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated permanencia,
     * or with status 400 (Bad Request) if the permanencia is not valid,
     * or with status 500 (Internal Server Error) if the permanencia couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/permanencias")
    public ResponseEntity<Permanencia> updatePermanencia(@RequestBody Permanencia permanencia) throws URISyntaxException {
        log.debug("REST request to update Permanencia : {}", permanencia);
        if (permanencia.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Permanencia result = permanenciaRepository.save(permanencia);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, permanencia.getId().toString()))
            .body(result);
    }

    /**
     * GET  /permanencias : get all the permanencias.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of permanencias in body
     */
    @GetMapping("/permanencias")
    public ResponseEntity<List<Permanencia>> getAllPermanencias(Pageable pageable) {
        log.debug("REST request to get a page of Permanencias");
        Page<Permanencia> page = permanenciaRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/permanencias");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * GET  /permanencias/:id : get the "id" permanencia.
     *
     * @param id the id of the permanencia to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the permanencia, or with status 404 (Not Found)
     */
    @GetMapping("/permanencias/{id}")
    public ResponseEntity<Permanencia> getPermanencia(@PathVariable Long id) {
        log.debug("REST request to get Permanencia : {}", id);
        Optional<Permanencia> permanencia = permanenciaRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(permanencia);
    }

    /**
     * DELETE  /permanencias/:id : delete the "id" permanencia.
     *
     * @param id the id of the permanencia to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/permanencias/{id}")
    public ResponseEntity<Void> deletePermanencia(@PathVariable Long id) {
        log.debug("REST request to delete Permanencia : {}", id);
        permanenciaRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
