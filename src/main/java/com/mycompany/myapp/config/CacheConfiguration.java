package com.mycompany.myapp.config;

import java.time.Duration;

import org.ehcache.config.builders.*;
import org.ehcache.jsr107.Eh107Configuration;

import io.github.jhipster.config.jcache.BeanClassLoaderAwareJCacheRegionFactory;
import io.github.jhipster.config.JHipsterProperties;

import org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.client.serviceregistry.Registration;
import org.springframework.context.annotation.*;

@Configuration
@EnableCaching
public class CacheConfiguration {

    private final javax.cache.configuration.Configuration<Object, Object> jcacheConfiguration;

    public CacheConfiguration(JHipsterProperties jHipsterProperties) {
        BeanClassLoaderAwareJCacheRegionFactory.setBeanClassLoader(this.getClass().getClassLoader());
        JHipsterProperties.Cache.Ehcache ehcache =
            jHipsterProperties.getCache().getEhcache();

        jcacheConfiguration = Eh107Configuration.fromEhcacheCacheConfiguration(
            CacheConfigurationBuilder.newCacheConfigurationBuilder(Object.class, Object.class,
                ResourcePoolsBuilder.heap(ehcache.getMaxEntries()))
                .withExpiry(ExpiryPolicyBuilder.timeToLiveExpiration(Duration.ofSeconds(ehcache.getTimeToLiveSeconds())))
                .build());
    }

    @Bean
    public JCacheManagerCustomizer cacheManagerCustomizer() {
        return cm -> {
            cm.createCache(com.mycompany.myapp.repository.UserRepository.USERS_BY_LOGIN_CACHE, jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.repository.UserRepository.USERS_BY_EMAIL_CACHE, jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.User.class.getName(), jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.Authority.class.getName(), jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.User.class.getName() + ".authorities", jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.Cliente.class.getName(), jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.Cliente.class.getName() + ".nombres", jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.Equipo.class.getName(), jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.Equipo.class.getName() + ".nombres", jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.Comercial.class.getName(), jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.Comercial.class.getName() + ".nombres", jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.Login.class.getName(), jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.Login.class.getName() + ".nombres", jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.Producto.class.getName(), jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.Producto.class.getName() + ".nombres", jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.CuentaLinea.class.getName(), jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.CuentaLinea.class.getName() + ".nombres", jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.Familia.class.getName(), jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.Familia.class.getName() + ".nombres", jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.TipoLinea.class.getName(), jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.TipoLinea.class.getName() + ".nombres", jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.Paquete.class.getName(), jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.Paquete.class.getName() + ".nombres", jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.Oferta.class.getName(), jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.Oferta.class.getName() + ".numeroOfertas", jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.EstadoPorcentual.class.getName(), jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.EstadoPorcentual.class.getName() + ".nombres", jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.TramitadoPor.class.getName(), jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.TramitadoPor.class.getName() + ".nombres", jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.Estado.class.getName(), jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.Estado.class.getName() + ".nombres", jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.Subestado.class.getName(), jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.Subestado.class.getName() + ".nombres", jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.Permanencia.class.getName(), jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.Permanencia.class.getName() + ".nombres", jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.Perfil.class.getName(), jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.Perfil.class.getName() + ".nombres", jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.Linea.class.getName(), jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.Gasto.class.getName(), jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.Empresa.class.getName(), jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.Empresa.class.getName() + ".nombres", jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.Agenda.class.getName(), jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.Cliente.class.getName() + ".ofertas", jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.Cliente.class.getName() + ".gastos", jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.Equipo.class.getName() + ".comercials", jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.Equipo.class.getName() + ".gastos", jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.Comercial.class.getName() + ".ofertas", jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.Comercial.class.getName() + ".gastos", jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.Comercial.class.getName() + ".agenda", jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.Login.class.getName() + ".comercials", jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.Producto.class.getName() + ".lineas", jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.CuentaLinea.class.getName() + ".productos", jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.Familia.class.getName() + ".productos", jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.TipoLinea.class.getName() + ".familias", jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.Paquete.class.getName() + ".lineas", jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.Oferta.class.getName() + ".lineas", jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.EstadoPorcentual.class.getName() + ".ofertas", jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.EstadoPorcentual.class.getName() + ".agenda", jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.TramitadoPor.class.getName() + ".ofertas", jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.Estado.class.getName() + ".subestados", jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.Subestado.class.getName() + ".ofertas", jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.Permanencia.class.getName() + ".lineas", jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.Perfil.class.getName() + ".logins", jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.Empresa.class.getName() + ".gastos", jcacheConfiguration);
            // jhipster-needle-ehcache-add-entry
        };
    }
}
