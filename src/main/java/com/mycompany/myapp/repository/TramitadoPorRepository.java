package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.TramitadoPor;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the TramitadoPor entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TramitadoPorRepository extends JpaRepository<TramitadoPor, Long> {

}
