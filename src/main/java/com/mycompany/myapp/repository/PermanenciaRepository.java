package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.Permanencia;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Permanencia entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PermanenciaRepository extends JpaRepository<Permanencia, Long> {

}
