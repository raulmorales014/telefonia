package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.CuentaLinea;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the CuentaLinea entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CuentaLineaRepository extends JpaRepository<CuentaLinea, Long> {

}
