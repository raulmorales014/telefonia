package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.Comercial;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Comercial entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ComercialRepository extends JpaRepository<Comercial, Long> {

}
