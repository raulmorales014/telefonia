package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.Linea;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Linea entity.
 */
@SuppressWarnings("unused")
@Repository
public interface LineaRepository extends JpaRepository<Linea, Long> {

}
