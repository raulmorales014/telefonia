package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.EstadoPorcentual;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the EstadoPorcentual entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EstadoPorcentualRepository extends JpaRepository<EstadoPorcentual, Long> {

}
