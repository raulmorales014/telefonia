package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.Subestado;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Subestado entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SubestadoRepository extends JpaRepository<Subestado, Long> {

}
