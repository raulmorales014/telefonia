package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.TipoLinea;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the TipoLinea entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TipoLineaRepository extends JpaRepository<TipoLinea, Long> {

}
