package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.Perfil;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing Perfil.
 */
public interface PerfilService {

    /**
     * Save a perfil.
     *
     * @param perfil the entity to save
     * @return the persisted entity
     */
    Perfil save(Perfil perfil);

    /**
     * Get all the perfils.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<Perfil> findAll(Pageable pageable);


    /**
     * Get the "id" perfil.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<Perfil> findOne(Long id);

    /**
     * Delete the "id" perfil.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
