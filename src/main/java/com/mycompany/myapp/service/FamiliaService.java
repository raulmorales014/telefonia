package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.Familia;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing Familia.
 */
public interface FamiliaService {

    /**
     * Save a familia.
     *
     * @param familia the entity to save
     * @return the persisted entity
     */
    Familia save(Familia familia);

    /**
     * Get all the familias.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<Familia> findAll(Pageable pageable);


    /**
     * Get the "id" familia.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<Familia> findOne(Long id);

    /**
     * Delete the "id" familia.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
