package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.Equipo;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing Equipo.
 */
public interface EquipoService {

    /**
     * Save a equipo.
     *
     * @param equipo the entity to save
     * @return the persisted entity
     */
    Equipo save(Equipo equipo);

    /**
     * Get all the equipos.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<Equipo> findAll(Pageable pageable);


    /**
     * Get the "id" equipo.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<Equipo> findOne(Long id);

    /**
     * Delete the "id" equipo.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
