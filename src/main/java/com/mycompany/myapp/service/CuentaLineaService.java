package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.CuentaLinea;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing CuentaLinea.
 */
public interface CuentaLineaService {

    /**
     * Save a cuentaLinea.
     *
     * @param cuentaLinea the entity to save
     * @return the persisted entity
     */
    CuentaLinea save(CuentaLinea cuentaLinea);

    /**
     * Get all the cuentaLineas.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<CuentaLinea> findAll(Pageable pageable);


    /**
     * Get the "id" cuentaLinea.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<CuentaLinea> findOne(Long id);

    /**
     * Delete the "id" cuentaLinea.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
