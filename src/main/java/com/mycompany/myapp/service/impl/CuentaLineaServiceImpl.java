package com.mycompany.myapp.service.impl;

import com.mycompany.myapp.service.CuentaLineaService;
import com.mycompany.myapp.domain.CuentaLinea;
import com.mycompany.myapp.repository.CuentaLineaRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing CuentaLinea.
 */
@Service
@Transactional
public class CuentaLineaServiceImpl implements CuentaLineaService {

    private final Logger log = LoggerFactory.getLogger(CuentaLineaServiceImpl.class);

    private final CuentaLineaRepository cuentaLineaRepository;

    public CuentaLineaServiceImpl(CuentaLineaRepository cuentaLineaRepository) {
        this.cuentaLineaRepository = cuentaLineaRepository;
    }

    /**
     * Save a cuentaLinea.
     *
     * @param cuentaLinea the entity to save
     * @return the persisted entity
     */
    @Override
    public CuentaLinea save(CuentaLinea cuentaLinea) {
        log.debug("Request to save CuentaLinea : {}", cuentaLinea);
        return cuentaLineaRepository.save(cuentaLinea);
    }

    /**
     * Get all the cuentaLineas.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<CuentaLinea> findAll(Pageable pageable) {
        log.debug("Request to get all CuentaLineas");
        return cuentaLineaRepository.findAll(pageable);
    }


    /**
     * Get one cuentaLinea by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<CuentaLinea> findOne(Long id) {
        log.debug("Request to get CuentaLinea : {}", id);
        return cuentaLineaRepository.findById(id);
    }

    /**
     * Delete the cuentaLinea by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete CuentaLinea : {}", id);
        cuentaLineaRepository.deleteById(id);
    }
}
