package com.mycompany.myapp.service.impl;

import com.mycompany.myapp.service.EquipoService;
import com.mycompany.myapp.domain.Equipo;
import com.mycompany.myapp.repository.EquipoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing Equipo.
 */
@Service
@Transactional
public class EquipoServiceImpl implements EquipoService {

    private final Logger log = LoggerFactory.getLogger(EquipoServiceImpl.class);

    private final EquipoRepository equipoRepository;

    public EquipoServiceImpl(EquipoRepository equipoRepository) {
        this.equipoRepository = equipoRepository;
    }

    /**
     * Save a equipo.
     *
     * @param equipo the entity to save
     * @return the persisted entity
     */
    @Override
    public Equipo save(Equipo equipo) {
        log.debug("Request to save Equipo : {}", equipo);
        return equipoRepository.save(equipo);
    }

    /**
     * Get all the equipos.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Equipo> findAll(Pageable pageable) {
        log.debug("Request to get all Equipos");
        return equipoRepository.findAll(pageable);
    }


    /**
     * Get one equipo by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<Equipo> findOne(Long id) {
        log.debug("Request to get Equipo : {}", id);
        return equipoRepository.findById(id);
    }

    /**
     * Delete the equipo by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Equipo : {}", id);
        equipoRepository.deleteById(id);
    }
}
