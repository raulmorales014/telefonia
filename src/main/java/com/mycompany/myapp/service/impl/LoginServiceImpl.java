package com.mycompany.myapp.service.impl;

import com.mycompany.myapp.service.LoginService;
import com.mycompany.myapp.domain.Login;
import com.mycompany.myapp.repository.LoginRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing Login.
 */
@Service
@Transactional
public class LoginServiceImpl implements LoginService {

    private final Logger log = LoggerFactory.getLogger(LoginServiceImpl.class);

    private final LoginRepository loginRepository;

    public LoginServiceImpl(LoginRepository loginRepository) {
        this.loginRepository = loginRepository;
    }

    /**
     * Save a login.
     *
     * @param login the entity to save
     * @return the persisted entity
     */
    @Override
    public Login save(Login login) {
        log.debug("Request to save Login : {}", login);
        return loginRepository.save(login);
    }

    /**
     * Get all the logins.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Login> findAll(Pageable pageable) {
        log.debug("Request to get all Logins");
        return loginRepository.findAll(pageable);
    }


    /**
     * Get one login by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<Login> findOne(Long id) {
        log.debug("Request to get Login : {}", id);
        return loginRepository.findById(id);
    }

    /**
     * Delete the login by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Login : {}", id);
        loginRepository.deleteById(id);
    }
}
