package com.mycompany.myapp.service.impl;

import com.mycompany.myapp.service.EstadoPorcentualService;
import com.mycompany.myapp.domain.EstadoPorcentual;
import com.mycompany.myapp.repository.EstadoPorcentualRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing EstadoPorcentual.
 */
@Service
@Transactional
public class EstadoPorcentualServiceImpl implements EstadoPorcentualService {

    private final Logger log = LoggerFactory.getLogger(EstadoPorcentualServiceImpl.class);

    private final EstadoPorcentualRepository estadoPorcentualRepository;

    public EstadoPorcentualServiceImpl(EstadoPorcentualRepository estadoPorcentualRepository) {
        this.estadoPorcentualRepository = estadoPorcentualRepository;
    }

    /**
     * Save a estadoPorcentual.
     *
     * @param estadoPorcentual the entity to save
     * @return the persisted entity
     */
    @Override
    public EstadoPorcentual save(EstadoPorcentual estadoPorcentual) {
        log.debug("Request to save EstadoPorcentual : {}", estadoPorcentual);
        return estadoPorcentualRepository.save(estadoPorcentual);
    }

    /**
     * Get all the estadoPorcentuals.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<EstadoPorcentual> findAll(Pageable pageable) {
        log.debug("Request to get all EstadoPorcentuals");
        return estadoPorcentualRepository.findAll(pageable);
    }


    /**
     * Get one estadoPorcentual by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<EstadoPorcentual> findOne(Long id) {
        log.debug("Request to get EstadoPorcentual : {}", id);
        return estadoPorcentualRepository.findById(id);
    }

    /**
     * Delete the estadoPorcentual by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete EstadoPorcentual : {}", id);
        estadoPorcentualRepository.deleteById(id);
    }
}
