package com.mycompany.myapp.service.impl;

import com.mycompany.myapp.service.TipoLineaService;
import com.mycompany.myapp.domain.TipoLinea;
import com.mycompany.myapp.repository.TipoLineaRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing TipoLinea.
 */
@Service
@Transactional
public class TipoLineaServiceImpl implements TipoLineaService {

    private final Logger log = LoggerFactory.getLogger(TipoLineaServiceImpl.class);

    private final TipoLineaRepository tipoLineaRepository;

    public TipoLineaServiceImpl(TipoLineaRepository tipoLineaRepository) {
        this.tipoLineaRepository = tipoLineaRepository;
    }

    /**
     * Save a tipoLinea.
     *
     * @param tipoLinea the entity to save
     * @return the persisted entity
     */
    @Override
    public TipoLinea save(TipoLinea tipoLinea) {
        log.debug("Request to save TipoLinea : {}", tipoLinea);
        return tipoLineaRepository.save(tipoLinea);
    }

    /**
     * Get all the tipoLineas.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<TipoLinea> findAll(Pageable pageable) {
        log.debug("Request to get all TipoLineas");
        return tipoLineaRepository.findAll(pageable);
    }


    /**
     * Get one tipoLinea by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<TipoLinea> findOne(Long id) {
        log.debug("Request to get TipoLinea : {}", id);
        return tipoLineaRepository.findById(id);
    }

    /**
     * Delete the tipoLinea by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete TipoLinea : {}", id);
        tipoLineaRepository.deleteById(id);
    }
}
