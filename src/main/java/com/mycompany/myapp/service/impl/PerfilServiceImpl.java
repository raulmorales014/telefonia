package com.mycompany.myapp.service.impl;

import com.mycompany.myapp.service.PerfilService;
import com.mycompany.myapp.domain.Perfil;
import com.mycompany.myapp.repository.PerfilRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing Perfil.
 */
@Service
@Transactional
public class PerfilServiceImpl implements PerfilService {

    private final Logger log = LoggerFactory.getLogger(PerfilServiceImpl.class);

    private final PerfilRepository perfilRepository;

    public PerfilServiceImpl(PerfilRepository perfilRepository) {
        this.perfilRepository = perfilRepository;
    }

    /**
     * Save a perfil.
     *
     * @param perfil the entity to save
     * @return the persisted entity
     */
    @Override
    public Perfil save(Perfil perfil) {
        log.debug("Request to save Perfil : {}", perfil);
        return perfilRepository.save(perfil);
    }

    /**
     * Get all the perfils.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Perfil> findAll(Pageable pageable) {
        log.debug("Request to get all Perfils");
        return perfilRepository.findAll(pageable);
    }


    /**
     * Get one perfil by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<Perfil> findOne(Long id) {
        log.debug("Request to get Perfil : {}", id);
        return perfilRepository.findById(id);
    }

    /**
     * Delete the perfil by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Perfil : {}", id);
        perfilRepository.deleteById(id);
    }
}
