package com.mycompany.myapp.service.impl;

import com.mycompany.myapp.service.ComercialService;
import com.mycompany.myapp.domain.Comercial;
import com.mycompany.myapp.repository.ComercialRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing Comercial.
 */
@Service
@Transactional
public class ComercialServiceImpl implements ComercialService {

    private final Logger log = LoggerFactory.getLogger(ComercialServiceImpl.class);

    private final ComercialRepository comercialRepository;

    public ComercialServiceImpl(ComercialRepository comercialRepository) {
        this.comercialRepository = comercialRepository;
    }

    /**
     * Save a comercial.
     *
     * @param comercial the entity to save
     * @return the persisted entity
     */
    @Override
    public Comercial save(Comercial comercial) {
        log.debug("Request to save Comercial : {}", comercial);
        return comercialRepository.save(comercial);
    }

    /**
     * Get all the comercials.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Comercial> findAll(Pageable pageable) {
        log.debug("Request to get all Comercials");
        return comercialRepository.findAll(pageable);
    }


    /**
     * Get one comercial by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<Comercial> findOne(Long id) {
        log.debug("Request to get Comercial : {}", id);
        return comercialRepository.findById(id);
    }

    /**
     * Delete the comercial by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Comercial : {}", id);
        comercialRepository.deleteById(id);
    }
}
