package com.mycompany.myapp.service.impl;

import com.mycompany.myapp.service.PermanenciaService;
import com.mycompany.myapp.domain.Permanencia;
import com.mycompany.myapp.repository.PermanenciaRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing Permanencia.
 */
@Service
@Transactional
public class PermanenciaServiceImpl implements PermanenciaService {

    private final Logger log = LoggerFactory.getLogger(PermanenciaServiceImpl.class);

    private final PermanenciaRepository permanenciaRepository;

    public PermanenciaServiceImpl(PermanenciaRepository permanenciaRepository) {
        this.permanenciaRepository = permanenciaRepository;
    }

    /**
     * Save a permanencia.
     *
     * @param permanencia the entity to save
     * @return the persisted entity
     */
    @Override
    public Permanencia save(Permanencia permanencia) {
        log.debug("Request to save Permanencia : {}", permanencia);
        return permanenciaRepository.save(permanencia);
    }

    /**
     * Get all the permanencias.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Permanencia> findAll(Pageable pageable) {
        log.debug("Request to get all Permanencias");
        return permanenciaRepository.findAll(pageable);
    }


    /**
     * Get one permanencia by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<Permanencia> findOne(Long id) {
        log.debug("Request to get Permanencia : {}", id);
        return permanenciaRepository.findById(id);
    }

    /**
     * Delete the permanencia by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Permanencia : {}", id);
        permanenciaRepository.deleteById(id);
    }
}
