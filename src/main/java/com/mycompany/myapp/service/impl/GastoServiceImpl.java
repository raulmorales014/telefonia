package com.mycompany.myapp.service.impl;

import com.mycompany.myapp.service.GastoService;
import com.mycompany.myapp.domain.Gasto;
import com.mycompany.myapp.repository.GastoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing Gasto.
 */
@Service
@Transactional
public class GastoServiceImpl implements GastoService {

    private final Logger log = LoggerFactory.getLogger(GastoServiceImpl.class);

    private final GastoRepository gastoRepository;

    public GastoServiceImpl(GastoRepository gastoRepository) {
        this.gastoRepository = gastoRepository;
    }

    /**
     * Save a gasto.
     *
     * @param gasto the entity to save
     * @return the persisted entity
     */
    @Override
    public Gasto save(Gasto gasto) {
        log.debug("Request to save Gasto : {}", gasto);
        return gastoRepository.save(gasto);
    }

    /**
     * Get all the gastos.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Gasto> findAll(Pageable pageable) {
        log.debug("Request to get all Gastos");
        return gastoRepository.findAll(pageable);
    }


    /**
     * Get one gasto by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<Gasto> findOne(Long id) {
        log.debug("Request to get Gasto : {}", id);
        return gastoRepository.findById(id);
    }

    /**
     * Delete the gasto by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Gasto : {}", id);
        gastoRepository.deleteById(id);
    }
}
