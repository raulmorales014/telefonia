package com.mycompany.myapp.service.impl;

import com.mycompany.myapp.service.LineaService;
import com.mycompany.myapp.domain.Linea;
import com.mycompany.myapp.repository.LineaRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing Linea.
 */
@Service
@Transactional
public class LineaServiceImpl implements LineaService {

    private final Logger log = LoggerFactory.getLogger(LineaServiceImpl.class);

    private final LineaRepository lineaRepository;

    public LineaServiceImpl(LineaRepository lineaRepository) {
        this.lineaRepository = lineaRepository;
    }

    /**
     * Save a linea.
     *
     * @param linea the entity to save
     * @return the persisted entity
     */
    @Override
    public Linea save(Linea linea) {
        log.debug("Request to save Linea : {}", linea);
        return lineaRepository.save(linea);
    }

    /**
     * Get all the lineas.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Linea> findAll(Pageable pageable) {
        log.debug("Request to get all Lineas");
        return lineaRepository.findAll(pageable);
    }


    /**
     * Get one linea by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<Linea> findOne(Long id) {
        log.debug("Request to get Linea : {}", id);
        return lineaRepository.findById(id);
    }

    /**
     * Delete the linea by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Linea : {}", id);
        lineaRepository.deleteById(id);
    }
}
