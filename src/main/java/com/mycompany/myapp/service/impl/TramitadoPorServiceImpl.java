package com.mycompany.myapp.service.impl;

import com.mycompany.myapp.service.TramitadoPorService;
import com.mycompany.myapp.domain.TramitadoPor;
import com.mycompany.myapp.repository.TramitadoPorRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing TramitadoPor.
 */
@Service
@Transactional
public class TramitadoPorServiceImpl implements TramitadoPorService {

    private final Logger log = LoggerFactory.getLogger(TramitadoPorServiceImpl.class);

    private final TramitadoPorRepository tramitadoPorRepository;

    public TramitadoPorServiceImpl(TramitadoPorRepository tramitadoPorRepository) {
        this.tramitadoPorRepository = tramitadoPorRepository;
    }

    /**
     * Save a tramitadoPor.
     *
     * @param tramitadoPor the entity to save
     * @return the persisted entity
     */
    @Override
    public TramitadoPor save(TramitadoPor tramitadoPor) {
        log.debug("Request to save TramitadoPor : {}", tramitadoPor);
        return tramitadoPorRepository.save(tramitadoPor);
    }

    /**
     * Get all the tramitadoPors.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<TramitadoPor> findAll(Pageable pageable) {
        log.debug("Request to get all TramitadoPors");
        return tramitadoPorRepository.findAll(pageable);
    }


    /**
     * Get one tramitadoPor by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<TramitadoPor> findOne(Long id) {
        log.debug("Request to get TramitadoPor : {}", id);
        return tramitadoPorRepository.findById(id);
    }

    /**
     * Delete the tramitadoPor by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete TramitadoPor : {}", id);
        tramitadoPorRepository.deleteById(id);
    }
}
