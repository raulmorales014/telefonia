package com.mycompany.myapp.service.impl;

import com.mycompany.myapp.service.SubestadoService;
import com.mycompany.myapp.domain.Subestado;
import com.mycompany.myapp.repository.SubestadoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing Subestado.
 */
@Service
@Transactional
public class SubestadoServiceImpl implements SubestadoService {

    private final Logger log = LoggerFactory.getLogger(SubestadoServiceImpl.class);

    private final SubestadoRepository subestadoRepository;

    public SubestadoServiceImpl(SubestadoRepository subestadoRepository) {
        this.subestadoRepository = subestadoRepository;
    }

    /**
     * Save a subestado.
     *
     * @param subestado the entity to save
     * @return the persisted entity
     */
    @Override
    public Subestado save(Subestado subestado) {
        log.debug("Request to save Subestado : {}", subestado);
        return subestadoRepository.save(subestado);
    }

    /**
     * Get all the subestados.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Subestado> findAll(Pageable pageable) {
        log.debug("Request to get all Subestados");
        return subestadoRepository.findAll(pageable);
    }


    /**
     * Get one subestado by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<Subestado> findOne(Long id) {
        log.debug("Request to get Subestado : {}", id);
        return subestadoRepository.findById(id);
    }

    /**
     * Delete the subestado by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Subestado : {}", id);
        subestadoRepository.deleteById(id);
    }
}
