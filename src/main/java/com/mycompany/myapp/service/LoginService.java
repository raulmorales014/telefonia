package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.Login;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing Login.
 */
public interface LoginService {

    /**
     * Save a login.
     *
     * @param login the entity to save
     * @return the persisted entity
     */
    Login save(Login login);

    /**
     * Get all the logins.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<Login> findAll(Pageable pageable);


    /**
     * Get the "id" login.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<Login> findOne(Long id);

    /**
     * Delete the "id" login.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
