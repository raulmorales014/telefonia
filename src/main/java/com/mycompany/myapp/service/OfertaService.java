package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.Oferta;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing Oferta.
 */
public interface OfertaService {

    /**
     * Save a oferta.
     *
     * @param oferta the entity to save
     * @return the persisted entity
     */
    Oferta save(Oferta oferta);

    /**
     * Get all the ofertas.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<Oferta> findAll(Pageable pageable);


    /**
     * Get the "id" oferta.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<Oferta> findOne(Long id);

    /**
     * Delete the "id" oferta.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
