package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.Comercial;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing Comercial.
 */
public interface ComercialService {

    /**
     * Save a comercial.
     *
     * @param comercial the entity to save
     * @return the persisted entity
     */
    Comercial save(Comercial comercial);

    /**
     * Get all the comercials.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<Comercial> findAll(Pageable pageable);


    /**
     * Get the "id" comercial.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<Comercial> findOne(Long id);

    /**
     * Delete the "id" comercial.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
