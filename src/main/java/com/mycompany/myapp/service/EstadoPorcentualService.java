package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.EstadoPorcentual;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing EstadoPorcentual.
 */
public interface EstadoPorcentualService {

    /**
     * Save a estadoPorcentual.
     *
     * @param estadoPorcentual the entity to save
     * @return the persisted entity
     */
    EstadoPorcentual save(EstadoPorcentual estadoPorcentual);

    /**
     * Get all the estadoPorcentuals.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<EstadoPorcentual> findAll(Pageable pageable);


    /**
     * Get the "id" estadoPorcentual.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<EstadoPorcentual> findOne(Long id);

    /**
     * Delete the "id" estadoPorcentual.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
