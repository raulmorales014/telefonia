package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.Subestado;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing Subestado.
 */
public interface SubestadoService {

    /**
     * Save a subestado.
     *
     * @param subestado the entity to save
     * @return the persisted entity
     */
    Subestado save(Subestado subestado);

    /**
     * Get all the subestados.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<Subestado> findAll(Pageable pageable);


    /**
     * Get the "id" subestado.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<Subestado> findOne(Long id);

    /**
     * Delete the "id" subestado.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
