package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.Permanencia;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing Permanencia.
 */
public interface PermanenciaService {

    /**
     * Save a permanencia.
     *
     * @param permanencia the entity to save
     * @return the persisted entity
     */
    Permanencia save(Permanencia permanencia);

    /**
     * Get all the permanencias.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<Permanencia> findAll(Pageable pageable);


    /**
     * Get the "id" permanencia.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<Permanencia> findOne(Long id);

    /**
     * Delete the "id" permanencia.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
