package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.Linea;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing Linea.
 */
public interface LineaService {

    /**
     * Save a linea.
     *
     * @param linea the entity to save
     * @return the persisted entity
     */
    Linea save(Linea linea);

    /**
     * Get all the lineas.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<Linea> findAll(Pageable pageable);


    /**
     * Get the "id" linea.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<Linea> findOne(Long id);

    /**
     * Delete the "id" linea.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
