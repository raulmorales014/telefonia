package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.TipoLinea;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing TipoLinea.
 */
public interface TipoLineaService {

    /**
     * Save a tipoLinea.
     *
     * @param tipoLinea the entity to save
     * @return the persisted entity
     */
    TipoLinea save(TipoLinea tipoLinea);

    /**
     * Get all the tipoLineas.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<TipoLinea> findAll(Pageable pageable);


    /**
     * Get the "id" tipoLinea.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<TipoLinea> findOne(Long id);

    /**
     * Delete the "id" tipoLinea.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
