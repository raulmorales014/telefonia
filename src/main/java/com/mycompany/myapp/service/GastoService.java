package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.Gasto;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing Gasto.
 */
public interface GastoService {

    /**
     * Save a gasto.
     *
     * @param gasto the entity to save
     * @return the persisted entity
     */
    Gasto save(Gasto gasto);

    /**
     * Get all the gastos.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<Gasto> findAll(Pageable pageable);


    /**
     * Get the "id" gasto.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<Gasto> findOne(Long id);

    /**
     * Delete the "id" gasto.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
