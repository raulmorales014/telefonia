package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.TramitadoPor;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing TramitadoPor.
 */
public interface TramitadoPorService {

    /**
     * Save a tramitadoPor.
     *
     * @param tramitadoPor the entity to save
     * @return the persisted entity
     */
    TramitadoPor save(TramitadoPor tramitadoPor);

    /**
     * Get all the tramitadoPors.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<TramitadoPor> findAll(Pageable pageable);


    /**
     * Get the "id" tramitadoPor.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<TramitadoPor> findOne(Long id);

    /**
     * Delete the "id" tramitadoPor.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
