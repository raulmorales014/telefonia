import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { IProducto } from 'app/shared/model/producto.model';
import { AccountService } from 'app/core';

import { ITEMS_PER_PAGE } from 'app/shared';
import { ProductoService } from './producto.service';

import { Exportar } from 'app/exportar/exportar.service';

@Component({
    selector: 'jhi-producto',
    templateUrl: './producto.component.html'
})
export class ProductoComponent implements OnInit, OnDestroy {
    currentAccount: any;
    productos: IProducto[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    routeData: any;
    links: any;
    totalItems: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;

    constructor(
        protected productoService: ProductoService,
        protected parseLinks: JhiParseLinks,
        protected jhiAlertService: JhiAlertService,
        protected accountService: AccountService,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected exportar: Exportar
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe(data => {
            this.page = data.pagingParams.page;
            this.previousPage = data.pagingParams.page;
            this.reverse = data.pagingParams.ascending;
            this.predicate = data.pagingParams.predicate;
        });
    }

    loadAll() {
        this.productoService
            .query({
                page: this.page - 1,
                size: this.itemsPerPage,
                sort: this.sort()
            })
            .subscribe(
                (res: HttpResponse<IProducto[]>) => this.paginateProductos(res.body, res.headers),
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/producto'], {
            queryParams: {
                page: this.page,
                size: this.itemsPerPage,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.router.navigate([
            '/producto',
            {
                page: this.page,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        ]);
        this.loadAll();
    }

    ngOnInit() {
        this.loadAll();
        this.accountService.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInProductos();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IProducto) {
        return item.id;
    }

    registerChangeInProductos() {
        this.eventSubscriber = this.eventManager.subscribe('productoListModification', response => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    protected paginateProductos(data: IProducto[], headers: HttpHeaders) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = parseInt(headers.get('X-Total-Count'), 10);
        this.productos = data;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    //  ********************************
    //  Metodos para la exportación
    //  ********************************
    generar(formato: String) {
        this.productoService
            .query({
                sort: this.sort()
            })
            .subscribe(
                (res: HttpResponse<IProducto[]>) => this.datosFull(formato, res.body, res.headers),
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    protected datosFull(formato: String, data: IProducto[], headers: HttpHeaders) {
        const tituloExportar = 'Productos';

        const filasCabecera = [
            {
                id: 'ID',
                nombre: 'Nombre',
                spirit: 'Spirit',
                ow: 'OW',
                comisionConPermanencia: 'Perman.',
                comisionSinPermanencia: 'Sin perman.',
                rappel: 'Rappel',
                precioVenta: 'Precio venta',
                linea: 'Línea',
                familia: 'Familia',
                cuentaLinea: 'Cuenta línea'
            }
        ];

        const datos = [];
        for (let j = 0; j <= data.length - 1; j++) {
            const producto: IProducto = data[j];
            datos.push({
                id: producto.id,
                nombre: producto.nombre,
                spirit: producto.spirit,
                ow: producto.ow,
                comisionConPermanencia: producto.comisionConPermanencia,
                comisionSinPermanencia: producto.comisionSinPermanencia,
                rappel: producto.rappel,
                precioVenta: producto.precioVenta,
                linea: producto.lineas,
                familia: producto.familia.nombre,
                cuentaLinea: producto.cuentaLinea.nombre
            });
        }

        switch (formato) {
            case 'PDF': {
                this.exportar.generarPDF(tituloExportar, filasCabecera, datos, false);
                break;
            }
            case 'XLS': {
                this.exportar.generarXLS(tituloExportar, datos);
                break;
            }
            default: {
                this.exportar.imprimirPDF(tituloExportar, filasCabecera, datos, false);
                break;
            }
        }
    }

    //  ********************************
    //  ********************************
}
