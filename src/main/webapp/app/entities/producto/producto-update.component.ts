import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IProducto } from 'app/shared/model/producto.model';
import { ProductoService } from './producto.service';
import { IFamilia } from 'app/shared/model/familia.model';
import { FamiliaService } from 'app/entities/familia';
import { ICuentaLinea } from 'app/shared/model/cuenta-linea.model';
import { CuentaLineaService } from 'app/entities/cuenta-linea';

@Component({
    selector: 'jhi-producto-update',
    templateUrl: './producto-update.component.html'
})
export class ProductoUpdateComponent implements OnInit {
    producto: IProducto;
    isSaving: boolean;

    familias: IFamilia[];

    cuentalineas: ICuentaLinea[];

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected productoService: ProductoService,
        protected familiaService: FamiliaService,
        protected cuentaLineaService: CuentaLineaService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ producto }) => {
            this.producto = producto;
        });
        this.familiaService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IFamilia[]>) => mayBeOk.ok),
                map((response: HttpResponse<IFamilia[]>) => response.body)
            )
            .subscribe((res: IFamilia[]) => (this.familias = res), (res: HttpErrorResponse) => this.onError(res.message));
        this.cuentaLineaService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<ICuentaLinea[]>) => mayBeOk.ok),
                map((response: HttpResponse<ICuentaLinea[]>) => response.body)
            )
            .subscribe((res: ICuentaLinea[]) => (this.cuentalineas = res), (res: HttpErrorResponse) => this.onError(res.message));
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.producto.id !== undefined) {
            this.subscribeToSaveResponse(this.productoService.update(this.producto));
        } else {
            this.subscribeToSaveResponse(this.productoService.create(this.producto));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IProducto>>) {
        result.subscribe((res: HttpResponse<IProducto>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackFamiliaById(index: number, item: IFamilia) {
        return item.id;
    }

    trackCuentaLineaById(index: number, item: ICuentaLinea) {
        return item.id;
    }
}
