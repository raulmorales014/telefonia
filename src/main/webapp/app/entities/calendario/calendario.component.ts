import { Component, OnInit, OnDestroy, ViewChild, TemplateRef } from '@angular/core';
import { OptionsInput } from '@fullcalendar/core';
import { EventInput } from '@fullcalendar/core';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGrigPlugin from '@fullcalendar/timegrid';
import interactionPlugin from '@fullcalendar/interaction';
import list from '@fullcalendar/list';
import esLocale from '@fullcalendar/core/locales/es';
import { FullCalendarComponent } from '@fullcalendar/angular';

import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { IAgenda } from 'app/shared/model/agenda.model';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';
import { AgendaService } from '../agenda/agenda.service';
import { IEstadoPorcentual } from 'app/shared/model/estado-porcentual.model';
import { EstadoPorcentualService } from 'app/entities/estado-porcentual';
import { IComercial } from 'app/shared/model/comercial.model';
import { ComercialService } from 'app/entities/comercial';
import { Subscription } from 'rxjs';
import { Moment } from 'moment';
import CalendarComponent from '@fullcalendar/core/CalendarComponent';
import { Agenda } from 'app/shared/model/agenda.model';

@Component({
    selector: 'jhi-calendario',
    templateUrl: './calendario.component.html',
    styleUrls: ['./calendario.component.css']
})
export class CalendarioComponent implements OnInit, OnDestroy {
    @ViewChild('calendar') calendarComponent: FullCalendarComponent; // the #calendar in the template
    @ViewChild('modalContent') modalContent: TemplateRef<any>;

    // Para la conexion con los Servicios
    eventoPopUp: IAgenda;
    eventoPopUpInicio: string;
    eventoPopUpFin: string;
    isSaving: boolean;
    estadoporcentuals: IEstadoPorcentual[];
    eventosAgenda: IAgenda[];
    comercials: IComercial[];
    eventSubscriber: Subscription;

    // Para el componente Calendario
    options: OptionsInput;
    events: EventInput[];
    idiomaCalendario = [esLocale];

    constructor(
        private modal: NgbModal,
        protected jhiAlertService: JhiAlertService,
        protected agendaService: AgendaService,
        protected estadoPorcentualService: EstadoPorcentualService,
        protected comercialService: ComercialService,
        protected activatedRoute: ActivatedRoute,
        protected eventManager: JhiEventManager
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.inicializarCalendario();

        this.loadPorMes();

        this.estadoPorcentualService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IEstadoPorcentual[]>) => mayBeOk.ok),
                map((response: HttpResponse<IEstadoPorcentual[]>) => response.body)
            )
            .subscribe((res: IEstadoPorcentual[]) => (this.estadoporcentuals = res), (res: HttpErrorResponse) => this.onError(res.message));

        // AHORA MISNO NO NECESITO LOS COMERCIALES PERO LOS NECESITARE
        this.comercialService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IComercial[]>) => mayBeOk.ok),
                map((response: HttpResponse<IComercial[]>) => response.body)
            )
            .subscribe((res: IComercial[]) => (this.comercials = res), (res: HttpErrorResponse) => this.onError(res.message));

        this.registerChangeInAgenda();
    }

    loadPorMes() {
        this.agendaService
            .query()
            .subscribe(
                (res: HttpResponse<IAgenda[]>) => this.colocarEventosAgendaEnCalendario(res.body, res.headers),
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    colocarEventosAgendaEnCalendario(data: IAgenda[], headers: HttpHeaders) {
        // this.links = this.parseLinks.parse(headers.get('link'));
        // this.totalItems = parseInt(headers.get('X-Total-Count'), 10);
        this.eventosAgenda = data;
        this.events = [];
        for (const eventoAgenda of this.eventosAgenda) {
            // BD to componente
            // this.inicio = this.agenda.inicio != null ? this.agenda.inicio.format(DATE_TIME_FORMAT) : null;
            // componente to BD
            // this.agenda.inicio = this.inicio != null ? moment(this.inicio, DATE_TIME_FORMAT) : null;
            const event = {
                id: eventoAgenda.id,
                title: eventoAgenda.titulo,
                start: eventoAgenda.inicio != null ? eventoAgenda.inicio.format(DATE_TIME_FORMAT) : null,
                end: eventoAgenda.fin != null ? eventoAgenda.fin.format(DATE_TIME_FORMAT) : null
                // color: 'pink',
            };
            // console.log('eventoAgenda.inicio ' + eventoAgenda.inicio);
            // console.log('event.start ' + event.start);
            this.events = this.events.concat(event);
        }
    }

    registerChangeInAgenda() {
        this.eventSubscriber = this.eventManager.subscribe('agendaListModification', response => this.loadPorMes());
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    inicializarCalendario() {
        // this.events = [
        //     {
        //         id: 155,
        //         title: 'Evento Now',
        //         start: new Date()
        //     },
        //     {
        //         id: 156,
        //         title: 'Raul Prueba',
        //         start: '2019-04-01T10:15'
        //     }
        // ];

        this.options = {
            editable: true,
            customButtons: {
                myCustomButton: {
                    text: 'EXTRA',
                    click() {
                        alert('Clic en el boton EXTRA!');
                    }
                }
            },
            header: {
                left: 'timeGridDay, timeGridWeek, dayGridMonth, listMonth',
                center: 'title',
                right: 'myCustomButton today prev,next '
            },
            plugins: [dayGridPlugin, interactionPlugin, timeGrigPlugin, list],
            views: {
                dayGridMonth: {
                    // eventLimit: 4 // Mejor no tocar esto porte al cambiar el tamaño de las celdas de la agenda se lía al hacer CLICK y pone los eventos en el dia que no es
                    // ,buttonText: 'Mes'
                },
                timeGridWeek: {
                    // eventLimit: 4
                    // ,buttonText: 'Semana'
                },
                timeGridDay: {
                    // eventLimit: 4
                    // ,buttonText: 'Dia'
                },
                listMonth: {
                    // eventLimit: 4
                    // ,buttonText: 'Agenda'
                }
            },
            defaultView: 'dayGridMonth',
            eventLimitText: 'más',
            buttonText: {
                today: 'Hoy',
                month: 'Mes',
                week: 'Semana',
                day: 'Dia',
                list: 'Agenda'
            },
            eventLimit: true,
            firstDay: 1
        };
    }

    eventDragStop(model) {
        console.log(model);
    }

    eventDragStart(model) {
        console.log(model);
    }

    // ESTE DOBLE CLIC NO FUNCIONA BIEN
    dayRender(ev) {
        ev.el.addEventListener('dblclick', () => {
            console.log('dblclick');
            this.handleDateClick(ev);
        });
    }

    handleDateClick(arg) {
        // this.events = this.events.concat({
        //     // add new event data. must create new array
        //     title: 'Nuevo evento',
        //     start: arg.date,
        //     allDay: arg.allDay,
        //     color: 'gray',
        //     // backgroundColor: 'red',
        //     textColor: 'white',
        //     borderColor: '#000',
        //     editable: true

        //     // allDay: true
        //     // allow: null
        //     // backgroundColor: (...)
        //     // borderColor: (...)
        //     // classNames: (...)
        //     // constraint: (...)
        //     // durationEditable: null
        //     // end: null
        //     // extendedProps: Object
        //     // groupId: ""
        //     // id: ""
        //     // overlap: (...)
        //     // rendering: (...)
        //     // source: (...)
        //     // start: Mon Apr 01 2019 00:00:00 GMT+0200 (hora de verano de Europa central)
        //     // startEditable: (...)
        //     // textColor: (...)
        //     // title: "Raul Prueba"
        //     // url: 'http://google.com/'
        // });

        // PARA AGRUPAR EVENTOS
        // https://github.com/fullcalendar/fullcalendar/blob/master/tests/manual/options.html
        // {
        //     groupId: 999,
        //     title: 'Repeating Event',
        //     start: new Date(y, m, d-3, 16, 0),
        //     allDay: false
        //   },
        //   {
        //     groupId: 999,
        //     title: 'Repeating Event',
        //     start: new Date(y, m, d+4, 16, 0),
        //     allDay: false
        //   },

        this.eventoPopUp = {
            id: undefined,
            titulo: null
        };
        this.eventoPopUpInicio = arg.date != null ? moment(arg.date).format(DATE_TIME_FORMAT) : null;
        this.eventoPopUpFin = null;

        this.modal.open(this.modalContent, { size: 'lg' });
    }

    handleEventClick(arg) {
        // // GET ID DE BD - id: arg.event.id
        // this.agendaService.find(arg.event.id).pipe(map((agenda: HttpResponse<Agenda>) => {
        //     if (agenda !== undefined ){
        //         console.log('PEPITO PEPITO');
        //     }
        // }));
        
        this.eventoPopUp = {
            id: arg.event.id,
            titulo: arg.event.title
        };
        this.eventoPopUpInicio = arg.event.start != null ? moment(arg.event.start).format(DATE_TIME_FORMAT) : null;
        this.eventoPopUpFin = arg.event.end != null ? moment(arg.event.end).format(DATE_TIME_FORMAT) : null;
        this.modal.open(this.modalContent, { size: 'lg' });
    }

    guardarEvento() {
        this.modal.dismissAll();
        console.log('GUARDAR EVENTO ' + JSON.stringify(this.eventoPopUp));
        this.isSaving = true;
        this.eventoPopUp.inicio = this.eventoPopUpInicio != null ? moment(this.eventoPopUpInicio, DATE_TIME_FORMAT) : null;
        this.eventoPopUp.fin = this.eventoPopUpFin != null ? moment(this.eventoPopUpFin, DATE_TIME_FORMAT) : null;
        if (this.eventoPopUp.id !== undefined) {
            console.log('update');
            this.subscribeToSaveResponse(this.agendaService.update(this.eventoPopUp));
        } else {
            console.log('create');
            this.subscribeToSaveResponse(this.agendaService.create(this.eventoPopUp));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IAgenda>>) {
        result.subscribe((res: HttpResponse<IAgenda>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.loadPorMes();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
