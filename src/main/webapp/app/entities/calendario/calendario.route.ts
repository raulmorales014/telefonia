import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Agenda } from 'app/shared/model/agenda.model';
import { AgendaService } from '../agenda/agenda.service';
import { IAgenda } from 'app/shared/model/agenda.model';
import { CalendarioComponent } from './calendario.component';

@Injectable({ providedIn: 'root' })
export class CalendarioResolve implements Resolve<IAgenda> {
    constructor(private service: AgendaService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IAgenda> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<Agenda>) => response.ok),
                map((agenda: HttpResponse<Agenda>) => agenda.body)
            );
        }
        return of(new Agenda());
    }
}

export const calendarioRoute: Routes = [
    {
        path: '',
        component: CalendarioComponent,
        resolve: {
            pagingParams: JhiResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            defaultSort: 'id,asc',
            pageTitle: 'telefoniaApp.calendario.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];
