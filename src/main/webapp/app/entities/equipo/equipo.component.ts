import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { IEquipo } from 'app/shared/model/equipo.model';
import { AccountService } from 'app/core';

import { ITEMS_PER_PAGE } from 'app/shared';
import { EquipoService } from './equipo.service';

import { Exportar } from 'app/exportar/exportar.service';

@Component({
    selector: 'jhi-equipo',
    templateUrl: './equipo.component.html'
})
export class EquipoComponent implements OnInit, OnDestroy {
    currentAccount: any;
    equipos: IEquipo[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    routeData: any;
    links: any;
    totalItems: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;

    constructor(
        protected equipoService: EquipoService,
        protected parseLinks: JhiParseLinks,
        protected jhiAlertService: JhiAlertService,
        protected accountService: AccountService,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected exportar: Exportar
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe(data => {
            this.page = data.pagingParams.page;
            this.previousPage = data.pagingParams.page;
            this.reverse = data.pagingParams.ascending;
            this.predicate = data.pagingParams.predicate;
        });
    }

    loadAll() {
        this.equipoService
            .query({
                page: this.page - 1,
                size: this.itemsPerPage,
                sort: this.sort()
            })
            .subscribe(
                (res: HttpResponse<IEquipo[]>) => this.paginateEquipos(res.body, res.headers),
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/equipo'], {
            queryParams: {
                page: this.page,
                size: this.itemsPerPage,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.router.navigate([
            '/equipo',
            {
                page: this.page,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        ]);
        this.loadAll();
    }

    ngOnInit() {
        this.loadAll();
        this.accountService.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInEquipos();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IEquipo) {
        return item.id;
    }

    registerChangeInEquipos() {
        this.eventSubscriber = this.eventManager.subscribe('equipoListModification', response => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    protected paginateEquipos(data: IEquipo[], headers: HttpHeaders) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = parseInt(headers.get('X-Total-Count'), 10);
        this.equipos = data;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    //  ********************************
    //  Metodos para la exportación
    //  ********************************
    generar(formato: String) {
        this.equipoService
            .query({
                sort: this.sort()
            })
            .subscribe(
                (res: HttpResponse<IEquipo[]>) => this.datosFull(formato, res.body, res.headers),
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    protected datosFull(formato: String, data: IEquipo[], headers: HttpHeaders) {
        // this.tituloExportar = this.translateService.instant('telefoniaApp.familia.home.title');
        // this.filasCabecera = [{
        //     id: this.translateService.instant('global.field.id'),
        //     nombre: this.translateService.instant('telefoniaApp.familia.nombre'),
        //     tipoLinea: this.translateService.instant('telefoniaApp.familia.tipoLinea'),
        // }];
        const tituloExportar = 'Equipos';
        const filasCabecera = [
            {
                id: 'ID',
                nombre: 'Nombre'
            }
        ];

        const datos = [];
        for (let j = 0; j <= data.length - 1; j++) {
            const equipo: IEquipo = data[j];
            datos.push({
                id: equipo.id,
                nombre: equipo.nombre
                // ,tipoLinea: familia.tipoLinea ? familia.tipoLinea.nombre : ''
            });
        }

        switch (formato) {
            case 'PDF': {
                this.exportar.generarPDF(tituloExportar, filasCabecera, datos, true);
                break;
            }
            case 'XLS': {
                this.exportar.generarXLS(tituloExportar, datos);
                break;
            }
            default: {
                this.exportar.imprimirPDF(tituloExportar, filasCabecera, datos, true);
                break;
            }
        }
    }

    //  ********************************
    //  ********************************
}
