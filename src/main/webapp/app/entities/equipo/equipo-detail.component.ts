import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IEquipo } from 'app/shared/model/equipo.model';

@Component({
    selector: 'jhi-equipo-detail',
    templateUrl: './equipo-detail.component.html'
})
export class EquipoDetailComponent implements OnInit {
    equipo: IEquipo;

    constructor(protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ equipo }) => {
            this.equipo = equipo;
        });
    }

    previousState() {
        window.history.back();
    }
}
