import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { IEquipo } from 'app/shared/model/equipo.model';
import { EquipoService } from './equipo.service';

@Component({
    selector: 'jhi-equipo-update',
    templateUrl: './equipo-update.component.html'
})
export class EquipoUpdateComponent implements OnInit {
    equipo: IEquipo;
    isSaving: boolean;

    constructor(protected equipoService: EquipoService, protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ equipo }) => {
            this.equipo = equipo;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.equipo.id !== undefined) {
            this.subscribeToSaveResponse(this.equipoService.update(this.equipo));
        } else {
            this.subscribeToSaveResponse(this.equipoService.create(this.equipo));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IEquipo>>) {
        result.subscribe((res: HttpResponse<IEquipo>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }
}
