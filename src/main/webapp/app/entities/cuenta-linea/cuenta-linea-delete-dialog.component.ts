import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ICuentaLinea } from 'app/shared/model/cuenta-linea.model';
import { CuentaLineaService } from './cuenta-linea.service';

@Component({
    selector: 'jhi-cuenta-linea-delete-dialog',
    templateUrl: './cuenta-linea-delete-dialog.component.html'
})
export class CuentaLineaDeleteDialogComponent {
    cuentaLinea: ICuentaLinea;

    constructor(
        protected cuentaLineaService: CuentaLineaService,
        public activeModal: NgbActiveModal,
        protected eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.cuentaLineaService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'cuentaLineaListModification',
                content: 'Deleted an cuentaLinea'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-cuenta-linea-delete-popup',
    template: ''
})
export class CuentaLineaDeletePopupComponent implements OnInit, OnDestroy {
    protected ngbModalRef: NgbModalRef;

    constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ cuentaLinea }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(CuentaLineaDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.cuentaLinea = cuentaLinea;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate(['/cuenta-linea', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate(['/cuenta-linea', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
