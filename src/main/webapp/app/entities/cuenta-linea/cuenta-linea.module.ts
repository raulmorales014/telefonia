import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { TelefoniaSharedModule } from 'app/shared';
import {
    CuentaLineaComponent,
    CuentaLineaDetailComponent,
    CuentaLineaUpdateComponent,
    CuentaLineaDeletePopupComponent,
    CuentaLineaDeleteDialogComponent,
    cuentaLineaRoute,
    cuentaLineaPopupRoute
} from './';

const ENTITY_STATES = [...cuentaLineaRoute, ...cuentaLineaPopupRoute];

@NgModule({
    imports: [TelefoniaSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        CuentaLineaComponent,
        CuentaLineaDetailComponent,
        CuentaLineaUpdateComponent,
        CuentaLineaDeleteDialogComponent,
        CuentaLineaDeletePopupComponent
    ],
    entryComponents: [CuentaLineaComponent, CuentaLineaUpdateComponent, CuentaLineaDeleteDialogComponent, CuentaLineaDeletePopupComponent],
    providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TelefoniaCuentaLineaModule {
    constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
        this.languageHelper.language.subscribe((languageKey: string) => {
            if (languageKey !== undefined) {
                this.languageService.changeLanguage(languageKey);
            }
        });
    }
}
