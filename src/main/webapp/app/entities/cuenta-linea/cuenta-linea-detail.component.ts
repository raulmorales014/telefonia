import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ICuentaLinea } from 'app/shared/model/cuenta-linea.model';

@Component({
    selector: 'jhi-cuenta-linea-detail',
    templateUrl: './cuenta-linea-detail.component.html'
})
export class CuentaLineaDetailComponent implements OnInit {
    cuentaLinea: ICuentaLinea;

    constructor(protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ cuentaLinea }) => {
            this.cuentaLinea = cuentaLinea;
        });
    }

    previousState() {
        window.history.back();
    }
}
