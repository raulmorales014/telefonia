import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { ICuentaLinea } from 'app/shared/model/cuenta-linea.model';
import { CuentaLineaService } from './cuenta-linea.service';

@Component({
    selector: 'jhi-cuenta-linea-update',
    templateUrl: './cuenta-linea-update.component.html'
})
export class CuentaLineaUpdateComponent implements OnInit {
    cuentaLinea: ICuentaLinea;
    isSaving: boolean;

    constructor(protected cuentaLineaService: CuentaLineaService, protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ cuentaLinea }) => {
            this.cuentaLinea = cuentaLinea;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.cuentaLinea.id !== undefined) {
            this.subscribeToSaveResponse(this.cuentaLineaService.update(this.cuentaLinea));
        } else {
            this.subscribeToSaveResponse(this.cuentaLineaService.create(this.cuentaLinea));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<ICuentaLinea>>) {
        result.subscribe((res: HttpResponse<ICuentaLinea>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }
}
