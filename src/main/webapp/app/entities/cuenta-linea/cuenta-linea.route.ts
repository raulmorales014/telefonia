import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { CuentaLinea } from 'app/shared/model/cuenta-linea.model';
import { CuentaLineaService } from './cuenta-linea.service';
import { CuentaLineaComponent } from './cuenta-linea.component';
import { CuentaLineaDetailComponent } from './cuenta-linea-detail.component';
import { CuentaLineaUpdateComponent } from './cuenta-linea-update.component';
import { CuentaLineaDeletePopupComponent } from './cuenta-linea-delete-dialog.component';
import { ICuentaLinea } from 'app/shared/model/cuenta-linea.model';

@Injectable({ providedIn: 'root' })
export class CuentaLineaResolve implements Resolve<ICuentaLinea> {
    constructor(private service: CuentaLineaService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ICuentaLinea> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<CuentaLinea>) => response.ok),
                map((cuentaLinea: HttpResponse<CuentaLinea>) => cuentaLinea.body)
            );
        }
        return of(new CuentaLinea());
    }
}

export const cuentaLineaRoute: Routes = [
    {
        path: '',
        component: CuentaLineaComponent,
        resolve: {
            pagingParams: JhiResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            defaultSort: 'id,asc',
            pageTitle: 'telefoniaApp.cuentaLinea.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/view',
        component: CuentaLineaDetailComponent,
        resolve: {
            cuentaLinea: CuentaLineaResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'telefoniaApp.cuentaLinea.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'new',
        component: CuentaLineaUpdateComponent,
        resolve: {
            cuentaLinea: CuentaLineaResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'telefoniaApp.cuentaLinea.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/edit',
        component: CuentaLineaUpdateComponent,
        resolve: {
            cuentaLinea: CuentaLineaResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'telefoniaApp.cuentaLinea.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const cuentaLineaPopupRoute: Routes = [
    {
        path: ':id/delete',
        component: CuentaLineaDeletePopupComponent,
        resolve: {
            cuentaLinea: CuentaLineaResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'telefoniaApp.cuentaLinea.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
