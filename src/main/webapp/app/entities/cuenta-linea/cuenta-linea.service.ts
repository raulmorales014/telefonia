import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ICuentaLinea } from 'app/shared/model/cuenta-linea.model';

type EntityResponseType = HttpResponse<ICuentaLinea>;
type EntityArrayResponseType = HttpResponse<ICuentaLinea[]>;

@Injectable({ providedIn: 'root' })
export class CuentaLineaService {
    public resourceUrl = SERVER_API_URL + 'api/cuenta-lineas';

    constructor(protected http: HttpClient) {}

    create(cuentaLinea: ICuentaLinea): Observable<EntityResponseType> {
        return this.http.post<ICuentaLinea>(this.resourceUrl, cuentaLinea, { observe: 'response' });
    }

    update(cuentaLinea: ICuentaLinea): Observable<EntityResponseType> {
        return this.http.put<ICuentaLinea>(this.resourceUrl, cuentaLinea, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<ICuentaLinea>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<ICuentaLinea[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
