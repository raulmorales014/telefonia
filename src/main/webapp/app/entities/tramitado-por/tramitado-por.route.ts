import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { TramitadoPor } from 'app/shared/model/tramitado-por.model';
import { TramitadoPorService } from './tramitado-por.service';
import { TramitadoPorComponent } from './tramitado-por.component';
import { TramitadoPorDetailComponent } from './tramitado-por-detail.component';
import { TramitadoPorUpdateComponent } from './tramitado-por-update.component';
import { TramitadoPorDeletePopupComponent } from './tramitado-por-delete-dialog.component';
import { ITramitadoPor } from 'app/shared/model/tramitado-por.model';

@Injectable({ providedIn: 'root' })
export class TramitadoPorResolve implements Resolve<ITramitadoPor> {
    constructor(private service: TramitadoPorService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ITramitadoPor> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<TramitadoPor>) => response.ok),
                map((tramitadoPor: HttpResponse<TramitadoPor>) => tramitadoPor.body)
            );
        }
        return of(new TramitadoPor());
    }
}

export const tramitadoPorRoute: Routes = [
    {
        path: '',
        component: TramitadoPorComponent,
        resolve: {
            pagingParams: JhiResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            defaultSort: 'id,asc',
            pageTitle: 'telefoniaApp.tramitadoPor.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/view',
        component: TramitadoPorDetailComponent,
        resolve: {
            tramitadoPor: TramitadoPorResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'telefoniaApp.tramitadoPor.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'new',
        component: TramitadoPorUpdateComponent,
        resolve: {
            tramitadoPor: TramitadoPorResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'telefoniaApp.tramitadoPor.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/edit',
        component: TramitadoPorUpdateComponent,
        resolve: {
            tramitadoPor: TramitadoPorResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'telefoniaApp.tramitadoPor.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const tramitadoPorPopupRoute: Routes = [
    {
        path: ':id/delete',
        component: TramitadoPorDeletePopupComponent,
        resolve: {
            tramitadoPor: TramitadoPorResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'telefoniaApp.tramitadoPor.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
