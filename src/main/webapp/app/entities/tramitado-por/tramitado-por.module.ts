import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { TelefoniaSharedModule } from 'app/shared';
import {
    TramitadoPorComponent,
    TramitadoPorDetailComponent,
    TramitadoPorUpdateComponent,
    TramitadoPorDeletePopupComponent,
    TramitadoPorDeleteDialogComponent,
    tramitadoPorRoute,
    tramitadoPorPopupRoute
} from './';

const ENTITY_STATES = [...tramitadoPorRoute, ...tramitadoPorPopupRoute];

@NgModule({
    imports: [TelefoniaSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        TramitadoPorComponent,
        TramitadoPorDetailComponent,
        TramitadoPorUpdateComponent,
        TramitadoPorDeleteDialogComponent,
        TramitadoPorDeletePopupComponent
    ],
    entryComponents: [
        TramitadoPorComponent,
        TramitadoPorUpdateComponent,
        TramitadoPorDeleteDialogComponent,
        TramitadoPorDeletePopupComponent
    ],
    providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TelefoniaTramitadoPorModule {
    constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
        this.languageHelper.language.subscribe((languageKey: string) => {
            if (languageKey !== undefined) {
                this.languageService.changeLanguage(languageKey);
            }
        });
    }
}
