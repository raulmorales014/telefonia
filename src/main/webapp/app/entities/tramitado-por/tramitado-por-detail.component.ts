import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ITramitadoPor } from 'app/shared/model/tramitado-por.model';

@Component({
    selector: 'jhi-tramitado-por-detail',
    templateUrl: './tramitado-por-detail.component.html'
})
export class TramitadoPorDetailComponent implements OnInit {
    tramitadoPor: ITramitadoPor;

    constructor(protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ tramitadoPor }) => {
            this.tramitadoPor = tramitadoPor;
        });
    }

    previousState() {
        window.history.back();
    }
}
