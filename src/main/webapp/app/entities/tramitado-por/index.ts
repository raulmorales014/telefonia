export * from './tramitado-por.service';
export * from './tramitado-por-update.component';
export * from './tramitado-por-delete-dialog.component';
export * from './tramitado-por-detail.component';
export * from './tramitado-por.component';
export * from './tramitado-por.route';
