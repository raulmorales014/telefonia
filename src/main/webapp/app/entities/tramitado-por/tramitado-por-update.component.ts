import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { ITramitadoPor } from 'app/shared/model/tramitado-por.model';
import { TramitadoPorService } from './tramitado-por.service';

@Component({
    selector: 'jhi-tramitado-por-update',
    templateUrl: './tramitado-por-update.component.html'
})
export class TramitadoPorUpdateComponent implements OnInit {
    tramitadoPor: ITramitadoPor;
    isSaving: boolean;

    constructor(protected tramitadoPorService: TramitadoPorService, protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ tramitadoPor }) => {
            this.tramitadoPor = tramitadoPor;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.tramitadoPor.id !== undefined) {
            this.subscribeToSaveResponse(this.tramitadoPorService.update(this.tramitadoPor));
        } else {
            this.subscribeToSaveResponse(this.tramitadoPorService.create(this.tramitadoPor));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<ITramitadoPor>>) {
        result.subscribe((res: HttpResponse<ITramitadoPor>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }
}
