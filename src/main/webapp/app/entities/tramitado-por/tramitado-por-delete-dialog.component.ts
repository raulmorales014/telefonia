import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ITramitadoPor } from 'app/shared/model/tramitado-por.model';
import { TramitadoPorService } from './tramitado-por.service';

@Component({
    selector: 'jhi-tramitado-por-delete-dialog',
    templateUrl: './tramitado-por-delete-dialog.component.html'
})
export class TramitadoPorDeleteDialogComponent {
    tramitadoPor: ITramitadoPor;

    constructor(
        protected tramitadoPorService: TramitadoPorService,
        public activeModal: NgbActiveModal,
        protected eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.tramitadoPorService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'tramitadoPorListModification',
                content: 'Deleted an tramitadoPor'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-tramitado-por-delete-popup',
    template: ''
})
export class TramitadoPorDeletePopupComponent implements OnInit, OnDestroy {
    protected ngbModalRef: NgbModalRef;

    constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ tramitadoPor }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(TramitadoPorDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.tramitadoPor = tramitadoPor;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate(['/tramitado-por', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate(['/tramitado-por', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
