import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ITramitadoPor } from 'app/shared/model/tramitado-por.model';

type EntityResponseType = HttpResponse<ITramitadoPor>;
type EntityArrayResponseType = HttpResponse<ITramitadoPor[]>;

@Injectable({ providedIn: 'root' })
export class TramitadoPorService {
    public resourceUrl = SERVER_API_URL + 'api/tramitado-pors';

    constructor(protected http: HttpClient) {}

    create(tramitadoPor: ITramitadoPor): Observable<EntityResponseType> {
        return this.http.post<ITramitadoPor>(this.resourceUrl, tramitadoPor, { observe: 'response' });
    }

    update(tramitadoPor: ITramitadoPor): Observable<EntityResponseType> {
        return this.http.put<ITramitadoPor>(this.resourceUrl, tramitadoPor, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<ITramitadoPor>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<ITramitadoPor[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
