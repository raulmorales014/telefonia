import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { TelefoniaSharedModule } from 'app/shared';
import {
    OfertaComponent,
    OfertaDetailComponent,
    OfertaUpdateComponent,
    OfertaDeletePopupComponent,
    OfertaDeleteDialogComponent,
    ofertaRoute,
    ofertaPopupRoute
} from './';

const ENTITY_STATES = [...ofertaRoute, ...ofertaPopupRoute];

@NgModule({
    imports: [TelefoniaSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [OfertaComponent, OfertaDetailComponent, OfertaUpdateComponent, OfertaDeleteDialogComponent, OfertaDeletePopupComponent],
    entryComponents: [OfertaComponent, OfertaUpdateComponent, OfertaDeleteDialogComponent, OfertaDeletePopupComponent],
    providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TelefoniaOfertaModule {
    constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
        this.languageHelper.language.subscribe((languageKey: string) => {
            if (languageKey !== undefined) {
                this.languageService.changeLanguage(languageKey);
            }
        });
    }
}
