export * from './oferta.service';
export * from './oferta-update.component';
export * from './oferta-delete-dialog.component';
export * from './oferta-detail.component';
export * from './oferta.component';
export * from './oferta.route';
