import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { JhiAlertService } from 'ng-jhipster';
import { IOferta } from 'app/shared/model/oferta.model';
import { OfertaService } from './oferta.service';
import { IEstadoPorcentual } from 'app/shared/model/estado-porcentual.model';
import { EstadoPorcentualService } from 'app/entities/estado-porcentual';
import { IComercial } from 'app/shared/model/comercial.model';
import { ComercialService } from 'app/entities/comercial';
import { ICliente } from 'app/shared/model/cliente.model';
import { ClienteService } from 'app/entities/cliente';
import { ITramitadoPor } from 'app/shared/model/tramitado-por.model';
import { TramitadoPorService } from 'app/entities/tramitado-por';
import { ISubestado } from 'app/shared/model/subestado.model';
import { SubestadoService } from 'app/entities/subestado';

@Component({
    selector: 'jhi-oferta-update',
    templateUrl: './oferta-update.component.html'
})
export class OfertaUpdateComponent implements OnInit {
    oferta: IOferta;
    isSaving: boolean;

    estadoporcentuals: IEstadoPorcentual[];

    comercials: IComercial[];

    clientes: ICliente[];

    tramitadopors: ITramitadoPor[];

    subestados: ISubestado[];
    fechaDp: any;

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected ofertaService: OfertaService,
        protected estadoPorcentualService: EstadoPorcentualService,
        protected comercialService: ComercialService,
        protected clienteService: ClienteService,
        protected tramitadoPorService: TramitadoPorService,
        protected subestadoService: SubestadoService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ oferta }) => {
            this.oferta = oferta;
        });
        this.estadoPorcentualService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IEstadoPorcentual[]>) => mayBeOk.ok),
                map((response: HttpResponse<IEstadoPorcentual[]>) => response.body)
            )
            .subscribe((res: IEstadoPorcentual[]) => (this.estadoporcentuals = res), (res: HttpErrorResponse) => this.onError(res.message));
        this.comercialService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IComercial[]>) => mayBeOk.ok),
                map((response: HttpResponse<IComercial[]>) => response.body)
            )
            .subscribe((res: IComercial[]) => (this.comercials = res), (res: HttpErrorResponse) => this.onError(res.message));
        this.clienteService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<ICliente[]>) => mayBeOk.ok),
                map((response: HttpResponse<ICliente[]>) => response.body)
            )
            .subscribe((res: ICliente[]) => (this.clientes = res), (res: HttpErrorResponse) => this.onError(res.message));
        this.tramitadoPorService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<ITramitadoPor[]>) => mayBeOk.ok),
                map((response: HttpResponse<ITramitadoPor[]>) => response.body)
            )
            .subscribe((res: ITramitadoPor[]) => (this.tramitadopors = res), (res: HttpErrorResponse) => this.onError(res.message));
        this.subestadoService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<ISubestado[]>) => mayBeOk.ok),
                map((response: HttpResponse<ISubestado[]>) => response.body)
            )
            .subscribe((res: ISubestado[]) => (this.subestados = res), (res: HttpErrorResponse) => this.onError(res.message));
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.oferta.id !== undefined) {
            this.subscribeToSaveResponse(this.ofertaService.update(this.oferta));
        } else {
            this.subscribeToSaveResponse(this.ofertaService.create(this.oferta));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IOferta>>) {
        result.subscribe((res: HttpResponse<IOferta>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackEstadoPorcentualById(index: number, item: IEstadoPorcentual) {
        return item.id;
    }

    trackComercialById(index: number, item: IComercial) {
        return item.id;
    }

    trackClienteById(index: number, item: ICliente) {
        return item.id;
    }

    trackTramitadoPorById(index: number, item: ITramitadoPor) {
        return item.id;
    }

    trackSubestadoById(index: number, item: ISubestado) {
        return item.id;
    }
}
