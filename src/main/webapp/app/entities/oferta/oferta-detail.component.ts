import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IOferta } from 'app/shared/model/oferta.model';

@Component({
    selector: 'jhi-oferta-detail',
    templateUrl: './oferta-detail.component.html'
})
export class OfertaDetailComponent implements OnInit {
    oferta: IOferta;

    constructor(protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ oferta }) => {
            this.oferta = oferta;
        });
    }

    previousState() {
        window.history.back();
    }
}
