import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IOferta } from 'app/shared/model/oferta.model';
import { OfertaService } from './oferta.service';

@Component({
    selector: 'jhi-oferta-delete-dialog',
    templateUrl: './oferta-delete-dialog.component.html'
})
export class OfertaDeleteDialogComponent {
    oferta: IOferta;

    constructor(protected ofertaService: OfertaService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.ofertaService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'ofertaListModification',
                content: 'Deleted an oferta'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-oferta-delete-popup',
    template: ''
})
export class OfertaDeletePopupComponent implements OnInit, OnDestroy {
    protected ngbModalRef: NgbModalRef;

    constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ oferta }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(OfertaDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
                this.ngbModalRef.componentInstance.oferta = oferta;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate(['/oferta', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate(['/oferta', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
