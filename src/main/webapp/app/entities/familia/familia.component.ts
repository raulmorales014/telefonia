import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { HttpErrorResponse, HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { IFamilia } from 'app/shared/model/familia.model';
import { AccountService } from 'app/core';

import { ITEMS_PER_PAGE } from 'app/shared';
import { FamiliaService } from './familia.service';

// import { TranslateService } from '@ngx-translate/core';
import { Exportar } from 'app/exportar/exportar.service';

@Component({
    selector: 'jhi-familia',
    templateUrl: './familia.component.html'
})
export class FamiliaComponent implements OnInit, OnDestroy {
    currentAccount: any;
    familias: IFamilia[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    routeData: any;
    links: any;
    totalItems: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;

    constructor(
        protected familiaService: FamiliaService,
        protected parseLinks: JhiParseLinks,
        protected jhiAlertService: JhiAlertService,
        protected accountService: AccountService,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        // protected translateService: TranslateService,
        protected exportar: Exportar
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe(data => {
            this.page = data.pagingParams.page;
            this.previousPage = data.pagingParams.page;
            this.reverse = data.pagingParams.ascending;
            this.predicate = data.pagingParams.predicate;
        });
    }

    loadAll() {
        this.familiaService
            .query({
                page: this.page - 1,
                size: this.itemsPerPage,
                sort: this.sort()
            })
            .subscribe(
                (res: HttpResponse<IFamilia[]>) => this.paginateFamilias(res.body, res.headers),
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/familia'], {
            queryParams: {
                page: this.page,
                size: this.itemsPerPage,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.router.navigate([
            '/familia',
            {
                page: this.page,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        ]);
        this.loadAll();
    }

    ngOnInit() {
        this.loadAll();
        this.accountService.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInFamilias();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IFamilia) {
        return item.id;
    }

    registerChangeInFamilias() {
        this.eventSubscriber = this.eventManager.subscribe('familiaListModification', response => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    protected paginateFamilias(data: IFamilia[], headers: HttpHeaders) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = parseInt(headers.get('X-Total-Count'), 10);
        this.familias = data;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    //  ********************************
    //  Metodos para la exportación
    //  ********************************
    generar(formato: String) {
        this.familiaService
            .query({
                sort: this.sort()
            })
            .subscribe(
                (res: HttpResponse<IFamilia[]>) => this.datosFull(formato, res.body, res.headers),
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    protected datosFull(formato: String, data: IFamilia[], headers: HttpHeaders) {
        // this.tituloExportar = this.translateService.instant('telefoniaApp.familia.home.title');
        // this.filasCabecera = [{
        //     id: this.translateService.instant('global.field.id'),
        //     nombre: this.translateService.instant('telefoniaApp.familia.nombre'),
        //     tipoLinea: this.translateService.instant('telefoniaApp.familia.tipoLinea'),
        // }];
        const tituloExportar = 'Familias';
        const filasCabecera = [
            {
                id: 'ID',
                nombre: 'Nombre',
                tipoLinea: 'Tipo linea'
            }
        ];

        const datos = [];
        for (let j = 0; j <= data.length - 1; j++) {
            const familia: IFamilia = data[j];
            datos.push({
                id: familia.id,
                nombre: familia.nombre,
                tipoLinea: familia.tipoLinea ? familia.tipoLinea.nombre : ''
            });
        }

        switch (formato) {
            case 'PDF': {
                this.exportar.generarPDF(tituloExportar, filasCabecera, datos, true);
                break;
            }
            case 'XLS': {
                this.exportar.generarXLS(tituloExportar, datos);
                break;
            }
            default: {
                this.exportar.imprimirPDF(tituloExportar, filasCabecera, datos, true);
                break;
            }
        }
    }

    //  ********************************
    //  ********************************
}
