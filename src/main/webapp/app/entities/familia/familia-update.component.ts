import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IFamilia } from 'app/shared/model/familia.model';
import { FamiliaService } from './familia.service';
import { ITipoLinea } from 'app/shared/model/tipo-linea.model';
import { TipoLineaService } from 'app/entities/tipo-linea';

@Component({
    selector: 'jhi-familia-update',
    templateUrl: './familia-update.component.html'
})
export class FamiliaUpdateComponent implements OnInit {
    familia: IFamilia;
    isSaving: boolean;

    tipolineas: ITipoLinea[];

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected familiaService: FamiliaService,
        protected tipoLineaService: TipoLineaService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ familia }) => {
            this.familia = familia;
        });
        this.tipoLineaService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<ITipoLinea[]>) => mayBeOk.ok),
                map((response: HttpResponse<ITipoLinea[]>) => response.body)
            )
            .subscribe((res: ITipoLinea[]) => (this.tipolineas = res), (res: HttpErrorResponse) => this.onError(res.message));
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.familia.id !== undefined) {
            this.subscribeToSaveResponse(this.familiaService.update(this.familia));
        } else {
            this.subscribeToSaveResponse(this.familiaService.create(this.familia));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IFamilia>>) {
        result.subscribe((res: HttpResponse<IFamilia>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackTipoLineaById(index: number, item: ITipoLinea) {
        return item.id;
    }
}
