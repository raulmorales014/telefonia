import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IComercial } from 'app/shared/model/comercial.model';

type EntityResponseType = HttpResponse<IComercial>;
type EntityArrayResponseType = HttpResponse<IComercial[]>;

@Injectable({ providedIn: 'root' })
export class ComercialService {
    public resourceUrl = SERVER_API_URL + 'api/comercials';

    constructor(protected http: HttpClient) {}

    create(comercial: IComercial): Observable<EntityResponseType> {
        return this.http.post<IComercial>(this.resourceUrl, comercial, { observe: 'response' });
    }

    update(comercial: IComercial): Observable<EntityResponseType> {
        return this.http.put<IComercial>(this.resourceUrl, comercial, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IComercial>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IComercial[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
