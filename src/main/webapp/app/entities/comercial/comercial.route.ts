import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Comercial } from 'app/shared/model/comercial.model';
import { ComercialService } from './comercial.service';
import { ComercialComponent } from './comercial.component';
import { ComercialDetailComponent } from './comercial-detail.component';
import { ComercialUpdateComponent } from './comercial-update.component';
import { ComercialDeletePopupComponent } from './comercial-delete-dialog.component';
import { IComercial } from 'app/shared/model/comercial.model';

@Injectable({ providedIn: 'root' })
export class ComercialResolve implements Resolve<IComercial> {
    constructor(private service: ComercialService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IComercial> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<Comercial>) => response.ok),
                map((comercial: HttpResponse<Comercial>) => comercial.body)
            );
        }
        return of(new Comercial());
    }
}

export const comercialRoute: Routes = [
    {
        path: '',
        component: ComercialComponent,
        resolve: {
            pagingParams: JhiResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            defaultSort: 'id,asc',
            pageTitle: 'telefoniaApp.comercial.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/view',
        component: ComercialDetailComponent,
        resolve: {
            comercial: ComercialResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'telefoniaApp.comercial.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'new',
        component: ComercialUpdateComponent,
        resolve: {
            comercial: ComercialResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'telefoniaApp.comercial.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/edit',
        component: ComercialUpdateComponent,
        resolve: {
            comercial: ComercialResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'telefoniaApp.comercial.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const comercialPopupRoute: Routes = [
    {
        path: ':id/delete',
        component: ComercialDeletePopupComponent,
        resolve: {
            comercial: ComercialResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'telefoniaApp.comercial.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
