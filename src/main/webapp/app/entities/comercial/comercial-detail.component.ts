import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IComercial } from 'app/shared/model/comercial.model';

@Component({
    selector: 'jhi-comercial-detail',
    templateUrl: './comercial-detail.component.html'
})
export class ComercialDetailComponent implements OnInit {
    comercial: IComercial;

    constructor(protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ comercial }) => {
            this.comercial = comercial;
        });
    }

    previousState() {
        window.history.back();
    }
}
