export * from './comercial.service';
export * from './comercial-update.component';
export * from './comercial-delete-dialog.component';
export * from './comercial-detail.component';
export * from './comercial.component';
export * from './comercial.route';
