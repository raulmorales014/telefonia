import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { TelefoniaSharedModule } from 'app/shared';
import {
    ComercialComponent,
    ComercialDetailComponent,
    ComercialUpdateComponent,
    ComercialDeletePopupComponent,
    ComercialDeleteDialogComponent,
    comercialRoute,
    comercialPopupRoute
} from './';

const ENTITY_STATES = [...comercialRoute, ...comercialPopupRoute];

@NgModule({
    imports: [TelefoniaSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        ComercialComponent,
        ComercialDetailComponent,
        ComercialUpdateComponent,
        ComercialDeleteDialogComponent,
        ComercialDeletePopupComponent
    ],
    entryComponents: [ComercialComponent, ComercialUpdateComponent, ComercialDeleteDialogComponent, ComercialDeletePopupComponent],
    providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TelefoniaComercialModule {
    constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
        this.languageHelper.language.subscribe((languageKey: string) => {
            if (languageKey !== undefined) {
                this.languageService.changeLanguage(languageKey);
            }
        });
    }
}
