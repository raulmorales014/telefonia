import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IComercial } from 'app/shared/model/comercial.model';
import { ComercialService } from './comercial.service';
import { IEquipo } from 'app/shared/model/equipo.model';
import { EquipoService } from 'app/entities/equipo';
import { ILogin } from 'app/shared/model/login.model';
import { LoginService } from 'app/entities/login';

@Component({
    selector: 'jhi-comercial-update',
    templateUrl: './comercial-update.component.html'
})
export class ComercialUpdateComponent implements OnInit {
    comercial: IComercial;
    isSaving: boolean;

    equipos: IEquipo[];

    logins: ILogin[];

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected comercialService: ComercialService,
        protected equipoService: EquipoService,
        protected loginService: LoginService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ comercial }) => {
            this.comercial = comercial;
        });
        this.equipoService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IEquipo[]>) => mayBeOk.ok),
                map((response: HttpResponse<IEquipo[]>) => response.body)
            )
            .subscribe((res: IEquipo[]) => (this.equipos = res), (res: HttpErrorResponse) => this.onError(res.message));
        this.loginService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<ILogin[]>) => mayBeOk.ok),
                map((response: HttpResponse<ILogin[]>) => response.body)
            )
            .subscribe((res: ILogin[]) => (this.logins = res), (res: HttpErrorResponse) => this.onError(res.message));
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.comercial.id !== undefined) {
            this.subscribeToSaveResponse(this.comercialService.update(this.comercial));
        } else {
            this.subscribeToSaveResponse(this.comercialService.create(this.comercial));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IComercial>>) {
        result.subscribe((res: HttpResponse<IComercial>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackEquipoById(index: number, item: IEquipo) {
        return item.id;
    }

    trackLoginById(index: number, item: ILogin) {
        return item.id;
    }
}
