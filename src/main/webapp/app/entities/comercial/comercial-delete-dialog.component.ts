import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IComercial } from 'app/shared/model/comercial.model';
import { ComercialService } from './comercial.service';

@Component({
    selector: 'jhi-comercial-delete-dialog',
    templateUrl: './comercial-delete-dialog.component.html'
})
export class ComercialDeleteDialogComponent {
    comercial: IComercial;

    constructor(
        protected comercialService: ComercialService,
        public activeModal: NgbActiveModal,
        protected eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.comercialService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'comercialListModification',
                content: 'Deleted an comercial'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-comercial-delete-popup',
    template: ''
})
export class ComercialDeletePopupComponent implements OnInit, OnDestroy {
    protected ngbModalRef: NgbModalRef;

    constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ comercial }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(ComercialDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
                this.ngbModalRef.componentInstance.comercial = comercial;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate(['/comercial', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate(['/comercial', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
