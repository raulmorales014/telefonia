import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { TelefoniaSharedModule } from 'app/shared';
import {
    SubestadoComponent,
    SubestadoDetailComponent,
    SubestadoUpdateComponent,
    SubestadoDeletePopupComponent,
    SubestadoDeleteDialogComponent,
    subestadoRoute,
    subestadoPopupRoute
} from './';

const ENTITY_STATES = [...subestadoRoute, ...subestadoPopupRoute];

@NgModule({
    imports: [TelefoniaSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        SubestadoComponent,
        SubestadoDetailComponent,
        SubestadoUpdateComponent,
        SubestadoDeleteDialogComponent,
        SubestadoDeletePopupComponent
    ],
    entryComponents: [SubestadoComponent, SubestadoUpdateComponent, SubestadoDeleteDialogComponent, SubestadoDeletePopupComponent],
    providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TelefoniaSubestadoModule {
    constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
        this.languageHelper.language.subscribe((languageKey: string) => {
            if (languageKey !== undefined) {
                this.languageService.changeLanguage(languageKey);
            }
        });
    }
}
