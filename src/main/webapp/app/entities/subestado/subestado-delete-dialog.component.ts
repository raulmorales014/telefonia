import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ISubestado } from 'app/shared/model/subestado.model';
import { SubestadoService } from './subestado.service';

@Component({
    selector: 'jhi-subestado-delete-dialog',
    templateUrl: './subestado-delete-dialog.component.html'
})
export class SubestadoDeleteDialogComponent {
    subestado: ISubestado;

    constructor(
        protected subestadoService: SubestadoService,
        public activeModal: NgbActiveModal,
        protected eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.subestadoService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'subestadoListModification',
                content: 'Deleted an subestado'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-subestado-delete-popup',
    template: ''
})
export class SubestadoDeletePopupComponent implements OnInit, OnDestroy {
    protected ngbModalRef: NgbModalRef;

    constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ subestado }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(SubestadoDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
                this.ngbModalRef.componentInstance.subestado = subestado;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate(['/subestado', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate(['/subestado', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
