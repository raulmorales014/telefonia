import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Subestado } from 'app/shared/model/subestado.model';
import { SubestadoService } from './subestado.service';
import { SubestadoComponent } from './subestado.component';
import { SubestadoDetailComponent } from './subestado-detail.component';
import { SubestadoUpdateComponent } from './subestado-update.component';
import { SubestadoDeletePopupComponent } from './subestado-delete-dialog.component';
import { ISubestado } from 'app/shared/model/subestado.model';

@Injectable({ providedIn: 'root' })
export class SubestadoResolve implements Resolve<ISubestado> {
    constructor(private service: SubestadoService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ISubestado> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<Subestado>) => response.ok),
                map((subestado: HttpResponse<Subestado>) => subestado.body)
            );
        }
        return of(new Subestado());
    }
}

export const subestadoRoute: Routes = [
    {
        path: '',
        component: SubestadoComponent,
        resolve: {
            pagingParams: JhiResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            defaultSort: 'id,asc',
            pageTitle: 'telefoniaApp.subestado.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/view',
        component: SubestadoDetailComponent,
        resolve: {
            subestado: SubestadoResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'telefoniaApp.subestado.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'new',
        component: SubestadoUpdateComponent,
        resolve: {
            subestado: SubestadoResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'telefoniaApp.subestado.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/edit',
        component: SubestadoUpdateComponent,
        resolve: {
            subestado: SubestadoResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'telefoniaApp.subestado.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const subestadoPopupRoute: Routes = [
    {
        path: ':id/delete',
        component: SubestadoDeletePopupComponent,
        resolve: {
            subestado: SubestadoResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'telefoniaApp.subestado.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
