import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { ISubestado } from 'app/shared/model/subestado.model';
import { SubestadoService } from './subestado.service';
import { IEstado } from 'app/shared/model/estado.model';
import { EstadoService } from 'app/entities/estado';

@Component({
    selector: 'jhi-subestado-update',
    templateUrl: './subestado-update.component.html'
})
export class SubestadoUpdateComponent implements OnInit {
    subestado: ISubestado;
    isSaving: boolean;

    estados: IEstado[];

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected subestadoService: SubestadoService,
        protected estadoService: EstadoService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ subestado }) => {
            this.subestado = subestado;
        });
        this.estadoService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IEstado[]>) => mayBeOk.ok),
                map((response: HttpResponse<IEstado[]>) => response.body)
            )
            .subscribe((res: IEstado[]) => (this.estados = res), (res: HttpErrorResponse) => this.onError(res.message));
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.subestado.id !== undefined) {
            this.subscribeToSaveResponse(this.subestadoService.update(this.subestado));
        } else {
            this.subscribeToSaveResponse(this.subestadoService.create(this.subestado));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<ISubestado>>) {
        result.subscribe((res: HttpResponse<ISubestado>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackEstadoById(index: number, item: IEstado) {
        return item.id;
    }
}
