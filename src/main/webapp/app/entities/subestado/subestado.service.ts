import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ISubestado } from 'app/shared/model/subestado.model';

type EntityResponseType = HttpResponse<ISubestado>;
type EntityArrayResponseType = HttpResponse<ISubestado[]>;

@Injectable({ providedIn: 'root' })
export class SubestadoService {
    public resourceUrl = SERVER_API_URL + 'api/subestados';

    constructor(protected http: HttpClient) {}

    create(subestado: ISubestado): Observable<EntityResponseType> {
        return this.http.post<ISubestado>(this.resourceUrl, subestado, { observe: 'response' });
    }

    update(subestado: ISubestado): Observable<EntityResponseType> {
        return this.http.put<ISubestado>(this.resourceUrl, subestado, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<ISubestado>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<ISubestado[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
