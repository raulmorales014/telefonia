import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ISubestado } from 'app/shared/model/subestado.model';

@Component({
    selector: 'jhi-subestado-detail',
    templateUrl: './subestado-detail.component.html'
})
export class SubestadoDetailComponent implements OnInit {
    subestado: ISubestado;

    constructor(protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ subestado }) => {
            this.subestado = subestado;
        });
    }

    previousState() {
        window.history.back();
    }
}
