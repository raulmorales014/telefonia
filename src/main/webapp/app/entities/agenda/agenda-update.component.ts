import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService } from 'ng-jhipster';
import { IAgenda } from 'app/shared/model/agenda.model';
import { AgendaService } from './agenda.service';
import { IEstadoPorcentual } from 'app/shared/model/estado-porcentual.model';
import { EstadoPorcentualService } from 'app/entities/estado-porcentual';
import { IComercial } from 'app/shared/model/comercial.model';
import { ComercialService } from 'app/entities/comercial';

@Component({
    selector: 'jhi-agenda-update',
    templateUrl: './agenda-update.component.html'
})
export class AgendaUpdateComponent implements OnInit {
    agenda: IAgenda;
    isSaving: boolean;

    estadoporcentuals: IEstadoPorcentual[];

    comercials: IComercial[];
    inicio: string;
    fin: string;

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected agendaService: AgendaService,
        protected estadoPorcentualService: EstadoPorcentualService,
        protected comercialService: ComercialService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ agenda }) => {
            this.agenda = agenda;
            this.inicio = this.agenda.inicio != null ? this.agenda.inicio.format(DATE_TIME_FORMAT) : null;
            this.fin = this.agenda.fin != null ? this.agenda.fin.format(DATE_TIME_FORMAT) : null;
        });
        this.estadoPorcentualService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IEstadoPorcentual[]>) => mayBeOk.ok),
                map((response: HttpResponse<IEstadoPorcentual[]>) => response.body)
            )
            .subscribe((res: IEstadoPorcentual[]) => (this.estadoporcentuals = res), (res: HttpErrorResponse) => this.onError(res.message));
        this.comercialService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IComercial[]>) => mayBeOk.ok),
                map((response: HttpResponse<IComercial[]>) => response.body)
            )
            .subscribe((res: IComercial[]) => (this.comercials = res), (res: HttpErrorResponse) => this.onError(res.message));
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        this.agenda.inicio = this.inicio != null ? moment(this.inicio, DATE_TIME_FORMAT) : null;
        this.agenda.fin = this.fin != null ? moment(this.fin, DATE_TIME_FORMAT) : null;
        if (this.agenda.id !== undefined) {
            this.subscribeToSaveResponse(this.agendaService.update(this.agenda));
        } else {
            this.subscribeToSaveResponse(this.agendaService.create(this.agenda));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IAgenda>>) {
        result.subscribe((res: HttpResponse<IAgenda>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackEstadoPorcentualById(index: number, item: IEstadoPorcentual) {
        return item.id;
    }

    trackComercialById(index: number, item: IComercial) {
        return item.id;
    }
}
