import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IEstadoPorcentual } from 'app/shared/model/estado-porcentual.model';

type EntityResponseType = HttpResponse<IEstadoPorcentual>;
type EntityArrayResponseType = HttpResponse<IEstadoPorcentual[]>;

@Injectable({ providedIn: 'root' })
export class EstadoPorcentualService {
    public resourceUrl = SERVER_API_URL + 'api/estado-porcentuals';

    constructor(protected http: HttpClient) {}

    create(estadoPorcentual: IEstadoPorcentual): Observable<EntityResponseType> {
        return this.http.post<IEstadoPorcentual>(this.resourceUrl, estadoPorcentual, { observe: 'response' });
    }

    update(estadoPorcentual: IEstadoPorcentual): Observable<EntityResponseType> {
        return this.http.put<IEstadoPorcentual>(this.resourceUrl, estadoPorcentual, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IEstadoPorcentual>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IEstadoPorcentual[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
