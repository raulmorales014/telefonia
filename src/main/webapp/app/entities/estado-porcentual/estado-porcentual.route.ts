import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { EstadoPorcentual } from 'app/shared/model/estado-porcentual.model';
import { EstadoPorcentualService } from './estado-porcentual.service';
import { EstadoPorcentualComponent } from './estado-porcentual.component';
import { EstadoPorcentualDetailComponent } from './estado-porcentual-detail.component';
import { EstadoPorcentualUpdateComponent } from './estado-porcentual-update.component';
import { EstadoPorcentualDeletePopupComponent } from './estado-porcentual-delete-dialog.component';
import { IEstadoPorcentual } from 'app/shared/model/estado-porcentual.model';

@Injectable({ providedIn: 'root' })
export class EstadoPorcentualResolve implements Resolve<IEstadoPorcentual> {
    constructor(private service: EstadoPorcentualService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IEstadoPorcentual> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<EstadoPorcentual>) => response.ok),
                map((estadoPorcentual: HttpResponse<EstadoPorcentual>) => estadoPorcentual.body)
            );
        }
        return of(new EstadoPorcentual());
    }
}

export const estadoPorcentualRoute: Routes = [
    {
        path: '',
        component: EstadoPorcentualComponent,
        resolve: {
            pagingParams: JhiResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            defaultSort: 'id,asc',
            pageTitle: 'telefoniaApp.estadoPorcentual.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/view',
        component: EstadoPorcentualDetailComponent,
        resolve: {
            estadoPorcentual: EstadoPorcentualResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'telefoniaApp.estadoPorcentual.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'new',
        component: EstadoPorcentualUpdateComponent,
        resolve: {
            estadoPorcentual: EstadoPorcentualResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'telefoniaApp.estadoPorcentual.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/edit',
        component: EstadoPorcentualUpdateComponent,
        resolve: {
            estadoPorcentual: EstadoPorcentualResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'telefoniaApp.estadoPorcentual.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const estadoPorcentualPopupRoute: Routes = [
    {
        path: ':id/delete',
        component: EstadoPorcentualDeletePopupComponent,
        resolve: {
            estadoPorcentual: EstadoPorcentualResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'telefoniaApp.estadoPorcentual.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
