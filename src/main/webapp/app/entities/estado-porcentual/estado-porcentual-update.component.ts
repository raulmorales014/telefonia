import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { IEstadoPorcentual } from 'app/shared/model/estado-porcentual.model';
import { EstadoPorcentualService } from './estado-porcentual.service';

@Component({
    selector: 'jhi-estado-porcentual-update',
    templateUrl: './estado-porcentual-update.component.html'
})
export class EstadoPorcentualUpdateComponent implements OnInit {
    estadoPorcentual: IEstadoPorcentual;
    isSaving: boolean;

    constructor(protected estadoPorcentualService: EstadoPorcentualService, protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ estadoPorcentual }) => {
            this.estadoPorcentual = estadoPorcentual;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.estadoPorcentual.id !== undefined) {
            this.subscribeToSaveResponse(this.estadoPorcentualService.update(this.estadoPorcentual));
        } else {
            this.subscribeToSaveResponse(this.estadoPorcentualService.create(this.estadoPorcentual));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IEstadoPorcentual>>) {
        result.subscribe((res: HttpResponse<IEstadoPorcentual>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }
}
