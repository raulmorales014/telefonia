import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IEstadoPorcentual } from 'app/shared/model/estado-porcentual.model';
import { EstadoPorcentualService } from './estado-porcentual.service';

@Component({
    selector: 'jhi-estado-porcentual-delete-dialog',
    templateUrl: './estado-porcentual-delete-dialog.component.html'
})
export class EstadoPorcentualDeleteDialogComponent {
    estadoPorcentual: IEstadoPorcentual;

    constructor(
        protected estadoPorcentualService: EstadoPorcentualService,
        public activeModal: NgbActiveModal,
        protected eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.estadoPorcentualService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'estadoPorcentualListModification',
                content: 'Deleted an estadoPorcentual'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-estado-porcentual-delete-popup',
    template: ''
})
export class EstadoPorcentualDeletePopupComponent implements OnInit, OnDestroy {
    protected ngbModalRef: NgbModalRef;

    constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ estadoPorcentual }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(EstadoPorcentualDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.estadoPorcentual = estadoPorcentual;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate(['/estado-porcentual', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate(['/estado-porcentual', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
