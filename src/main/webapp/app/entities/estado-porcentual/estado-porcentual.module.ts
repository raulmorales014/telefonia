import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { TelefoniaSharedModule } from 'app/shared';
import {
    EstadoPorcentualComponent,
    EstadoPorcentualDetailComponent,
    EstadoPorcentualUpdateComponent,
    EstadoPorcentualDeletePopupComponent,
    EstadoPorcentualDeleteDialogComponent,
    estadoPorcentualRoute,
    estadoPorcentualPopupRoute
} from './';

const ENTITY_STATES = [...estadoPorcentualRoute, ...estadoPorcentualPopupRoute];

@NgModule({
    imports: [TelefoniaSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        EstadoPorcentualComponent,
        EstadoPorcentualDetailComponent,
        EstadoPorcentualUpdateComponent,
        EstadoPorcentualDeleteDialogComponent,
        EstadoPorcentualDeletePopupComponent
    ],
    entryComponents: [
        EstadoPorcentualComponent,
        EstadoPorcentualUpdateComponent,
        EstadoPorcentualDeleteDialogComponent,
        EstadoPorcentualDeletePopupComponent
    ],
    providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TelefoniaEstadoPorcentualModule {
    constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
        this.languageHelper.language.subscribe((languageKey: string) => {
            if (languageKey !== undefined) {
                this.languageService.changeLanguage(languageKey);
            }
        });
    }
}
