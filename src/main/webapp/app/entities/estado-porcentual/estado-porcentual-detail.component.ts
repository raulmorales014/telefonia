import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IEstadoPorcentual } from 'app/shared/model/estado-porcentual.model';

@Component({
    selector: 'jhi-estado-porcentual-detail',
    templateUrl: './estado-porcentual-detail.component.html'
})
export class EstadoPorcentualDetailComponent implements OnInit {
    estadoPorcentual: IEstadoPorcentual;

    constructor(protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ estadoPorcentual }) => {
            this.estadoPorcentual = estadoPorcentual;
        });
    }

    previousState() {
        window.history.back();
    }
}
