import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: 'cliente',
                loadChildren: './cliente/cliente.module#TelefoniaClienteModule'
            },
            {
                path: 'equipo',
                loadChildren: './equipo/equipo.module#TelefoniaEquipoModule'
            },
            {
                path: 'comercial',
                loadChildren: './comercial/comercial.module#TelefoniaComercialModule'
            },
            {
                path: 'login',
                loadChildren: './login/login.module#TelefoniaLoginModule'
            },
            {
                path: 'producto',
                loadChildren: './producto/producto.module#TelefoniaProductoModule'
            },
            {
                path: 'cuenta-linea',
                loadChildren: './cuenta-linea/cuenta-linea.module#TelefoniaCuentaLineaModule'
            },
            {
                path: 'familia',
                loadChildren: './familia/familia.module#TelefoniaFamiliaModule'
            },
            {
                path: 'tipo-linea',
                loadChildren: './tipo-linea/tipo-linea.module#TelefoniaTipoLineaModule'
            },
            {
                path: 'paquete',
                loadChildren: './paquete/paquete.module#TelefoniaPaqueteModule'
            },
            {
                path: 'oferta',
                loadChildren: './oferta/oferta.module#TelefoniaOfertaModule'
            },
            {
                path: 'estado-porcentual',
                loadChildren: './estado-porcentual/estado-porcentual.module#TelefoniaEstadoPorcentualModule'
            },
            {
                path: 'tramitado-por',
                loadChildren: './tramitado-por/tramitado-por.module#TelefoniaTramitadoPorModule'
            },
            {
                path: 'estado',
                loadChildren: './estado/estado.module#TelefoniaEstadoModule'
            },
            {
                path: 'subestado',
                loadChildren: './subestado/subestado.module#TelefoniaSubestadoModule'
            },
            {
                path: 'permanencia',
                loadChildren: './permanencia/permanencia.module#TelefoniaPermanenciaModule'
            },
            {
                path: 'perfil',
                loadChildren: './perfil/perfil.module#TelefoniaPerfilModule'
            },
            {
                path: 'linea',
                loadChildren: './linea/linea.module#TelefoniaLineaModule'
            },
            {
                path: 'gasto',
                loadChildren: './gasto/gasto.module#TelefoniaGastoModule'
            },
            {
                path: 'empresa',
                loadChildren: './empresa/empresa.module#TelefoniaEmpresaModule'
            },
            {
                path: 'agenda',
                loadChildren: './agenda/agenda.module#TelefoniaAgendaModule'
            },
            {
                path: 'cliente',
                loadChildren: './cliente/cliente.module#TelefoniaClienteModule'
            },
            {
                path: 'equipo',
                loadChildren: './equipo/equipo.module#TelefoniaEquipoModule'
            },
            {
                path: 'comercial',
                loadChildren: './comercial/comercial.module#TelefoniaComercialModule'
            },
            {
                path: 'login',
                loadChildren: './login/login.module#TelefoniaLoginModule'
            },
            {
                path: 'producto',
                loadChildren: './producto/producto.module#TelefoniaProductoModule'
            },
            {
                path: 'cuenta-linea',
                loadChildren: './cuenta-linea/cuenta-linea.module#TelefoniaCuentaLineaModule'
            },
            {
                path: 'familia',
                loadChildren: './familia/familia.module#TelefoniaFamiliaModule'
            },
            {
                path: 'tipo-linea',
                loadChildren: './tipo-linea/tipo-linea.module#TelefoniaTipoLineaModule'
            },
            {
                path: 'paquete',
                loadChildren: './paquete/paquete.module#TelefoniaPaqueteModule'
            },
            {
                path: 'oferta',
                loadChildren: './oferta/oferta.module#TelefoniaOfertaModule'
            },
            {
                path: 'estado-porcentual',
                loadChildren: './estado-porcentual/estado-porcentual.module#TelefoniaEstadoPorcentualModule'
            },
            {
                path: 'tramitado-por',
                loadChildren: './tramitado-por/tramitado-por.module#TelefoniaTramitadoPorModule'
            },
            {
                path: 'estado',
                loadChildren: './estado/estado.module#TelefoniaEstadoModule'
            },
            {
                path: 'subestado',
                loadChildren: './subestado/subestado.module#TelefoniaSubestadoModule'
            },
            {
                path: 'permanencia',
                loadChildren: './permanencia/permanencia.module#TelefoniaPermanenciaModule'
            },
            {
                path: 'perfil',
                loadChildren: './perfil/perfil.module#TelefoniaPerfilModule'
            },
            {
                path: 'linea',
                loadChildren: './linea/linea.module#TelefoniaLineaModule'
            },
            {
                path: 'gasto',
                loadChildren: './gasto/gasto.module#TelefoniaGastoModule'
            },
            {
                path: 'empresa',
                loadChildren: './empresa/empresa.module#TelefoniaEmpresaModule'
            },
            {
                path: 'agenda',
                loadChildren: './agenda/agenda.module#TelefoniaAgendaModule'
            },
            {
                path: 'cliente',
                loadChildren: './cliente/cliente.module#TelefoniaClienteModule'
            },
            {
                path: 'equipo',
                loadChildren: './equipo/equipo.module#TelefoniaEquipoModule'
            },
            {
                path: 'comercial',
                loadChildren: './comercial/comercial.module#TelefoniaComercialModule'
            },
            {
                path: 'login',
                loadChildren: './login/login.module#TelefoniaLoginModule'
            },
            {
                path: 'producto',
                loadChildren: './producto/producto.module#TelefoniaProductoModule'
            },
            {
                path: 'cuenta-linea',
                loadChildren: './cuenta-linea/cuenta-linea.module#TelefoniaCuentaLineaModule'
            },
            {
                path: 'familia',
                loadChildren: './familia/familia.module#TelefoniaFamiliaModule'
            },
            {
                path: 'tipo-linea',
                loadChildren: './tipo-linea/tipo-linea.module#TelefoniaTipoLineaModule'
            },
            {
                path: 'paquete',
                loadChildren: './paquete/paquete.module#TelefoniaPaqueteModule'
            },
            {
                path: 'oferta',
                loadChildren: './oferta/oferta.module#TelefoniaOfertaModule'
            },
            {
                path: 'estado-porcentual',
                loadChildren: './estado-porcentual/estado-porcentual.module#TelefoniaEstadoPorcentualModule'
            },
            {
                path: 'tramitado-por',
                loadChildren: './tramitado-por/tramitado-por.module#TelefoniaTramitadoPorModule'
            },
            {
                path: 'estado',
                loadChildren: './estado/estado.module#TelefoniaEstadoModule'
            },
            {
                path: 'subestado',
                loadChildren: './subestado/subestado.module#TelefoniaSubestadoModule'
            },
            {
                path: 'permanencia',
                loadChildren: './permanencia/permanencia.module#TelefoniaPermanenciaModule'
            },
            {
                path: 'perfil',
                loadChildren: './perfil/perfil.module#TelefoniaPerfilModule'
            },
            {
                path: 'linea',
                loadChildren: './linea/linea.module#TelefoniaLineaModule'
            },
            {
                path: 'gasto',
                loadChildren: './gasto/gasto.module#TelefoniaGastoModule'
            },
            {
                path: 'empresa',
                loadChildren: './empresa/empresa.module#TelefoniaEmpresaModule'
            },
            {
                path: 'agenda',
                loadChildren: './agenda/agenda.module#TelefoniaAgendaModule'
            },
            {
                path: 'calendario',
                loadChildren: './calendario/calendario.module#TelefoniaCalendarioModule'
            }
            /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
        ])
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TelefoniaEntityModule {}
