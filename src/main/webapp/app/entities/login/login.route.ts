import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Login } from 'app/shared/model/login.model';
import { LoginService } from './login.service';
import { LoginComponent } from './login.component';
import { LoginDetailComponent } from './login-detail.component';
import { LoginUpdateComponent } from './login-update.component';
import { LoginDeletePopupComponent } from './login-delete-dialog.component';
import { ILogin } from 'app/shared/model/login.model';

@Injectable({ providedIn: 'root' })
export class LoginResolve implements Resolve<ILogin> {
    constructor(private service: LoginService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ILogin> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<Login>) => response.ok),
                map((login: HttpResponse<Login>) => login.body)
            );
        }
        return of(new Login());
    }
}

export const loginRoute: Routes = [
    {
        path: '',
        component: LoginComponent,
        resolve: {
            pagingParams: JhiResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            defaultSort: 'id,asc',
            pageTitle: 'telefoniaApp.login.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/view',
        component: LoginDetailComponent,
        resolve: {
            login: LoginResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'telefoniaApp.login.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'new',
        component: LoginUpdateComponent,
        resolve: {
            login: LoginResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'telefoniaApp.login.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/edit',
        component: LoginUpdateComponent,
        resolve: {
            login: LoginResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'telefoniaApp.login.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const loginPopupRoute: Routes = [
    {
        path: ':id/delete',
        component: LoginDeletePopupComponent,
        resolve: {
            login: LoginResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'telefoniaApp.login.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
