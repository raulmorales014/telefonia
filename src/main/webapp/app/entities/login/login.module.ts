import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { TelefoniaSharedModule } from 'app/shared';
import {
    LoginComponent,
    LoginDetailComponent,
    LoginUpdateComponent,
    LoginDeletePopupComponent,
    LoginDeleteDialogComponent,
    loginRoute,
    loginPopupRoute
} from './';

const ENTITY_STATES = [...loginRoute, ...loginPopupRoute];

@NgModule({
    imports: [TelefoniaSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [LoginComponent, LoginDetailComponent, LoginUpdateComponent, LoginDeleteDialogComponent, LoginDeletePopupComponent],
    entryComponents: [LoginComponent, LoginUpdateComponent, LoginDeleteDialogComponent, LoginDeletePopupComponent],
    providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TelefoniaLoginModule {
    constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
        this.languageHelper.language.subscribe((languageKey: string) => {
            if (languageKey !== undefined) {
                this.languageService.changeLanguage(languageKey);
            }
        });
    }
}
