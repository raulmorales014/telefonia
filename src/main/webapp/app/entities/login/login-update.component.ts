import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { ILogin } from 'app/shared/model/login.model';
import { LoginService } from './login.service';
import { IPerfil } from 'app/shared/model/perfil.model';
import { PerfilService } from 'app/entities/perfil';

@Component({
    selector: 'jhi-login-update',
    templateUrl: './login-update.component.html'
})
export class LoginUpdateComponent implements OnInit {
    login: ILogin;
    isSaving: boolean;

    perfils: IPerfil[];

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected loginService: LoginService,
        protected perfilService: PerfilService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ login }) => {
            this.login = login;
        });
        this.perfilService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IPerfil[]>) => mayBeOk.ok),
                map((response: HttpResponse<IPerfil[]>) => response.body)
            )
            .subscribe((res: IPerfil[]) => (this.perfils = res), (res: HttpErrorResponse) => this.onError(res.message));
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.login.id !== undefined) {
            this.subscribeToSaveResponse(this.loginService.update(this.login));
        } else {
            this.subscribeToSaveResponse(this.loginService.create(this.login));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<ILogin>>) {
        result.subscribe((res: HttpResponse<ILogin>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackPerfilById(index: number, item: IPerfil) {
        return item.id;
    }
}
