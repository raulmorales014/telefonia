import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Permanencia } from 'app/shared/model/permanencia.model';
import { PermanenciaService } from './permanencia.service';
import { PermanenciaComponent } from './permanencia.component';
import { PermanenciaDetailComponent } from './permanencia-detail.component';
import { PermanenciaUpdateComponent } from './permanencia-update.component';
import { PermanenciaDeletePopupComponent } from './permanencia-delete-dialog.component';
import { IPermanencia } from 'app/shared/model/permanencia.model';

@Injectable({ providedIn: 'root' })
export class PermanenciaResolve implements Resolve<IPermanencia> {
    constructor(private service: PermanenciaService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IPermanencia> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<Permanencia>) => response.ok),
                map((permanencia: HttpResponse<Permanencia>) => permanencia.body)
            );
        }
        return of(new Permanencia());
    }
}

export const permanenciaRoute: Routes = [
    {
        path: '',
        component: PermanenciaComponent,
        resolve: {
            pagingParams: JhiResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            defaultSort: 'id,asc',
            pageTitle: 'telefoniaApp.permanencia.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/view',
        component: PermanenciaDetailComponent,
        resolve: {
            permanencia: PermanenciaResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'telefoniaApp.permanencia.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'new',
        component: PermanenciaUpdateComponent,
        resolve: {
            permanencia: PermanenciaResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'telefoniaApp.permanencia.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/edit',
        component: PermanenciaUpdateComponent,
        resolve: {
            permanencia: PermanenciaResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'telefoniaApp.permanencia.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const permanenciaPopupRoute: Routes = [
    {
        path: ':id/delete',
        component: PermanenciaDeletePopupComponent,
        resolve: {
            permanencia: PermanenciaResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'telefoniaApp.permanencia.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
