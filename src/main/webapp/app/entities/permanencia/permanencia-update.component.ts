import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { IPermanencia } from 'app/shared/model/permanencia.model';
import { PermanenciaService } from './permanencia.service';

@Component({
    selector: 'jhi-permanencia-update',
    templateUrl: './permanencia-update.component.html'
})
export class PermanenciaUpdateComponent implements OnInit {
    permanencia: IPermanencia;
    isSaving: boolean;

    constructor(protected permanenciaService: PermanenciaService, protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ permanencia }) => {
            this.permanencia = permanencia;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.permanencia.id !== undefined) {
            this.subscribeToSaveResponse(this.permanenciaService.update(this.permanencia));
        } else {
            this.subscribeToSaveResponse(this.permanenciaService.create(this.permanencia));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IPermanencia>>) {
        result.subscribe((res: HttpResponse<IPermanencia>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }
}
