import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IPermanencia } from 'app/shared/model/permanencia.model';

type EntityResponseType = HttpResponse<IPermanencia>;
type EntityArrayResponseType = HttpResponse<IPermanencia[]>;

@Injectable({ providedIn: 'root' })
export class PermanenciaService {
    public resourceUrl = SERVER_API_URL + 'api/permanencias';

    constructor(protected http: HttpClient) {}

    create(permanencia: IPermanencia): Observable<EntityResponseType> {
        return this.http.post<IPermanencia>(this.resourceUrl, permanencia, { observe: 'response' });
    }

    update(permanencia: IPermanencia): Observable<EntityResponseType> {
        return this.http.put<IPermanencia>(this.resourceUrl, permanencia, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IPermanencia>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IPermanencia[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
