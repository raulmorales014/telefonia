export * from './permanencia.service';
export * from './permanencia-update.component';
export * from './permanencia-delete-dialog.component';
export * from './permanencia-detail.component';
export * from './permanencia.component';
export * from './permanencia.route';
