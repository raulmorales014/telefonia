import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { TelefoniaSharedModule } from 'app/shared';
import {
    PermanenciaComponent,
    PermanenciaDetailComponent,
    PermanenciaUpdateComponent,
    PermanenciaDeletePopupComponent,
    PermanenciaDeleteDialogComponent,
    permanenciaRoute,
    permanenciaPopupRoute
} from './';

const ENTITY_STATES = [...permanenciaRoute, ...permanenciaPopupRoute];

@NgModule({
    imports: [TelefoniaSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        PermanenciaComponent,
        PermanenciaDetailComponent,
        PermanenciaUpdateComponent,
        PermanenciaDeleteDialogComponent,
        PermanenciaDeletePopupComponent
    ],
    entryComponents: [PermanenciaComponent, PermanenciaUpdateComponent, PermanenciaDeleteDialogComponent, PermanenciaDeletePopupComponent],
    providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TelefoniaPermanenciaModule {
    constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
        this.languageHelper.language.subscribe((languageKey: string) => {
            if (languageKey !== undefined) {
                this.languageService.changeLanguage(languageKey);
            }
        });
    }
}
