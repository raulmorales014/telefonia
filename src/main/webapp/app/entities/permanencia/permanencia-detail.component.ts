import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IPermanencia } from 'app/shared/model/permanencia.model';

@Component({
    selector: 'jhi-permanencia-detail',
    templateUrl: './permanencia-detail.component.html'
})
export class PermanenciaDetailComponent implements OnInit {
    permanencia: IPermanencia;

    constructor(protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ permanencia }) => {
            this.permanencia = permanencia;
        });
    }

    previousState() {
        window.history.back();
    }
}
