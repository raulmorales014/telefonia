import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { IPaquete } from 'app/shared/model/paquete.model';
import { PaqueteService } from './paquete.service';

@Component({
    selector: 'jhi-paquete-update',
    templateUrl: './paquete-update.component.html'
})
export class PaqueteUpdateComponent implements OnInit {
    paquete: IPaquete;
    isSaving: boolean;

    constructor(protected paqueteService: PaqueteService, protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ paquete }) => {
            this.paquete = paquete;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.paquete.id !== undefined) {
            this.subscribeToSaveResponse(this.paqueteService.update(this.paquete));
        } else {
            this.subscribeToSaveResponse(this.paqueteService.create(this.paquete));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IPaquete>>) {
        result.subscribe((res: HttpResponse<IPaquete>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }
}
