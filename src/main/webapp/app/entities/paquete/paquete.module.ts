import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { TelefoniaSharedModule } from 'app/shared';
import {
    PaqueteComponent,
    PaqueteDetailComponent,
    PaqueteUpdateComponent,
    PaqueteDeletePopupComponent,
    PaqueteDeleteDialogComponent,
    paqueteRoute,
    paquetePopupRoute
} from './';

const ENTITY_STATES = [...paqueteRoute, ...paquetePopupRoute];

@NgModule({
    imports: [TelefoniaSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        PaqueteComponent,
        PaqueteDetailComponent,
        PaqueteUpdateComponent,
        PaqueteDeleteDialogComponent,
        PaqueteDeletePopupComponent
    ],
    entryComponents: [PaqueteComponent, PaqueteUpdateComponent, PaqueteDeleteDialogComponent, PaqueteDeletePopupComponent],
    providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TelefoniaPaqueteModule {
    constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
        this.languageHelper.language.subscribe((languageKey: string) => {
            if (languageKey !== undefined) {
                this.languageService.changeLanguage(languageKey);
            }
        });
    }
}
