export * from './paquete.service';
export * from './paquete-update.component';
export * from './paquete-delete-dialog.component';
export * from './paquete-detail.component';
export * from './paquete.component';
export * from './paquete.route';
