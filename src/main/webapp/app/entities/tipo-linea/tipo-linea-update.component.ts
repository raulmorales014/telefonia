import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { ITipoLinea } from 'app/shared/model/tipo-linea.model';
import { TipoLineaService } from './tipo-linea.service';

@Component({
    selector: 'jhi-tipo-linea-update',
    templateUrl: './tipo-linea-update.component.html'
})
export class TipoLineaUpdateComponent implements OnInit {
    tipoLinea: ITipoLinea;
    isSaving: boolean;

    constructor(protected tipoLineaService: TipoLineaService, protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ tipoLinea }) => {
            this.tipoLinea = tipoLinea;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.tipoLinea.id !== undefined) {
            this.subscribeToSaveResponse(this.tipoLineaService.update(this.tipoLinea));
        } else {
            this.subscribeToSaveResponse(this.tipoLineaService.create(this.tipoLinea));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<ITipoLinea>>) {
        result.subscribe((res: HttpResponse<ITipoLinea>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }
}
