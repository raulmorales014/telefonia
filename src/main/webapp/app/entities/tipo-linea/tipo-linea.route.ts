import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { TipoLinea } from 'app/shared/model/tipo-linea.model';
import { TipoLineaService } from './tipo-linea.service';
import { TipoLineaComponent } from './tipo-linea.component';
import { TipoLineaDetailComponent } from './tipo-linea-detail.component';
import { TipoLineaUpdateComponent } from './tipo-linea-update.component';
import { TipoLineaDeletePopupComponent } from './tipo-linea-delete-dialog.component';
import { ITipoLinea } from 'app/shared/model/tipo-linea.model';

@Injectable({ providedIn: 'root' })
export class TipoLineaResolve implements Resolve<ITipoLinea> {
    constructor(private service: TipoLineaService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ITipoLinea> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<TipoLinea>) => response.ok),
                map((tipoLinea: HttpResponse<TipoLinea>) => tipoLinea.body)
            );
        }
        return of(new TipoLinea());
    }
}

export const tipoLineaRoute: Routes = [
    {
        path: '',
        component: TipoLineaComponent,
        resolve: {
            pagingParams: JhiResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            defaultSort: 'id,asc',
            pageTitle: 'telefoniaApp.tipoLinea.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/view',
        component: TipoLineaDetailComponent,
        resolve: {
            tipoLinea: TipoLineaResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'telefoniaApp.tipoLinea.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'new',
        component: TipoLineaUpdateComponent,
        resolve: {
            tipoLinea: TipoLineaResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'telefoniaApp.tipoLinea.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/edit',
        component: TipoLineaUpdateComponent,
        resolve: {
            tipoLinea: TipoLineaResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'telefoniaApp.tipoLinea.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const tipoLineaPopupRoute: Routes = [
    {
        path: ':id/delete',
        component: TipoLineaDeletePopupComponent,
        resolve: {
            tipoLinea: TipoLineaResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'telefoniaApp.tipoLinea.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
