import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ITipoLinea } from 'app/shared/model/tipo-linea.model';
import { TipoLineaService } from './tipo-linea.service';

@Component({
    selector: 'jhi-tipo-linea-delete-dialog',
    templateUrl: './tipo-linea-delete-dialog.component.html'
})
export class TipoLineaDeleteDialogComponent {
    tipoLinea: ITipoLinea;

    constructor(
        protected tipoLineaService: TipoLineaService,
        public activeModal: NgbActiveModal,
        protected eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.tipoLineaService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'tipoLineaListModification',
                content: 'Deleted an tipoLinea'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-tipo-linea-delete-popup',
    template: ''
})
export class TipoLineaDeletePopupComponent implements OnInit, OnDestroy {
    protected ngbModalRef: NgbModalRef;

    constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ tipoLinea }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(TipoLineaDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
                this.ngbModalRef.componentInstance.tipoLinea = tipoLinea;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate(['/tipo-linea', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate(['/tipo-linea', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
