import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { TelefoniaSharedModule } from 'app/shared';
import {
    TipoLineaComponent,
    TipoLineaDetailComponent,
    TipoLineaUpdateComponent,
    TipoLineaDeletePopupComponent,
    TipoLineaDeleteDialogComponent,
    tipoLineaRoute,
    tipoLineaPopupRoute
} from './';

const ENTITY_STATES = [...tipoLineaRoute, ...tipoLineaPopupRoute];

@NgModule({
    imports: [TelefoniaSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        TipoLineaComponent,
        TipoLineaDetailComponent,
        TipoLineaUpdateComponent,
        TipoLineaDeleteDialogComponent,
        TipoLineaDeletePopupComponent
    ],
    entryComponents: [TipoLineaComponent, TipoLineaUpdateComponent, TipoLineaDeleteDialogComponent, TipoLineaDeletePopupComponent],
    providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TelefoniaTipoLineaModule {
    constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
        this.languageHelper.language.subscribe((languageKey: string) => {
            if (languageKey !== undefined) {
                this.languageService.changeLanguage(languageKey);
            }
        });
    }
}
