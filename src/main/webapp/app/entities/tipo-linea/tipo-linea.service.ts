import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ITipoLinea } from 'app/shared/model/tipo-linea.model';

type EntityResponseType = HttpResponse<ITipoLinea>;
type EntityArrayResponseType = HttpResponse<ITipoLinea[]>;

@Injectable({ providedIn: 'root' })
export class TipoLineaService {
    public resourceUrl = SERVER_API_URL + 'api/tipo-lineas';

    constructor(protected http: HttpClient) {}

    create(tipoLinea: ITipoLinea): Observable<EntityResponseType> {
        return this.http.post<ITipoLinea>(this.resourceUrl, tipoLinea, { observe: 'response' });
    }

    update(tipoLinea: ITipoLinea): Observable<EntityResponseType> {
        return this.http.put<ITipoLinea>(this.resourceUrl, tipoLinea, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<ITipoLinea>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<ITipoLinea[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
