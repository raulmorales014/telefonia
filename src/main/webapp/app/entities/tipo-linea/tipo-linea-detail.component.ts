import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ITipoLinea } from 'app/shared/model/tipo-linea.model';

@Component({
    selector: 'jhi-tipo-linea-detail',
    templateUrl: './tipo-linea-detail.component.html'
})
export class TipoLineaDetailComponent implements OnInit {
    tipoLinea: ITipoLinea;

    constructor(protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ tipoLinea }) => {
            this.tipoLinea = tipoLinea;
        });
    }

    previousState() {
        window.history.back();
    }
}
