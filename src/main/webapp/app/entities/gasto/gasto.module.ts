import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { TelefoniaSharedModule } from 'app/shared';
import {
    GastoComponent,
    GastoDetailComponent,
    GastoUpdateComponent,
    GastoDeletePopupComponent,
    GastoDeleteDialogComponent,
    gastoRoute,
    gastoPopupRoute
} from './';

const ENTITY_STATES = [...gastoRoute, ...gastoPopupRoute];

@NgModule({
    imports: [TelefoniaSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [GastoComponent, GastoDetailComponent, GastoUpdateComponent, GastoDeleteDialogComponent, GastoDeletePopupComponent],
    entryComponents: [GastoComponent, GastoUpdateComponent, GastoDeleteDialogComponent, GastoDeletePopupComponent],
    providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TelefoniaGastoModule {
    constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
        this.languageHelper.language.subscribe((languageKey: string) => {
            if (languageKey !== undefined) {
                this.languageService.changeLanguage(languageKey);
            }
        });
    }
}
