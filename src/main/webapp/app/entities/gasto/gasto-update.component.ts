import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { JhiAlertService } from 'ng-jhipster';
import { IGasto } from 'app/shared/model/gasto.model';
import { GastoService } from './gasto.service';
import { ICliente } from 'app/shared/model/cliente.model';
import { ClienteService } from 'app/entities/cliente';
import { IEmpresa } from 'app/shared/model/empresa.model';
import { EmpresaService } from 'app/entities/empresa';
import { IComercial } from 'app/shared/model/comercial.model';
import { ComercialService } from 'app/entities/comercial';
import { IEquipo } from 'app/shared/model/equipo.model';
import { EquipoService } from 'app/entities/equipo';

@Component({
    selector: 'jhi-gasto-update',
    templateUrl: './gasto-update.component.html'
})
export class GastoUpdateComponent implements OnInit {
    gasto: IGasto;
    isSaving: boolean;

    clientes: ICliente[];

    empresas: IEmpresa[];

    comercials: IComercial[];

    equipos: IEquipo[];
    fechaDp: any;

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected gastoService: GastoService,
        protected clienteService: ClienteService,
        protected empresaService: EmpresaService,
        protected comercialService: ComercialService,
        protected equipoService: EquipoService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ gasto }) => {
            this.gasto = gasto;
        });
        this.clienteService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<ICliente[]>) => mayBeOk.ok),
                map((response: HttpResponse<ICliente[]>) => response.body)
            )
            .subscribe((res: ICliente[]) => (this.clientes = res), (res: HttpErrorResponse) => this.onError(res.message));
        this.empresaService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IEmpresa[]>) => mayBeOk.ok),
                map((response: HttpResponse<IEmpresa[]>) => response.body)
            )
            .subscribe((res: IEmpresa[]) => (this.empresas = res), (res: HttpErrorResponse) => this.onError(res.message));
        this.comercialService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IComercial[]>) => mayBeOk.ok),
                map((response: HttpResponse<IComercial[]>) => response.body)
            )
            .subscribe((res: IComercial[]) => (this.comercials = res), (res: HttpErrorResponse) => this.onError(res.message));
        this.equipoService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IEquipo[]>) => mayBeOk.ok),
                map((response: HttpResponse<IEquipo[]>) => response.body)
            )
            .subscribe((res: IEquipo[]) => (this.equipos = res), (res: HttpErrorResponse) => this.onError(res.message));
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.gasto.id !== undefined) {
            this.subscribeToSaveResponse(this.gastoService.update(this.gasto));
        } else {
            this.subscribeToSaveResponse(this.gastoService.create(this.gasto));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IGasto>>) {
        result.subscribe((res: HttpResponse<IGasto>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackClienteById(index: number, item: ICliente) {
        return item.id;
    }

    trackEmpresaById(index: number, item: IEmpresa) {
        return item.id;
    }

    trackComercialById(index: number, item: IComercial) {
        return item.id;
    }

    trackEquipoById(index: number, item: IEquipo) {
        return item.id;
    }
}
