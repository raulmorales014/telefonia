import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ILinea } from 'app/shared/model/linea.model';

type EntityResponseType = HttpResponse<ILinea>;
type EntityArrayResponseType = HttpResponse<ILinea[]>;

@Injectable({ providedIn: 'root' })
export class LineaService {
    public resourceUrl = SERVER_API_URL + 'api/lineas';

    constructor(protected http: HttpClient) {}

    create(linea: ILinea): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(linea);
        return this.http
            .post<ILinea>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    update(linea: ILinea): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(linea);
        return this.http
            .put<ILinea>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http
            .get<ILinea>(`${this.resourceUrl}/${id}`, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http
            .get<ILinea[]>(this.resourceUrl, { params: options, observe: 'response' })
            .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    protected convertDateFromClient(linea: ILinea): ILinea {
        const copy: ILinea = Object.assign({}, linea, {
            provision: linea.provision != null && linea.provision.isValid() ? linea.provision.format(DATE_FORMAT) : null,
            activacion: linea.activacion != null && linea.activacion.isValid() ? linea.activacion.format(DATE_FORMAT) : null
        });
        return copy;
    }

    protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
        if (res.body) {
            res.body.provision = res.body.provision != null ? moment(res.body.provision) : null;
            res.body.activacion = res.body.activacion != null ? moment(res.body.activacion) : null;
        }
        return res;
    }

    protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
        if (res.body) {
            res.body.forEach((linea: ILinea) => {
                linea.provision = linea.provision != null ? moment(linea.provision) : null;
                linea.activacion = linea.activacion != null ? moment(linea.activacion) : null;
            });
        }
        return res;
    }
}
