import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ILinea } from 'app/shared/model/linea.model';

@Component({
    selector: 'jhi-linea-detail',
    templateUrl: './linea-detail.component.html'
})
export class LineaDetailComponent implements OnInit {
    linea: ILinea;

    constructor(protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ linea }) => {
            this.linea = linea;
        });
    }

    previousState() {
        window.history.back();
    }
}
