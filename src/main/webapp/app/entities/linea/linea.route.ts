import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Linea } from 'app/shared/model/linea.model';
import { LineaService } from './linea.service';
import { LineaComponent } from './linea.component';
import { LineaDetailComponent } from './linea-detail.component';
import { LineaUpdateComponent } from './linea-update.component';
import { LineaDeletePopupComponent } from './linea-delete-dialog.component';
import { ILinea } from 'app/shared/model/linea.model';

@Injectable({ providedIn: 'root' })
export class LineaResolve implements Resolve<ILinea> {
    constructor(private service: LineaService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ILinea> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<Linea>) => response.ok),
                map((linea: HttpResponse<Linea>) => linea.body)
            );
        }
        return of(new Linea());
    }
}

export const lineaRoute: Routes = [
    {
        path: '',
        component: LineaComponent,
        resolve: {
            pagingParams: JhiResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            defaultSort: 'id,asc',
            pageTitle: 'telefoniaApp.linea.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/view',
        component: LineaDetailComponent,
        resolve: {
            linea: LineaResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'telefoniaApp.linea.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'new',
        component: LineaUpdateComponent,
        resolve: {
            linea: LineaResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'telefoniaApp.linea.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/edit',
        component: LineaUpdateComponent,
        resolve: {
            linea: LineaResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'telefoniaApp.linea.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const lineaPopupRoute: Routes = [
    {
        path: ':id/delete',
        component: LineaDeletePopupComponent,
        resolve: {
            linea: LineaResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'telefoniaApp.linea.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
