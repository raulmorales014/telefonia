import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ILinea } from 'app/shared/model/linea.model';
import { LineaService } from './linea.service';

@Component({
    selector: 'jhi-linea-delete-dialog',
    templateUrl: './linea-delete-dialog.component.html'
})
export class LineaDeleteDialogComponent {
    linea: ILinea;

    constructor(protected lineaService: LineaService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.lineaService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'lineaListModification',
                content: 'Deleted an linea'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-linea-delete-popup',
    template: ''
})
export class LineaDeletePopupComponent implements OnInit, OnDestroy {
    protected ngbModalRef: NgbModalRef;

    constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ linea }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(LineaDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
                this.ngbModalRef.componentInstance.linea = linea;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate(['/linea', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate(['/linea', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
