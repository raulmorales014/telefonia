import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { TelefoniaSharedModule } from 'app/shared';
import {
    LineaComponent,
    LineaDetailComponent,
    LineaUpdateComponent,
    LineaDeletePopupComponent,
    LineaDeleteDialogComponent,
    lineaRoute,
    lineaPopupRoute
} from './';

const ENTITY_STATES = [...lineaRoute, ...lineaPopupRoute];

@NgModule({
    imports: [TelefoniaSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [LineaComponent, LineaDetailComponent, LineaUpdateComponent, LineaDeleteDialogComponent, LineaDeletePopupComponent],
    entryComponents: [LineaComponent, LineaUpdateComponent, LineaDeleteDialogComponent, LineaDeletePopupComponent],
    providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TelefoniaLineaModule {
    constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
        this.languageHelper.language.subscribe((languageKey: string) => {
            if (languageKey !== undefined) {
                this.languageService.changeLanguage(languageKey);
            }
        });
    }
}
