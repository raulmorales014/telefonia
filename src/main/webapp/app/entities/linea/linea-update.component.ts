import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { JhiAlertService } from 'ng-jhipster';
import { ILinea } from 'app/shared/model/linea.model';
import { LineaService } from './linea.service';
import { IProducto } from 'app/shared/model/producto.model';
import { ProductoService } from 'app/entities/producto';
import { IPermanencia } from 'app/shared/model/permanencia.model';
import { PermanenciaService } from 'app/entities/permanencia';
import { IPaquete } from 'app/shared/model/paquete.model';
import { PaqueteService } from 'app/entities/paquete';
import { IOferta } from 'app/shared/model/oferta.model';
import { OfertaService } from 'app/entities/oferta';

@Component({
    selector: 'jhi-linea-update',
    templateUrl: './linea-update.component.html'
})
export class LineaUpdateComponent implements OnInit {
    linea: ILinea;
    isSaving: boolean;

    productos: IProducto[];

    permanencias: IPermanencia[];

    paquetes: IPaquete[];

    ofertas: IOferta[];
    provisionDp: any;
    activacionDp: any;

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected lineaService: LineaService,
        protected productoService: ProductoService,
        protected permanenciaService: PermanenciaService,
        protected paqueteService: PaqueteService,
        protected ofertaService: OfertaService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ linea }) => {
            this.linea = linea;
        });
        this.productoService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IProducto[]>) => mayBeOk.ok),
                map((response: HttpResponse<IProducto[]>) => response.body)
            )
            .subscribe((res: IProducto[]) => (this.productos = res), (res: HttpErrorResponse) => this.onError(res.message));
        this.permanenciaService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IPermanencia[]>) => mayBeOk.ok),
                map((response: HttpResponse<IPermanencia[]>) => response.body)
            )
            .subscribe((res: IPermanencia[]) => (this.permanencias = res), (res: HttpErrorResponse) => this.onError(res.message));
        this.paqueteService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IPaquete[]>) => mayBeOk.ok),
                map((response: HttpResponse<IPaquete[]>) => response.body)
            )
            .subscribe((res: IPaquete[]) => (this.paquetes = res), (res: HttpErrorResponse) => this.onError(res.message));
        this.ofertaService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IOferta[]>) => mayBeOk.ok),
                map((response: HttpResponse<IOferta[]>) => response.body)
            )
            .subscribe((res: IOferta[]) => (this.ofertas = res), (res: HttpErrorResponse) => this.onError(res.message));
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.linea.id !== undefined) {
            this.subscribeToSaveResponse(this.lineaService.update(this.linea));
        } else {
            this.subscribeToSaveResponse(this.lineaService.create(this.linea));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<ILinea>>) {
        result.subscribe((res: HttpResponse<ILinea>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackProductoById(index: number, item: IProducto) {
        return item.id;
    }

    trackPermanenciaById(index: number, item: IPermanencia) {
        return item.id;
    }

    trackPaqueteById(index: number, item: IPaquete) {
        return item.id;
    }

    trackOfertaById(index: number, item: IOferta) {
        return item.id;
    }
}
