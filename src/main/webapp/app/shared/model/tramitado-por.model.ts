import { IOferta } from 'app/shared/model/oferta.model';

export interface ITramitadoPor {
    id?: number;
    nombre?: string;
    ofertas?: IOferta[];
}

export class TramitadoPor implements ITramitadoPor {
    constructor(public id?: number, public nombre?: string, public ofertas?: IOferta[]) {}
}
