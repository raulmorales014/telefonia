import { Moment } from 'moment';
import { IProducto } from 'app/shared/model/producto.model';
import { IPermanencia } from 'app/shared/model/permanencia.model';
import { IPaquete } from 'app/shared/model/paquete.model';
import { IOferta } from 'app/shared/model/oferta.model';

export interface ILinea {
    id?: number;
    linea?: number;
    esNueva?: boolean;
    descuento?: number;
    porta?: boolean;
    portaNombre?: string;
    portaCif?: string;
    portaOperador?: string;
    provision?: Moment;
    activacion?: Moment;
    paqueteNumero?: string;
    terminal?: string;
    pagoInicial?: number;
    cuotas?: number;
    pagoUnico?: number;
    observacion?: string;
    observacionAdicional?: string;
    producto?: IProducto;
    permanencia?: IPermanencia;
    paquete?: IPaquete;
    oferta?: IOferta;
}

export class Linea implements ILinea {
    constructor(
        public id?: number,
        public linea?: number,
        public esNueva?: boolean,
        public descuento?: number,
        public porta?: boolean,
        public portaNombre?: string,
        public portaCif?: string,
        public portaOperador?: string,
        public provision?: Moment,
        public activacion?: Moment,
        public paqueteNumero?: string,
        public terminal?: string,
        public pagoInicial?: number,
        public cuotas?: number,
        public pagoUnico?: number,
        public observacion?: string,
        public observacionAdicional?: string,
        public producto?: IProducto,
        public permanencia?: IPermanencia,
        public paquete?: IPaquete,
        public oferta?: IOferta
    ) {
        this.esNueva = this.esNueva || false;
        this.porta = this.porta || false;
    }
}
