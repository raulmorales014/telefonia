import { ISubestado } from 'app/shared/model/subestado.model';

export interface IEstado {
    id?: number;
    nombre?: string;
    subestados?: ISubestado[];
}

export class Estado implements IEstado {
    constructor(public id?: number, public nombre?: string, public subestados?: ISubestado[]) {}
}
