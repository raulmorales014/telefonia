import { ILinea } from 'app/shared/model/linea.model';
import { IFamilia } from 'app/shared/model/familia.model';
import { ICuentaLinea } from 'app/shared/model/cuenta-linea.model';

export interface IProducto {
    id?: number;
    nombre?: string;
    spirit?: string;
    ow?: string;
    comisionConPermanencia?: number;
    comisionSinPermanencia?: number;
    rappel?: number;
    precioVenta?: number;
    lineas?: ILinea[];
    familia?: IFamilia;
    cuentaLinea?: ICuentaLinea;
}

export class Producto implements IProducto {
    constructor(
        public id?: number,
        public nombre?: string,
        public spirit?: string,
        public ow?: string,
        public comisionConPermanencia?: number,
        public comisionSinPermanencia?: number,
        public rappel?: number,
        public precioVenta?: number,
        public lineas?: ILinea[],
        public familia?: IFamilia,
        public cuentaLinea?: ICuentaLinea
    ) {}
}
