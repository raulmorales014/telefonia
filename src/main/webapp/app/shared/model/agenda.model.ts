import { Moment } from 'moment';
import { IEstadoPorcentual } from 'app/shared/model/estado-porcentual.model';
import { IComercial } from 'app/shared/model/comercial.model';

export const enum Color {
    RojoClaro = 'RojoClaro',
    Azul = 'Azul',
    Turquesa = 'Turquesa',
    Verde = 'Verde',
    Amarillo = 'Amarillo',
    Naranja = 'Naranja',
    Rojo = 'Rojo',
    Negro = 'Negro'
}

export interface IAgenda {
    id?: number;
    titulo?: string;
    color?: Color;
    inicio?: Moment;
    fin?: Moment;
    observacion?: string;
    estadoPorcentual?: IEstadoPorcentual;
    comercial?: IComercial;
}

export class Agenda implements IAgenda {
    constructor(
        public id?: number,
        public titulo?: string,
        public color?: Color,
        public inicio?: Moment,
        public fin?: Moment,
        public observacion?: string,
        public estadoPorcentual?: IEstadoPorcentual,
        public comercial?: IComercial
    ) {}
}
