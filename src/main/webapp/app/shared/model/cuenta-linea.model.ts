import { IProducto } from 'app/shared/model/producto.model';

export interface ICuentaLinea {
    id?: number;
    nombre?: string;
    productos?: IProducto[];
}

export class CuentaLinea implements ICuentaLinea {
    constructor(public id?: number, public nombre?: string, public productos?: IProducto[]) {}
}
