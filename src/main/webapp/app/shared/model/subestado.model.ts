import { IOferta } from 'app/shared/model/oferta.model';
import { IEstado } from 'app/shared/model/estado.model';

export interface ISubestado {
    id?: number;
    nombre?: string;
    ofertas?: IOferta[];
    estado?: IEstado;
}

export class Subestado implements ISubestado {
    constructor(public id?: number, public nombre?: string, public ofertas?: IOferta[], public estado?: IEstado) {}
}
