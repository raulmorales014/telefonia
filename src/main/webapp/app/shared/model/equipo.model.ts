import { IComercial } from 'app/shared/model/comercial.model';
import { IGasto } from 'app/shared/model/gasto.model';

export interface IEquipo {
    id?: number;
    nombre?: string;
    comercials?: IComercial[];
    gastos?: IGasto[];
}

export class Equipo implements IEquipo {
    constructor(public id?: number, public nombre?: string, public comercials?: IComercial[], public gastos?: IGasto[]) {}
}
