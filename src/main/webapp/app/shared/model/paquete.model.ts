import { ILinea } from 'app/shared/model/linea.model';

export interface IPaquete {
    id?: number;
    nombre?: string;
    precio?: number;
    lineas?: ILinea[];
}

export class Paquete implements IPaquete {
    constructor(public id?: number, public nombre?: string, public precio?: number, public lineas?: ILinea[]) {}
}
