import { IGasto } from 'app/shared/model/gasto.model';

export interface IEmpresa {
    id?: number;
    nombre?: string;
    gastos?: IGasto[];
}

export class Empresa implements IEmpresa {
    constructor(public id?: number, public nombre?: string, public gastos?: IGasto[]) {}
}
