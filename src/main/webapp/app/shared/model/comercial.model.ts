import { IOferta } from 'app/shared/model/oferta.model';
import { IGasto } from 'app/shared/model/gasto.model';
import { IAgenda } from 'app/shared/model/agenda.model';
import { IEquipo } from 'app/shared/model/equipo.model';
import { ILogin } from 'app/shared/model/login.model';

export interface IComercial {
    id?: number;
    nombre?: string;
    apellidos?: string;
    dni?: string;
    sfid?: string;
    ofertas?: IOferta[];
    gastos?: IGasto[];
    agenda?: IAgenda[];
    equipo?: IEquipo;
    login?: ILogin;
}

export class Comercial implements IComercial {
    constructor(
        public id?: number,
        public nombre?: string,
        public apellidos?: string,
        public dni?: string,
        public sfid?: string,
        public ofertas?: IOferta[],
        public gastos?: IGasto[],
        public agenda?: IAgenda[],
        public equipo?: IEquipo,
        public login?: ILogin
    ) {}
}
