import { IProducto } from 'app/shared/model/producto.model';
import { ITipoLinea } from 'app/shared/model/tipo-linea.model';

export interface IFamilia {
    id?: number;
    nombre?: string;
    productos?: IProducto[];
    tipoLinea?: ITipoLinea;
}

export class Familia implements IFamilia {
    constructor(public id?: number, public nombre?: string, public productos?: IProducto[], public tipoLinea?: ITipoLinea) {}
}
