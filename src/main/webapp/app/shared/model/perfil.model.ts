import { ILogin } from 'app/shared/model/login.model';

export interface IPerfil {
    id?: number;
    nombre?: string;
    logins?: ILogin[];
}

export class Perfil implements IPerfil {
    constructor(public id?: number, public nombre?: string, public logins?: ILogin[]) {}
}
