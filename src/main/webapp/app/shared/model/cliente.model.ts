import { Moment } from 'moment';
import { IOferta } from 'app/shared/model/oferta.model';
import { IGasto } from 'app/shared/model/gasto.model';

export interface ICliente {
    id?: number;
    nombre?: string;
    cif?: string;
    apoderado?: string;
    nacimiento?: Moment;
    dni?: string;
    personaContacto?: string;
    correo?: string;
    telefono?: number;
    direccion?: string;
    localidad?: string;
    provincia?: string;
    codigoPostal?: number;
    cta?: string;
    observaciones?: string;
    penalizacionesVF?: number;
    penalizacionesUn4tel?: number;
    liberalizaciones?: number;
    costeAlcliente?: number;
    ocultarComerciales?: boolean;
    ofertas?: IOferta[];
    gastos?: IGasto[];
}

export class Cliente implements ICliente {
    constructor(
        public id?: number,
        public nombre?: string,
        public cif?: string,
        public apoderado?: string,
        public nacimiento?: Moment,
        public dni?: string,
        public personaContacto?: string,
        public correo?: string,
        public telefono?: number,
        public direccion?: string,
        public localidad?: string,
        public provincia?: string,
        public codigoPostal?: number,
        public cta?: string,
        public observaciones?: string,
        public penalizacionesVF?: number,
        public penalizacionesUn4tel?: number,
        public liberalizaciones?: number,
        public costeAlcliente?: number,
        public ocultarComerciales?: boolean,
        public ofertas?: IOferta[],
        public gastos?: IGasto[]
    ) {
        this.ocultarComerciales = this.ocultarComerciales || false;
    }
}
