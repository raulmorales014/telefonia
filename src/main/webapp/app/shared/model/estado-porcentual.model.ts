import { IOferta } from 'app/shared/model/oferta.model';
import { IAgenda } from 'app/shared/model/agenda.model';

export interface IEstadoPorcentual {
    id?: number;
    nombre?: string;
    ofertas?: IOferta[];
    agenda?: IAgenda[];
}

export class EstadoPorcentual implements IEstadoPorcentual {
    constructor(public id?: number, public nombre?: string, public ofertas?: IOferta[], public agenda?: IAgenda[]) {}
}
