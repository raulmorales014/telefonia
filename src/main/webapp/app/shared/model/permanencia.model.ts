import { ILinea } from 'app/shared/model/linea.model';

export interface IPermanencia {
    id?: number;
    nombre?: string;
    lineas?: ILinea[];
}

export class Permanencia implements IPermanencia {
    constructor(public id?: number, public nombre?: string, public lineas?: ILinea[]) {}
}
