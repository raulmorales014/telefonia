import { IFamilia } from 'app/shared/model/familia.model';

export interface ITipoLinea {
    id?: number;
    nombre?: string;
    familias?: IFamilia[];
}

export class TipoLinea implements ITipoLinea {
    constructor(public id?: number, public nombre?: string, public familias?: IFamilia[]) {}
}
