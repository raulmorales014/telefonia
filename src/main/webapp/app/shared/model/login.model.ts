import { IComercial } from 'app/shared/model/comercial.model';
import { IPerfil } from 'app/shared/model/perfil.model';

export interface ILogin {
    id?: number;
    nombre?: string;
    pass?: string;
    comercials?: IComercial[];
    perfil?: IPerfil;
}

export class Login implements ILogin {
    constructor(
        public id?: number,
        public nombre?: string,
        public pass?: string,
        public comercials?: IComercial[],
        public perfil?: IPerfil
    ) {}
}
