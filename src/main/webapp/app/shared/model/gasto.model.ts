import { Moment } from 'moment';
import { ICliente } from 'app/shared/model/cliente.model';
import { IEmpresa } from 'app/shared/model/empresa.model';
import { IComercial } from 'app/shared/model/comercial.model';
import { IEquipo } from 'app/shared/model/equipo.model';

export const enum Asociado {
    Empresa = 'Empresa',
    Cliente = 'Cliente',
    Comercial = 'Comercial',
    Equipo = 'Equipo'
}

export interface IGasto {
    id?: number;
    nombre?: string;
    fecha?: Moment;
    precio?: number;
    asociado?: Asociado;
    cliente?: ICliente;
    empresa?: IEmpresa;
    comercial?: IComercial;
    equipo?: IEquipo;
}

export class Gasto implements IGasto {
    constructor(
        public id?: number,
        public nombre?: string,
        public fecha?: Moment,
        public precio?: number,
        public asociado?: Asociado,
        public cliente?: ICliente,
        public empresa?: IEmpresa,
        public comercial?: IComercial,
        public equipo?: IEquipo
    ) {}
}
