import { Moment } from 'moment';
import { ILinea } from 'app/shared/model/linea.model';
import { IEstadoPorcentual } from 'app/shared/model/estado-porcentual.model';
import { IComercial } from 'app/shared/model/comercial.model';
import { ICliente } from 'app/shared/model/cliente.model';
import { ITramitadoPor } from 'app/shared/model/tramitado-por.model';
import { ISubestado } from 'app/shared/model/subestado.model';

export interface IOferta {
    id?: number;
    numeroOferta?: string;
    fecha?: Moment;
    firmado?: boolean;
    lineas?: ILinea[];
    estadoPorcentual?: IEstadoPorcentual;
    comercial?: IComercial;
    cliente?: ICliente;
    tramitadoPor?: ITramitadoPor;
    subestado?: ISubestado;
}

export class Oferta implements IOferta {
    constructor(
        public id?: number,
        public numeroOferta?: string,
        public fecha?: Moment,
        public firmado?: boolean,
        public lineas?: ILinea[],
        public estadoPorcentual?: IEstadoPorcentual,
        public comercial?: IComercial,
        public cliente?: ICliente,
        public tramitadoPor?: ITramitadoPor,
        public subestado?: ISubestado
    ) {
        this.firmado = this.firmado || false;
    }
}
