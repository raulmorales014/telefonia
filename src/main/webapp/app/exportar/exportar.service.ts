import { Injectable } from '@angular/core';
// import { TranslateService } from '@ngx-translate/core';

import * as jsPDF from 'jspdf';
import 'jspdf-autotable';

import * as XLSX from 'xlsx';

@Injectable({ providedIn: 'root' })
export class Exportar {
    constructor() {} // protected translateService: TranslateService

    generarXLS(titulo: string, datos) {
        const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(datos);
        const wb: XLSX.WorkBook = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(wb, ws, titulo);

        /* save to file */
        XLSX.writeFile(wb, titulo + '.xlsx');
    }

    generarPDF(titulo: string, filasCabecera, filasCuerpo, orientacionVertical: boolean) {
        const doc = this.generarjsPDF(titulo, filasCabecera, filasCuerpo, orientacionVertical);
        const nombrefichero = titulo.replace(/ /g, '') + '.pdf';
        doc.save(nombrefichero);
    }

    imprimirPDF(titulo: string, filasCabecera, filasCuerpo, orientacionVertical: boolean) {
        const doc = this.generarjsPDF(titulo, filasCabecera, filasCuerpo, orientacionVertical);
        doc.autoPrint();
        window.open(doc.output('bloburl'), 'fami');
    }

    generarjsPDF(titulo: string, filasCabecera, filasCuerpo, orientacionVertical: boolean) {
        let orientacion = 'p';
        let plusMargenPaginacion = 0;
        if (!orientacionVertical) {
            orientacion = 'l';
            plusMargenPaginacion = 130;
        }
        const doc = new jsPDF(orientacion, 'pt');
        const totalPagesExp = '{total_pages_count_string}'; // placeholder for total number of pages
        doc.autoTable({
            styles: {
                cellPadding: 0.5,
                fontSize: 12
            },
            // startY: 30, /* if start position is fixed from top */
            tableLineColor: [0, 0, 0], // choose RGB
            // tableLineWidth: 0.5, // table border width
            head: filasCabecera, // define head rows
            body: filasCuerpo,
            didDrawPage: data => {
                // Header
                doc.setFontSize(20);
                doc.setTextColor(40);
                doc.setFontStyle('normal');
                // if (base64Img) {
                //     doc.addImage(base64Img, 'JPEG', data.settings.margin.left, 15, 10, 10);
                // }
                doc.text(titulo, data.settings.margin.left, 55);

                // Footer
                let str = 'Página' + ' ' + doc.internal.getNumberOfPages(); // this.translateService.instant('global.exportar.pagina')
                // Total page number plugin only available in jspdf v1.0+
                if (typeof doc.putTotalPages === 'function') {
                    const de = 'de'; // this.translateService.instant('global.exportar.de')
                    str = str + ' ' + de + ' ' + totalPagesExp;
                }
                doc.setFontSize(10);

                // jsPDF 1.4+ uses getWidth, <1.4 uses .width
                const pageSize = doc.internal.pageSize;
                const pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight();
                doc.text(str, data.settings.margin.left + 210 + plusMargenPaginacion, pageHeight - 10);
            },
            margin: { top: 60 }
        });

        // Total page number plugin only available in jspdf v1.0+
        if (typeof doc.putTotalPages === 'function') {
            doc.putTotalPages(totalPagesExp);
        }

        return doc;
    }
}
